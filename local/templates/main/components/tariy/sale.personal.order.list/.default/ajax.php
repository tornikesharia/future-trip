<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if ($_REQUEST["key"]=="dell") {
    $arResult['status'] = true;
    $arResult['error'] = '';

    CModule::IncludeModule("sale");
    if (!CSaleOrder::StatusOrder($_REQUEST["id"], "P")) {
        $arResult['status'] = false;
        $arResult['error'] = 'Ошибка установки нового статуса заказа';
    }

    echo json_encode($arResult);
}