<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php    
CModule::IncludeModule($arResult["MODULES_NAME"]);

foreach( $arResult["ITEMS"] as $key => $arItem ):    
        if( !empty($arItem["PREVIEW_PICTURE"]) ):
            
                $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                        $arItem["PREVIEW_PICTURE"],
                        array('width' => 300,'height' => 200),
                        BX_RESIZE_IMAGE_EXACT,
                        true,
                        Array()
                );
            
        elseif( !empty($arItem["DETAIL_PICTURE"]) ):
            
                $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                        $arItem["DETAIL_PICTURE"],
                        array('width' => 300,'height' => 200),
                        BX_RESIZE_IMAGE_EXACT,
                        true,
                        Array()
                );
        else:
            
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"] = TMyClass::FromRootToFile(__DIR__)."/img/img.jpg";
            
        endif;
endforeach;