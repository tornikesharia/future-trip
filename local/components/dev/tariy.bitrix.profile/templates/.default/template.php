<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
//Lib::Debug($arResult['USER']);
?>
                        <div class="acc-profile">
            
            
            
                                <div class="acc-profile__title">Редактирование профиля</div>
            
                                <div class="nav nav-pills acc-profile__tab-buttons" role="tablist">
                                    <a class="nav-link active acc-profile__tab-button"
                                       id="acc-profile__tab1-button"
                                       data-toggle="tab"
                                       href="#acc-profile__tab_1"
                                       role="tab"
                                       aria-selected="true">Физическое лицо</a>
                                    <a class="nav-link acc-profile__tab-button"
                                       id="acc-profile__tab2-button"
                                       data-toggle="tab"
                                       href="#acc-profile__tab_2"
                                       role="tab"
                                       aria-selected="false">Юридическое лицо</a>
                                </div>
            
                                <div class="tab-content">
                                    <div class="tab-pane fade show active acc-profile__tab acc-profile__tab_1" id="acc-profile__tab_1" role="tabpanel">
										
                                        <form id="acc-profile__form-personal" class="acc-profile__form" action="<?= $componentPath ?>/component.php" method="post" enctype="multipart/form-data">
											<?= bitrix_sessid_post("profile_personal") ?>
											<input type="hidden" name="DATA[PROFILE_TYPE]" value="PERSONAL" />
											
											<div class="result"></div>
											
                                            <div class="form-group row acc-profile__form-group align-items-start">
                                                <div class="col-sm-4 col-md-3 col-lg-2 col-form-label">Фото</div>
                                                <div class="col-sm-8 col-md-9 col-lg-10 acc-profile__col-with-photo">
													<?if($arResult['USER']['PERSONAL_PHOTO']['SRC']):?>
                                                    <div class="acc-profile__avatar">
                                                        <!-- вариант загруженной фотографии -->
                                                        <img src="<?=$arResult['USER']['PERSONAL_PHOTO']['SRC']?>" alt="" class="acc-profile__avatar-img">
                                                        <button type="button" class="acc-profile__delete-photo-button">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
													<?else:?>
                                                    <?/*вариант пустой фотографии, из за нестандартного инпута название файла не будет показываться после выбора, можно сразу загрузить фотографию и показать по примеру как в добавлении нового отзыва */?>
                                                    <div class="acc-profile__empty-avatar">
														<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/img-placeholder.svg" alt="" class="acc-profile__img-placeholder">
														<div class="custom-file">
															<input type="file" name="PERSONAL_PHOTO" class="custom-file-input" id="acc-profile__avatar" accept="image/x-png,image/gif,image/jpeg">
															<label class="custom-file-label" for="acc-profile__avatar">Загрузить фото</label>
														</div>
                                                    </div>
													<?endif;?>
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_name" class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Имя</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           id="acc-profile__input_name"
                                                           name="DATA[NAME]"
                                                           value="<?= $arResult["USER"]["NAME"]?>" />
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_email" class="col-sm-4 col-md-3 col-lg-2 col-form-label required">E-mail</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10 acc-profile__col-with-hint">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           id="acc-profile__input_email"
                                                           name="DATA[EMAIL]"
                                                           value="<?= $arResult["USER"]["EMAIL"] ?>">
													<?/*	   
                                                    <div class="acc-profile__input-hint">
                                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/ok.svg" alt="" class="acc-profile__input-hint-img">
                                                        <div class="acc-profile__input-hint-text">Подтвержден</div>
                                                    </div>
													*/?>
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_phone" class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Телефон</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10 acc-profile__col-with-hint">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           id="acc-profile__input_phone"
                                                           name="DATA[PERSONAL_PHONE]"
                                                           placeholder="+7____-___-__-__"
														   value="<?= $arResult["USER"]["PERSONAL_PHONE"] ?>" />
													<?/*	   
                                                    <div class="acc-profile__input-hint">
                                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/i.svg" alt="" class="acc-profile__input-hint-img">
                                                        <div class="acc-profile__input-hint-text">Для подтверждения нового номера вам будет выслан на него SMS-код</div>
                                                    </div>
													*/?>
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label  class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Дата рождения</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10 acc-profile__birthday-col">
                                                    <div class="acc-profile__birthday-wrap">
                                                        <!-- инициализируется плагином календаря -->
                                                        <input type="text"
                                                               class="moto-input acc-profile__input acc-profile__input_birth js__acc-profile-birth"
                                                               id=""
                                                               name="DATA[PERSONAL_BIRTHDAY]"
                                                               value="<?=$arResult["USER"]["PERSONAL_BIRTHDAY"]?>"
                                                               placeholder="Выберите дату" />
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label class="col-sm-4 col-md-3 col-lg-2 col-form-label">Пол</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10 acc-profile__gender-col">
                                                    <div class="custom-control custom-radio custom-control-inline moto-radio acc-profile__radio">
                                                        <input type="radio"
                                                               id="acc-profile__gender_1"
                                                               name="DATA[PERSONAL_GENDER]"
															   value="M"
                                                               class="custom-control-input"
                                                               <?= ($arResult["USER"]["PERSONAL_GENDER"] == 'M' ? 'checked' : '') ?> />
                                                        <label class="custom-control-label" for="acc-profile__gender_1">Мужской</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline moto-radio acc-profile__radio">
                                                        <input type="radio"
                                                               id="acc-profile__gender_2"
                                                               name="DATA[PERSONAL_GENDER]"
															   value="F"
                                                               class="custom-control-input"
															   <?=($arResult["USER"]["PERSONAL_GENDER"] == 'F' ? 'checked' : '')?> />
                                                        <label class="custom-control-label" for="acc-profile__gender_2">Женский</label>
                                                    </div>
                                                </div>
                                            </div>
            
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_address" class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Адрес поставки</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           id="acc-profile__input_address"
                                                           name="DATA[PERSONAL_STREET]"
                                                           value="<?=$arResult["USER"]["PERSONAL_STREET"]?>"
                                                           required>
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_index" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Индекс</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_sm"
                                                           id="acc-profile__input_index"
                                                           name="DATA[PERSONAL_ZIP]"
                                                           value="<?=$arResult["USER"]["PERSONAL_ZIP"]?>" />
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group acc-profile__form-group_with-textarea">
                                                <label for="acc-profile__input_comment" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Комментарии</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
													<textarea class="moto-input acc-profile__input acc-profile__input_md"
														id="acc-profile__input_comment"
														name="DATA[PERSONAL_NOTES]"
														rows="8"
														placeholder="Укажите здесь комментраии к доставке, подъезд, этаж, наличие и код домофона, а также альтернативные способы связи."><?=$arResult["USER"]["PERSONAL_NOTES"]?></textarea>
                                                </div>
                                            </div>
											<div class="form-group row acc-profile__form-group">
												<label for="acc-profile__personal-old-pass" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Текущий пароль</label>
												<div class="col-sm-8 col-md-9 col-lg-10">
													<input type="password"
														   class="moto-input acc-profile__input acc-profile__input_md"
														   id="acc-profile__personal-old-pass"
														   name="DATA[OLD_PASS]"
														   />
												</div>
											</div>	
											<div class="form-group row acc-profile__form-group">
												<label for="acc-profile__personal-pass" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Новый пароль</label>
												<div class="col-sm-8 col-md-9 col-lg-10">
													<input type="password"
														   class="moto-input acc-profile__input acc-profile__input_md"
														   id="acc-profile__personal-pass"
														   name="DATA[PASS]"
														   />
												</div>
											</div>
											
											<div class="form-group row acc-profile__form-group">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" name="DATA[FZ152]" class="custom-control-input" id="stub-form__checkbox-personal" value="Y" checked>
													<label class="custom-control-label" for="stub-form__checkbox-personal" style="margin: 8px 0 0 15px;">&nbsp</label>
												</div>
												<div style="font-size:14px;">
													<label for="stub-form__checkbox-personal" style="margin:0 !important;">
													Я согласен на обработку персональных данных в соответствии с</label>
													<a href="#" style="color:#FFE23F;">Политикой конфиденциальности</a>
												</div>
											</div>												
											
                                            <div class="acc-profile__help-text">
                                                <span class="acc-profile__help-text-notice">*</span>
                                                Обязательные поля для заполнения
                                            </div>
			<?/* 
                                            <div class="acc-profile__title acc-profile__title_contacts">Контакты</div>
            
                                            <div class="acc-profile__contacts">
            
                                                <div class="acc-profile__contact">
                                                    <!--в дизайне этот блок с кнопками но мб можно это сделать каким нибудь стандартным компонентом, -->
                                                    <textarea class="moto-input acc-profile__input acc-profile__input_contact"
                                                              name="acc-profile__input_contact"
                                                              rows="8"
                                                              disabled
                                                    >Михаил Михайлович Иванов, michael@mail.ru, +7 925 100 00 05, 123 456, Москва, ул. Первая, д.55. Второй подъезд за мусоркой, затем налево, домофона нет. Просьба под окнами не кричать, соседи злые.</textarea>
                                                    <div class="acc-profile__contact-buttons">
                                                        <button class="acc-profile__contact-button acc-profile__contact-button_trash">
                                                            <svg class="acc-profile__contact-button-img" >
                                                                <use xlink:href="#icon-trash"></use>
                                                            </svg>
                                                        </button>
                                                        <button class="acc-profile__contact-button acc-profile__contact-button_edit">
                                                            <svg class="acc-profile__contact-button-img" >
                                                                <use xlink:href="#icon-edit"></use>
                                                            </svg>
                                                        </button>
                                                        <!--в дизайне есть зелёный стиль последней кнопки (или это не кнопка), для него добавить .valid к .acc-profile__contact-button_ok-->
                                                        <div class="acc-profile__contact-button acc-profile__contact-button_ok">
                                                            <svg class="acc-profile__contact-button-img" >
                                                                <use xlink:href="#icon-ok-empty"></use>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--для отключенного состояния добавить атрибут disabled, пример был в дизайне-->
                                                <button type="button" class="acc-profile__new-contact-button">
                                                    <span class="acc-profile__new-contact-plus">+</span>
                                                    <span class="acc-profile__new-contact-text">Добавить контакт</span>
                                                </button>
            
                                            </div><!-- /.acc-profile__contacts -->
             */?>
                                            <div class="acc-profile__buttons">
                                                <!--для отключенного состояния добавить атрибут disabled-->
                                                <button type="submit" class="button_yellow acc-profile__save-button">СОХРАНИТЬ</button>
                                            </div>
                                        </form><!-- /.acc-profile__form -->
            
                                    </div><!--/. tab1 -->
                                    <div class="tab-pane fade acc-profile__tab acc-profile__tab_2" id="acc-profile__tab_2" role="tabpanel">
										
                                        <form id="acc-profile__form-legal" class="acc-profile__form" action="<?= $componentPath ?>/component.php" method="post" name="form2">
											<?= bitrix_sessid_post("profile_legal") ?>
											<input type="hidden" name="DATA[PROFILE_TYPE]" value="LEGAL" />
											
											<div class="result"></div>
											
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_work-company" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Название организации</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
														   id="acc-profile__input_work-company"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           name="DATA[WORK_COMPANY]"
                                                           value="<?=$arResult["USER"]["WORK_COMPANY"]?>" />
                                                </div>
                                            </div>
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_inn" class="col-sm-4 col-md-3 col-lg-2 col-form-label">ИНН</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
														   id="acc-profile__input_inn"
                                                           class="moto-input acc-profile__input acc-profile__input_sm"
                                                           name="DATA[UF_INN]" 
														   value="<?=$arResult["USER"]["UF_INN"]?>" />
                                                </div>
                                            </div>
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_kpp" class="col-sm-4 col-md-3 col-lg-2 col-form-label">КПП</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
														   id="acc-profile__input_kpp"
                                                           class="moto-input acc-profile__input acc-profile__input_sm"
                                                           name="DATA[UF_KPP]"
                                                           value="<?=$arResult["USER"]["UF_KPP"]?>"
                                                           >
                                                </div>
                                            </div>
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_physical" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Физический адрес</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
														   id="acc-profile__input_physical"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           name="DATA[UF_PHYSICAL_ADDRESS]"
                                                           value="<?=$arResult["USER"]["UF_PHYSICAL_ADDRESS"]?>" />
                                                </div>
                                            </div>
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_legal" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Юридический адрес</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
														   id="acc-profile__input_legal"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           name="DATA[UF_LEGAL_ADDRESS]"
                                                           value="<?=$arResult["USER"]["UF_LEGAL_ADDRESS"]?>" />
                                                </div>
                                            </div>

                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_work_phone" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Телефон</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
														   id="acc-profile__input_work_phone"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           name="DATA[WORK_PHONE]"
                                                           value="<?=$arResult["USER"]["WORK_PHONE"]?>" />
                                                </div>
                                            </div>

											<div class="form-group row acc-profile__form-group">
												<div class="custom-control custom-checkbox">
													<input type="checkbox" name="DATA[FZ152]" class="custom-control-input" id="stub-form__checkbox-legal" value="Y" checked>
													<label class="custom-control-label" for="stub-form__checkbox-legal" style="margin: 8px 0 0 15px;">&nbsp</label>
												</div>
												<div style="font-size:14px;">
													<label for="stub-form__checkbox-legal" style="margin:0 !important;">
													Я согласен на обработку персональных данных в соответствии с</label>
													<a href="#" style="color:#FFE23F;">Политикой конфиденциальности</a>
												</div>
											</div>	
											
                                            <div class="acc-profile__buttons">
                                                <!--для отключенного состояния добавить атрибут disabled-->
                                                <button type="submit" class="button_yellow acc-profile__save-button">СОХРАНИТЬ</button>
                                            </div>
                                        </form><!-- /.acc-profile__form -->
            
                                    </div><!--/. tab2 -->
                                </div><!--/. tabs -->
                                
            
            
            
            
            
            
            
                        </div><!-- /.acc-profile -->
            