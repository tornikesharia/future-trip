$( document ).ready(function() {

    //$('.reg-modal').modal('show'); для теста всплывающих окон

    //================================================================================================== ОБЩИЕ

    //кастомный скролл внутри выпадающей корзины
    //https://github.com/malihu/malihu-custom-scrollbar-plugin
    $(".cart-dropdown__items").mCustomScrollbar({
        theme:"dark-3",
        scrollInertia: 200//хз
    });

    //=========блок выпадающей корзины
    var cartBlock = $('.header-middle__cart-block'),
        cartHolder = $('.header-middle__cart-holder'),
        cart = $('.cart-dropdown');
    //ховер над тригером корзины
    cartHolder.on('mouseenter', function (e) {
        //console.log('hovered');
        if ( window.matchMedia('(min-width: 992px)').matches ){
            cartBlock.addClass('open');
        }
    });
    //уход курсора с корзины
    cart.on('mouseleave', function (e) {
        //console.log('leave');
        if ( window.matchMedia('(min-width: 992px)').matches ){
            cartBlock.removeClass('open');
        }
    });
    //уход с триггера выпадения корзины
    cartHolder.on('mouseleave', function (e) {
        //console.log('leave');
        if ( window.matchMedia('(min-width: 992px)').matches ){
            setTimeout(function () {
                if ( !$('.header-middle__cart-block:hover').length != 0 ){
                    cartBlock.removeClass('open');
                }
            }, 600);
        }
    });

    //клик на тригере корзины
    cartHolder.on('click', function (e) {
        //console.log('hovered');
        if ( window.matchMedia('(max-width: 991px)').matches ){
            cartBlock.toggleClass('open');
        }
    });

    //====== блок выпадающего меню аккаунта
    var accBlock = $('.header-top__acc-block'),
        accHolder = $('.header-top__acc-holder'),
        acc = $('.acc-dropdown');
    //ховер над тригером меню аккаунта
    accHolder.on('mouseenter', function (e) {
        //console.log('hovered');
        if ( window.matchMedia('(min-width: 992px)').matches ){
            accBlock.addClass('open');
        }
    });
    //уход курсора с меню аккаунта
    acc.on('mouseleave', function (e) {
        //console.log('leave');
        if ( window.matchMedia('(min-width: 992px)').matches ){
            accBlock.removeClass('open');
        }
    });
    //уход с триггера выпадения меню аккаунта
    accHolder.on('mouseleave', function (e) {
        //console.log('leave');
        if ( window.matchMedia('(min-width: 992px)').matches ){
            setTimeout(function () {
                if ( $('.header-top__acc-block:hover').length == 0  ){
                    accBlock.removeClass('open');
                }
            }, 600);
        }
    });
    //клик на тригере меню аккаунта
    accHolder.on('click', function (e) {
        //console.log('hovered');
        if ( window.matchMedia('(max-width: 991px)').matches ){
            accBlock.toggleClass('open');
        }
    });


    //клик вне корзины всегда ее закрывает, клик вне выпадающего меню закрывает его
    $(document).on('click', function (e) {
        if (
            ( !cartBlock.is(e.target) // if the target of the click isn't the container...
                && cartBlock.has(e.target).length === 0 )// ... nor a descendant of the container

        )
        {
            cartBlock.removeClass('open');
        }

        if (
            ( !accBlock.is(e.target)
                && accBlock.has(e.target).length === 0 )

        )
        {
            accBlock.removeClass('open');
        }

    });

    //кнопка скрола наверх страницы в футере
    $('.js__to-top-button').on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 500);
    });


    //запускает видео через plyr, на блоке должен быть класс .js__trigger-video
    //plyr.setup('.js__trigger-video',{ });
    players = Plyr.setup('.js__trigger-video');

    //замена стандартных селектов на те что в дизайне
    $('.moto-select').select2({
        //closeOnSelect: false,
        containerCssClass: 'moto__select_inner-selection',
        dropdownCssClass: 'moto__select_inner-dropdown',
        minimumResultsForSearch: Infinity
    });

    // фильтры в виде слайдера, опции в html
    // доки http://ionden.com/a/plugins/ion.rangeSlider/index.html
    $(".moto-range-input").ionRangeSlider({
        //обновляется само
        // onFinish: function (data) { //обновить значение изначального input
        //     $(data.input[0]).attr('value',data.from_value);
        // }
    });


    //переключение значения инпута в компоненте с кнопками + и -
    $('.input-count__button').click(function(e){
        e.preventDefault();

        var inputCountBlock = $(this).closest('.input-count');
        var type      = $(this).attr('data-type');
        var input = inputCountBlock.find('.input-count__input');
        var currentVal = parseInt(input.val());

        if (!isNaN(currentVal)) {
            if(type == 'minus') {
                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
            } else if(type == 'plus') {
                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
            }
        } else {
            input.val(0);
        }
    });


    //переключение подменю в главном меню на 767px-
    $('.main-menu__item.has-children .main-menu__link').on('click', function(e) {
        if ( window.matchMedia('(max-width: 767px)').matches ) {
            e.preventDefault();
            var li = $(this).closest('.main-menu__item');
            var thisSubmenu = li.find('.main-menu__submenu');

            //свернуть остальные
            li.closest('.main-menu__inner').find('.main-menu__item').not(li).removeClass('active');
            li.closest('.main-menu__inner').find('.main-menu__submenu').not( thisSubmenu ).slideUp('fast');

            //переключить нажатый
            li.toggleClass('active');
            thisSubmenu.slideToggle('fast');
        }
    });

    //================================================================================================== ГЛАВНАЯ

    //слайдер на главной
    $('.home-slider__slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: $('.home-slider__arrow_prev'),
        nextArrow: $('.home-slider__arrow_next'),
        dots: true,
        appendDots: $('.home-slider__slider-outer'),
        autoplay: false,
        autoplaySpeed: 5000
    });

    //кастомный скролл внутри блока с текстом внутри таба
    //https://github.com/malihu/malihu-custom-scrollbar-plugin
    $(".advices__text-block").mCustomScrollbar({
        theme:"dark-3",
        scrollInertia: 200//хз
    });

    //обновление текста в лейбле возле инпута-полоски
    $('.js__front-range-input').on('change', function () {
        var $this = $(this),
            value = $this.prop("value").split(";"),
            unit = $this.data('unit');

        if ($this.hasClass('js__prices')){ //если цена то форматировать
            $this.closest('.js__range-block').find('.js__range-values').text(value[0].replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')+'-'+value[1].replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')+' '+unit);
        } else {
            $this.closest('.js__range-block').find('.js__range-values').text(value[0]+'-'+value[1]+' '+unit);
        }

    });

    //переключение выпадающих кусков в слайдере на главной
    $('.home-slider__more').on('click', function () {
        if ($(this).closest('.home-slider__more').hasClass('open')){
            $(this).closest('.home-slider__more').removeClass('open');
        } else {
            $(this).closest('.home-slider__slide-inner').find('.home-slider__more').removeClass('open');
            $(this).closest('.home-slider__more').addClass('open');
        }
    });

    //================================================================================================== КАТАЛОГ

    //нажатие на стрелку закрывает блок фильтра
    $('.filters__block-toggle').on('click', function () {
        //может дергаться, но не должно
        $(this).closest('.filters__block').find('.filters__block-content').slideToggle('fast');
        var $this = $(this);
        setTimeout(function () {
            $this.closest('.filters__block').toggleClass('open');
        }, 200);

    });

    //обновление текста в лейбле возле инпута-полоски
    $('.js__catalog-range').on('change', function () {
        var $this = $(this),
            value = $this.prop("value").split(";"),
            unit = $this.data('unit');

        if ($this.hasClass('js__prices')){ //если отмечена как цена то добавлять пробел между разрядами
            $this.closest('.js__range-block')
                .find('.js__range-values')
                .text(value[0].replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')+'-'+value[1].replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ')+' '+unit);
        } else {
            $this.closest('.js__range-block').find('.js__range-values').text(value[0]+'-'+value[1]+' '+unit);
        }

    });

    //переключение фильтров на узких мобилах
    $('.catalog__filters-toggle').on('click', function () {
        $(this).closest('.catalog__filters').find('.filters').slideToggle();
        if ($(this).text() === 'Развернуть фильтры'){
            $(this).text('Свернуть фильтры');
        } else if ($(this).text() === 'Свернуть фильтры') {
            $(this).text('Развернуть фильтры');
        }
    });


    //================================================================================================== СТРАНИЦА ТОВАРА

    //слайдер на изображений товара
    var productGallerySlick = $('.js__products-img-slider').slick({
        infinite: true, //нужен чтобы slickGoTo переходил на любой слайд, иначе на последних не работает
        slidesToShow: 5,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: $('.product-imgs__arrow_prev'),
        nextArrow: $('.product-imgs__arrow_next'),
        dots: false,
        autoplay: false,
        vertical: true,
        responsive: [
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 5
                }
            }
        ]
    });

    //при нажатии стрелок переключить полное изображение
    $('.js__products-img-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        //console.log('start beforechange');
        changeFullImg(nextSlide);
    });

    //ручное переключение при нажатии на миниатюру, можно сделать как asNavFor но так будет проще дорабатывать потом
    $('.product-imgs__slide').on('click', function () {
        var slick = productGallerySlick[0].slick;
        var newSlideIndex = $(this).data('slick-index');
        //console.log(slick);

        //если слайдов меньше чем должно быть для прокрутки то они уходят за край видимости
        //поэтому костыль который проверяет достаточное количество слайдов
        if (slick.slideCount > slick.options.slidesToShow ) {
            $(this).closest('.js__products-img-slider').slick('slickGoTo', newSlideIndex);//переключить слайдер
        } else { //и если недостаточно то вместо перелистывания просто перевешивается класс активного слайда
            var currentIndex =  $('.product-imgs__slide.slick-current').data('slick-index');
            //console.log('current slide:' +currentIndex+ " , nextSlide:"+ newSlideIndex);
            $('.product-imgs__slide[data-slick-index="'+ currentIndex +'"]').removeClass('slick-current');
            $('.product-imgs__slide[data-slick-index="'+ newSlideIndex +'"]').addClass('slick-current');
        }
        changeFullImg(newSlideIndex);//изменить полную картинку
    });

    //получает номер нового слайда, берет путь из data-full и меняет полное изображение
    function changeFullImg(newSlideIndex) {
        var newImgSrc = $('.product-imgs__slide[data-slick-index="'+ newSlideIndex +'"]').data('full');
        $('.product-imgs__full-img-block').find('.product-imgs__full-img').attr('src', newImgSrc);//поменять изображение
    }


    //слайдер кроссселов на странице товара (или на других страницах если кроссселы будут на других,
    // но два на одной странице не будут рабоать правильно если не менять классы)
    $('.js__product-cross-sells-slider').slick({
        infinite: true, //нужен чтобы slickGoTo переходил на любой слайд, иначе на последних не работает
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: $('.cross-sells__arrow_prev'),
        nextArrow: $('.cross-sells__arrow_next'),
        dots: false,
        autoplay: false,
        responsive: [
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    });

    if ( $('.product-info__custom-color').length > 0 ){
        $('.product-info__color-input').on('change', function () {
            $('.product-info__custom-color').removeClass('selected');
            $(this).find('.product-info__custom-color-input').val(0);

            $('.product-info__custom-color-holder').slideUp(400, function () {
                $('.product-info__custom-color-holder').detach().appendTo('.product-info__hidden');
            });

        });
    }

    //нажатие на кнопку "свой цвет" убирает выделение с цветов и добавляет поля для своего цвета
    $('.product-info__custom-color').on('click', function () {
        $(this).closest('.product-info__colors').find('.product-info__color-input').prop( "checked", false );

        $(this).addClass('selected');
        $(this).find('.product-info__custom-color-input').val(1);

        $('.product-info__custom-color-holder').detach().appendTo('.product-info__custom-color-block');
        $('.product-info__custom-color-holder').slideDown();
    });

    //стилизация инпута файлов под кнопку, если будет работать криво можно убрать и оставить обычный file input
    $('.product-info__custom-color-file').bootstrapFileInput();

    //================================================================================================== ЧЕКАУТ
    //подстановка нужных полей в форму при выборе физ. или юр. лица
    $(".js__checkout-type-radio").on('change', function() {

        var isUr = ($(this).hasClass('js__checkout-type-radio_2')),
            urBlock = $('.js__checkout-ur-block'),
            blockToInsert = $('.checkout1__legal-entity'),
            hiddenBlock = $('.checkout1__hidden-block');

        if ( isUr ){
            urBlock.prependTo(blockToInsert);
        } else {
            urBlock.prependTo(hiddenBlock);
        }
    });

    //================================================================================================== ПРОФИЛЬ

    //выпадающий дейтпикер на странице заказов в профиле
    //https://chmln.github.io/flatpickr/
    var pickr = $(".acc1-header__date-input").flatpickr({
        animate: false,
        "locale": "ru",
        dateFormat: "d.m.Y",
        disableMobile: false,
        mode: 'range',
        onChange: function(selectedDates, dateStr, instance) { //из за того что в дизайне календарь не в инпуте приходиться вручную изменять значения и тексты
            //console.log(dateStr);
            $('.acc1-header__date-text').text(dateStr);
        },
        onOpen: function(selectedDates, dateStr, instance) {
            $('.acc1-header__date').addClass('acc1-header__date_opened');
        },
        onClose: function(selectedDates, dateStr, instance) {
            $('.acc1-header__date').removeClass('acc1-header__date_opened');
            $('.acc1-header__date-input').trigger('close');
        },
        onReady: function(dateObj, dateStr, instance) { //добавление кнопки очистить, на прошлых проектах просили это
            var $cal = $(instance.calendarContainer);
            if ($cal.find('.flatpickr-clear').length < 1) {
                $cal.append('<div class="flatpickr-clear" style="cursor: pointer">Очистить</div>');
                $cal.find('.flatpickr-clear').on('click', function() {
                    instance.clear();
                    instance.close();
                    $('.acc1-header__date-text').text('Выбрать период');
                });
            }
        }
    });

    //переключение выпадающего блока в списке заказов
    $('.acc1-item__toggler').on('click', function () {
       var item = $(this).closest('.acc1-item');
       item.toggleClass('acc1-item_open');
       item.find('.acc1__sub').slideToggle();
    });

    //выбор даты рождения на странице профиля
    var pickr2 = $(".js__acc-profile-birth").flatpickr({
        animate: false,
        "locale": "ru",
        dateFormat: "d.m.Y",
        disableMobile: false,
        minDate: '01.01.1940',
        maxDate: "today",
        allowInput: true,
        onReady: function(dateObj, dateStr, instance) {
            var $cal = $(instance.calendarContainer);
            if ($cal.find('.flatpickr-clear').length < 1) {
                $cal.append('<div class="flatpickr-clear" style="cursor: pointer">Очистить</div>');
                $cal.find('.flatpickr-clear').on('click', function() {
                    instance.clear();
                    instance.close();
                    $('.acc1-header__date-text').text('Выбрать период');
                });
            }
        }
    });

    //================================================ ОТЗЫВЫ

    //звезды на отзывах
    $(".review__rateyo").rateYo({ });
    //рейтинг нового отзыва на форме
    $(".new-review__rateyo").rateYo({
        onSet: function (rating, rateYoInstance) { //заполнение скрытого инпута рядом
            $(this).siblings('.new-review__rating-input').attr('value', rating);
        }
    });

    //загрузка фото в новый отзыв сразу при выборе в инпуте, если не нужно то можно убрать
    //https://stackoverflow.com/questions/4459379/preview-an-image-before-it-is-uploaded
    $(".new-review__file-input").on('change',function() {
        readNewReviewAvatarURL(this);
    });
    function readNewReviewAvatarURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.new-review__avatar').css('background-image', 'url(' + e.target.result + ')');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

});//end of $doc ready function

//может пригодится
function is_touch_device() {
    return 'ontouchstart' in window        // works on most browsers
        || navigator.maxTouchPoints;       // works on IE10/11 and Surface
}


//test is mobile device
// var isMobile = false;
// if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
//     isMobile = true;
// }