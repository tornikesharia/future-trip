<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION, $USER;

$arResult = array(
	"CUR_URI" => "",
	"MESSAGE" => "",
	"ERROR" => array(),
	"TYPE" => "ERROR"
);

if($_POST['USER_TYPE']=='individual'){

	if (!empty($_POST['NAME']) && !empty($_POST['EMAIL']) && !empty($_POST['PASSWORD']) && !empty($_POST['CONFIRM_PASSWORD'])){

		foreach ($_POST as $key => $value) {
			$arData[$key] = addslashes(strip_tags($value));
			switch ($key) {
				case 'EMAIL':
					if (!empty($arData['EMAIL'])) {
						$value = $arData['EMAIL'];
						if (!preg_match("/^[-0-9a-z_\.]+@[-0-9a-z^\.]+\.[a-z]{2,4}$/i", $value)) {
							$arResult['ERROR'][] = array(
								"REQUIRED_FIELDS",
								"Формат почты неправильно заполнено!"
							);
						}
					}		
					break;
				case 'PERSONAL_PHONE':
					if (!empty($value)) {
						$poneArray = Array('+', '(', ')', ' ', '-');
						foreach ($poneArray as $pArray) {
							$value = str_replace($pArray, "", $value);
						}
						if (!is_numeric($value)) {
							$arResult['ERROR'][] = array(
								'DATA',
								'Ошибка, в поле "Номер телефона" должны быть только цифры'
							);
						} else if (iconv_strlen($value) < 6) {
							$arResult['ERROR'][] = array(
								'DATA',
								'Ошибка, в поле "Номер телефона" не менее 6 символов'
							);
						} else if (iconv_strlen($value) > 15) {
							$arResult['ERROR'][] = array(
								'DATA',
								'Ошибка, в поле "Номер телефона" не более 15 символов'
							);
						}
					}
					break;					
			}
		}

		if (count($arResult["ERROR"]) <= 0) {

			$arResult = $USER->Register($arData['EMAIL'], $arData['NAME'], $arData['SURNAME'], $arData['PASSWORD'], $arData['CONFIRM_PASSWORD'],$arData['EMAIL']);
			if($USER->GetID()){
				$PERSONAL_PHONE = trim($_POST['PERSONAL_PHONE']);
				$fields = Array(
				  "PERSONAL_PHONE" => ($PERSONAL_PHONE ? $PERSONAL_PHONE :"")
				  );
				$USER->Update($USER->GetID(), $fields);
			}		
			$arResult['MESSAGE'] = str_replace("Пользователь с логином", "Пользователь с email", $arResult['MESSAGE']);
			$arResult["CUR_URI"] = ($arData['CUR_URL'] ? $arData['CUR_URL'] : "/");	

		} else {
			$html_error = "";
			foreach ($arResult["ERROR"] as $error){
				$html_error .= '<div style="color:#f00;">'.$error[1].'</div>'."\n";
			}

			$arResult["MESSAGE"] = $html_error;
			$arResult["TYPE"] = "ERROR";
		}

	} else {
			$arResult["MESSAGE"] = "Не заполнены обязательные поля";
			$arResult["TYPE"] = "ERROR";
	}
}

if($_POST['USER_TYPE'] == 'legal'){

	if (!empty($_POST['WORK_COMPANY']) && !empty($_POST['UF_INN']) && !empty($_POST['UF_LEGAL_ADDRESS']) && !empty($_POST['UF_PHYSICAL_ADDRESS']) && !empty($_POST['EMAIL']) && !empty($_POST['PERSONAL_PHONE']) && !empty($_POST['PASSWORD']) && !empty($_POST['CONFIRM_PASSWORD'])){

		foreach ($_POST as $key => $value) {
			$arData[$key] = addslashes(strip_tags($value));
			switch ($key) {
				case 'EMAIL':
					if (!empty($arData['EMAIL'])) {
						$value = $arData['EMAIL'];
						if (!preg_match("/^[-0-9a-z_\.]+@[-0-9a-z^\.]+\.[a-z]{2,4}$/i", $value)) {
							$arResult['ERROR'][] = array(
								"DATA",
								"Формат почты неправильно заполнено!"
							);
						}
					}		
					break;
				case 'PERSONAL_PHONE':
					if (!empty($value)) {
						$poneArray = Array('+', '(', ')', ' ', '-');
						foreach ($poneArray as $pArray) {
							$value = str_replace($pArray, "", $value);
						}
						if (!is_numeric($value)) {
							$arResult['ERROR'][] = array(
								'DATA',
								'Ошибка, в поле "Номер телефона" должны быть только цифры'
							);
						} else if (iconv_strlen($value) < 6) {
							$arResult['ERROR'][] = array(
								'DATA',
								'Ошибка, в поле "Номер телефона" не менее 6 символов'
							);
						} else if (iconv_strlen($value) > 15) {
							$arResult['ERROR'][] = array(
								'DATA',
								'Ошибка, в поле "Номер телефона" не более 15 символов'
							);
						}
					}
					break;
                case 'UF_INN':
					if (!is_numeric($value)) {
						$arResult['ERROR'][] = array(
							'DATA',
							'Ошибка, в поле "Инн" должны быть только цифры'
						);
					} else if (iconv_strlen($value) < 10) {
						$arResult['ERROR'][] = array(
							'DATA',
							'Ошибка, в поле "Инн" не менее 10 символов'
						);
					} else if (iconv_strlen($value) > 12) {
						$arResult['ERROR'][] = array(
							'DATA',
							'Ошибка, в поле "Инн" не более 12 символов'
						);
					}

                    break;					
			}
		}

		if (count($arResult["ERROR"]) <= 0) {

			$arResult = $USER->Register($arData['EMAIL'], $arData['NAME'], $arData['SURNAME'], $arData['PASSWORD'], $arData['CONFIRM_PASSWORD'], $arData['EMAIL']);
			if($USER->GetID()){
				
				$WORK_COMPANY = trim($arData['WORK_COMPANY']);
				$UF_INN = trim($arData['UF_INN']);
				$UF_KPP = trim($arData['UF_KPP']);
				$UF_LEGAL_ADDRESS = trim($arData['UF_LEGAL_ADDRESS']);
				$UF_PHYSICAL_ADDRESS = trim($arData['UF_PHYSICAL_ADDRESS']);
				$PERSONAL_PHONE = trim($arData['PERSONAL_PHONE']);
				
				$fields = Array(
					"PERSONAL_PHONE" => ($PERSONAL_PHONE ? $PERSONAL_PHONE : ""),
					"WORK_COMPANY" => ($WORK_COMPANY ? $WORK_COMPANY : ""),
					"UF_INN" => ($UF_INN ? $UF_INN : ""),
					"UF_KPP" => ($UF_KPP ? $UF_KPP : ""),
					"UF_LEGAL_ADDRESS" => ($UF_LEGAL_ADDRESS ? $UF_LEGAL_ADDRESS : ""),
					"UF_PHYSICAL_ADDRESS" => ($UF_PHYSICAL_ADDRESS ? $UF_PHYSICAL_ADDRESS : "")
				);
				$USER->Update($USER->GetID(), $fields);
			}		
			$arResult['MESSAGE'] = str_replace("Пользователь с логином", "Пользователь с email", $arResult['MESSAGE']);
			$arResult["CUR_URI"] = ($arData['CUR_URL'] ? $arData['CUR_URL'] : "/");	

		} else {
			$html_error = "";
			foreach ($arResult["ERROR"] as $error){
				$html_error .= '<div style="color:#f00;">'.$error[1].'</div>'."\n";
			}

			$arResult["MESSAGE"] = $html_error;
			$arResult["TYPE"] = "ERROR";
		}

	} else {
			$arResult["MESSAGE"] = "Не заполнены обязательные поля";
			$arResult["TYPE"] = "ERROR";
	}
}


echo json_encode($arResult);

//Lib::Debug($arResult,false,true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>