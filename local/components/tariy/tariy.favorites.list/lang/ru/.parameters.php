<?php
$MESS["SORT_BY1"] = "Поле для первой сортировки";
$MESS["SORT_ORDER1"] = "Направление для первой сортировки";
$MESS["SORT_BY2"] = "Поле для второй сортировки";
$MESS["SORT_ORDER2"] = "Направление для второй сортировки";
$MESS["T_IBLOCK_DESC_FID"] = "ID";
$MESS["T_IBLOCK_DESC_FNAME"] = "Название";
$MESS["T_IBLOCK_DESC_FACT"] = "Дата начала активности";
$MESS["T_IBLOCK_DESC_FSORT"] = "Сортировка";
$MESS["T_IBLOCK_DESC_FTSAMP"] = "Дата последнего изменения";
$MESS["T_IBLOCK_DESC_ASC"] = "По возрастанию";
$MESS["T_IBLOCK_DESC_DESC"] = "По убыванию";