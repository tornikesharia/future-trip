<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult['USER']['PERSONAL_PHOTO']):
	$renderImage = CFile::ResizeImageGet($arResult['USER']['PERSONAL_PHOTO'], Array("width" => $arParams['PERSONAL_PHOTO_WIDTH'], "height" => $arParams['PERSONAL_PHOTO_HEIGHT']), BX_RESIZE_IMAGE_EXACT, true);
	$arResult['USER']['PERSONAL_PHOTO'] = [];
	$arResult['USER']['PERSONAL_PHOTO']['SRC'] = $renderImage['src'];
	$arResult['USER']['PERSONAL_PHOTO']['WIDTH'] = $renderImage['width'];
	$arResult['USER']['PERSONAL_PHOTO']['HEIGHT'] = $renderImage['height'];
	$arResult['USER']['PERSONAL_PHOTO']['FILE_SIZE'] = $renderImage['size'];
endif;