<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div id="favoritesBlock">    
        <?php foreach( $arResult["ITEMS"] as $key => $arItem ): ?>
                <div class="favorites--items" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["src"]?>);">
                        <?$APPLICATION->IncludeComponent(
                                "tariy:tariy.favorites.button",
                                "",
                                Array(
                                        "CACHE_TIME" => "3600",
                                        "CACHE_TYPE" => "A",
                                        "ELEMENT_ID" => "{$arItem["ID"]}"
                                )
                        );?>
                        <?=$arItem["NAME"]?>
                </div>
        <?php endforeach; ?>
</div>
