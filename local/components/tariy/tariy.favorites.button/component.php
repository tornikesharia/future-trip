<?php session_start();
$arResult["MODULES_NAME"] = "tariy.favorites";

if ($_REQUEST["KEY"] == "PrintAjax") {

    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule($arResult["MODULES_NAME"]);
    global $USER, $TPrintFavorites, $APPLICATION;
    $timeCookie = time() + (60 * 60 * 24 * COption::GetOptionString("tariy.favorites", "T_FAVORITES_CONFIGURATION_P3", "365"));//Записываем на кол-во дней
    $TPrintFavorites = TMySqlFavorites::FavoritesSelect();//Данные из БД

    //Выбираем значения понравившихся "ID элементов" в БД или куках, в зависимости от того авторизован ли пользователь, или нет.
    //Проверка на авторизацию. проверяем авторизован ли пользователь
    if ($USER->IsAuthorized()) {
        $idArray = (!empty($TPrintFavorites["FAVORITES_ID"]) ? $TPrintFavorites["FAVORITES_ID"] : $APPLICATION->get_cookie("T_FAVORITES_ID"));
    } else {
        //$idArray = $APPLICATION->get_cookie("T_FAVORITES_ID");
        $idArray = $_COOKIE["T_FAVORITES_ID_NEW"];
    }
    //Пропускаем значение через класс, для удаления или добавления ID
    $idNewArray = TMyClass::ArrayWithString($_REQUEST["ELEMENT_ID"], $idArray);
    //Перезаписываем данные
    //Проверка на авторизацию. проверяем авторизован ли пользователь
    if ($USER->IsAuthorized()) {
        //$APPLICATION->set_cookie("T_FAVORITES_ID", $idNewArray, $timeCookie);
        setcookie("T_FAVORITES_ID_NEW", $idNewArray, $timeCookie);
        //Проверяем есть ли у пользователя понравившиеся элементы, или нет
        if ($TPrintFavorites) {
            //Если пользователь есть, перезапишем его данные
            TMySqlFavorites::FavoritesUpdate($idNewArray);
        } else {
            //Если пользователя нет, создадим для него строку
            TMySqlFavorites::FavoritesInsert($idNewArray);
        }
    } else {
        //$APPLICATION->set_cookie("T_FAVORITES_ID", $idNewArray, $timeCookie);
        setcookie("T_FAVORITES_ID_NEW", $idNewArray, $timeCookie);
    }

    echo json_encode( $idNewArray );

} else if ($_REQUEST["KEY"] == "PrintAjax2") {

    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
    CModule::IncludeModule($arResult["MODULES_NAME"]);
    global $USER, $TPrintFavorites;
    $TPrintFavorites = TMySqlFavorites::FavoritesSelect();//Данные из БД

    //Выбираем значения понравившихся "ID элементов" в БД или куках, в зависимости от того авторизован ли пользователь, или нет.
    //Проверка на авторизацию. проверяем авторизован ли пользователь
    if ($USER->IsAuthorized()) {
        $idArray = (!empty($TPrintFavorites["FAVORITES_ID"]) ? $TPrintFavorites["FAVORITES_ID"] : $APPLICATION->get_cookie("T_FAVORITES_ID"));
    } else {
        //$idArray = $APPLICATION->get_cookie("T_FAVORITES_ID");
        $idArray = $_COOKIE["T_FAVORITES_ID_NEW"];
    }
    $arResult = explode(":", $idArray);

    echo json_encode($arResult);

} else {

    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
    if (CModule::IncludeModule($arResult["MODULES_NAME"])) {
        global $TPrintFavorites;

        $arResult["ELEMENT_ID"] = $arParams["ELEMENT_ID"];
        if ($USER->IsAuthorized()) {
            $cookie = (!empty($TPrintFavorites["FAVORITES_ID"]) ? $TPrintFavorites["FAVORITES_ID"] : $APPLICATION->get_cookie("T_FAVORITES_ID"));
        } else {
            //$cookie = $APPLICATION->get_cookie("T_FAVORITES_ID");
            $cookie = $_COOKIE["T_FAVORITES_ID_NEW"];
        }
        $arResult["COOKIE"] = explode(":", $cookie);

//                if ( $this->StartResultCache(0) ):
        $this->AbortResultCache();
        $this->IncludeComponentTemplate();
//                endif;
    }

}