<?
define("BS_FORM_TYPE", 'QUESTIONS'); //Тип почтового события
define("BS_FORM_TEMPLATE", 85); // Шаблон почтового события
define("NO_AGENT_CHECK", true);//Отключаем выполнение агентов, при выполнении данного скрипта
define("NO_AGENT_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


global $APPLICATION, $USER;

$arResult = array(
    "RESULT" => "OK",
    "ERROR" => array()
);

$arDataNew = Array(
    "NAME" => $_REQUEST["NAME"],
    "PHONE" => $_REQUEST["PHONE"],
	//"EMAIL" => $_REQUEST["EMAIL"],
	//"MESSAGE" => $_REQUEST["MESSAGE"],
    "URL" => $_REQUEST["URL"],
);
foreach ($arDataNew as $key => $value) {
    $arData[$key] = addslashes(strip_tags($value));
    switch ($key) {
        case 'NAME':
            if (empty($value)) {
                $arResult['ERROR'][] = array(
                    "REQUIRED_FIELDS",
                    "Не заполнено обязательное поле \"Ваше имя\"!"
                );
            } else {
                if (Lib::ContainsNumbers($value) === true) {
                    $arResult['ERROR'][] = array(
                        "REQUIRED_FIELDS",
                        "Поле \"Ваше имя\" должно содержать только буквы"
                    );
                }
            }
            break;
        case 'MESSAGE':
            if (empty($value)) {
                $arResult['ERROR'][] = array(
                    "REQUIRED_FIELDS",
                    "Не заполнено обязательное поле \"Ваше сообщение\"!"
                );
            } 
            break;			
        case 'PHONE':
            if (empty($value)) {
                $arResult['ERROR'][] = array(
                    "REQUIRED_FIELDS",
                    "Не заполнено обязательное поле \"Укажите Ваш телефон\"!"
                );
            } else {
                $poneArray = Array('+', '(', ')', ' ', '-');
                foreach ($poneArray as $pArray) {
                    $value = str_replace($pArray, "", $value);
                }
                if (!is_numeric($value)) {
                    $arResult['ERROR'][] = array(
                        "REQUIRED_FIELDS",
                        'Ошибка, в поле "Укажите Ваш телефон" должны быть только цифры'
                    );
                } else if (iconv_strlen($value) < 6) {
                    $arResult['ERROR'][] = array(
                        "REQUIRED_FIELDS",
                        'Ошибка, в поле "Укажите Ваш телефон" не менее 6 символов'
                    );
                } else if (iconv_strlen($value) > 16) {
                    $arResult['ERROR'][] = array(
                        "REQUIRED_FIELDS",
                        'Ошибка, в поле "Укажите Ваш телефон" не более 16 символов'
                    );
                }
            }
            break;
    }
}

if (empty($arResult["ERROR"])) {

    $arFields = array(
        "EMAIL_FROM" => 'Futuretrip@' . Lib::GetDomainName(),
        "NAME" => $arData["NAME"],
        "PHONE" => $arData["PHONE"],
		"EMAIL" => $arData["EMAIL"],
		"MESSAGE" => $arData["MESSAGE"],
        "URL" => $arData["URL"],
        "DATE" => date("d/m/Y G:i:s", time() + CTimeZone::GetOffset()) // Текущее время с учетом часового пояса
    );

    //Отправляем почтовое событие
    if (CEvent::Send(BS_FORM_TYPE, SITE_ID, $arFields, true, BS_FORM_TEMPLATE)) {

        $arResult["RESULT_DETAIL"] = '<div style="padding:50px 0; font-size:25px;"><strong>Cпасибо за заявку, менеджер свяжется с вами в ближайшее время.</strong></div>';

        $arResult["RESULT"] = "OK";
    } else {
        $arResult["ERROR"][] = array("EVENT_FORM", "Произошла ошибка почтовой системы!");
    }

} else {

    $arResult["RESULT"] = "ERROR";
    $arResult["RESULT_DETAIL"] = '';
    foreach ($arResult["ERROR"] as $error) {
        $arResult["RESULT_DETAIL"] .= '<span style="color:red">' . $error[1] . '</span><br>';
    }
}

/** @global CMain $APPLICATION */
if (strtolower(SITE_CHARSET) != 'utf-8')
    $arResult = $APPLICATION->ConvertCharsetArray($arResult, SITE_CHARSET, 'utf-8');

header('Content-Type: application/json');
echo json_encode($arResult);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");