<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка и оплата");
?>


                        <div class="payment__block">
                            <div class="payment__title">Доставка</div>
                            <div class="row">
                                <div class="col-sm-6 col-lg-4">
                                    <div class="payment__item">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/delivery1.svg" alt="" class="payment__item-icon">
                                        <div class="payment__item-title"><?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/curier_name_1.php", Array(), Array("MODE" => "text"));?></div>
                                        <div class="payment__item-text"><?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/curier_text_1.php", Array(), Array("MODE" => "text"));?></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="payment__item">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/delivery2.svg" alt="" class="payment__item-icon">
                                        <div class="payment__item-title"><?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/curier_name_2.php", Array(), Array("MODE" => "text"));?></div>
                                        <div class="payment__item-text"><?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/curier_text_2.php", Array(), Array("MODE" => "text"));?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="payment__block">
                            <div class="payment__title">Оплата</div>
                            <div class="row">
                                <div class="col-sm-6 col-lg-4">
                                    <div class="payment__item">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/payment1.svg" alt="" class="payment__item-icon">
                                        <div class="payment__item-title"><?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/payment_name_1.php", Array(), Array("MODE" => "text"));?></div>
                                        <div class="payment__item-text"><?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/payment_text_1.php", Array(), Array("MODE" => "text"));?></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="payment__item">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/payment2.svg" alt="" class="payment__item-icon">
                                        <div class="payment__item-title"><?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/payment_name_2.php", Array(), Array("MODE" => "text"));?></div>
                                        <div class="payment__item-text"><?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/payment_text_2.php", Array(), Array("MODE" => "text"));?></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-4">
                                    <div class="payment__item">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/payment3.svg" alt="" class="payment__item-icon">
                                        <div class="payment__item-title"><?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/payment_name_3.php", Array(), Array("MODE" => "text"));?></div>
                                        <div class="payment__item-text"><?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/payment_text_3.php", Array(), Array("MODE" => "text"));?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="payment__title">Оплата</div>
                        <div class="payment__text html-content">
						<?$APPLICATION->IncludeFile(__include__ . "delivery-and-payment/payment_big_text.php", Array(), Array("MODE" => "html"));?>
                        </div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>