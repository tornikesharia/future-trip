<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои заказы");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>
<?$APPLICATION->IncludeComponent(
	"dev:sale.personal.order.list", 
	"main", 
	array(
		"STATUS_COLOR_N" => "green",
		"STATUS_COLOR_P" => "yellow",
		"STATUS_COLOR_F" => "gray",
		"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
		"PATH_TO_DETAIL" => "order_detail.php?ID=#ID#",
		"PATH_TO_COPY" => "basket.php",
		"PATH_TO_CANCEL" => "order_cancel.php?ID=#ID#",
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_PAYMENT" => "payment.php",
		"ORDERS_PER_PAGE" => "20",
		"ID" => $ID,
		"SET_TITLE" => "Y",
		"SAVE_IN_SESSION" => "Y",
		"NAV_TEMPLATE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"CACHE_GROUPS" => "Y",
		"HISTORIC_STATUSES" => array(
		),
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"COMPONENT_TEMPLATE" => "main",
		"PATH_TO_CATALOG" => "/catalog/",
		"RESTRICT_CHANGE_PAYSYSTEM" => array(
			0 => "F",
			1 => "P",
		),
		"REFRESH_PRICES" => "Y",
		"DEFAULT_SORT" => "STATUS"
	),
	false
);?>

<?/* $APPLICATION->IncludeComponent(
	"bitrix:sale.personal.order.list", 
	"bootstrap_v4", 
	array(
		"STATUS_COLOR_N" => "green",
		"STATUS_COLOR_P" => "yellow",
		"STATUS_COLOR_F" => "gray",
		"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
		"PATH_TO_DETAIL" => "order_detail.php?ID=#ID#",
		"PATH_TO_COPY" => "basket.php",
		"PATH_TO_CANCEL" => "order_cancel.php?ID=#ID#",
		"PATH_TO_BASKET" => "/personal/cart/",
		"PATH_TO_PAYMENT" => "payment.php",
		"ORDERS_PER_PAGE" => "20",
		"ID" => $ID,
		"SET_TITLE" => "Y",
		"SAVE_IN_SESSION" => "Y",
		"NAV_TEMPLATE" => "",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600000",
		"CACHE_GROUPS" => "Y",
		"HISTORIC_STATUSES" => array(
		),
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"COMPONENT_TEMPLATE" => "bootstrap_v4",
		"PATH_TO_CATALOG" => "/catalog/",
		"RESTRICT_CHANGE_PAYSYSTEM" => array(
			0 => "0",
		),
		"REFRESH_PRICES" => "Y",
		"DEFAULT_SORT" => "STATUS"
	),
	false
); 
						


							<div class="acc1">
								<div class="acc1__orders">
									<div class="acc1__orders-header">
										<div class="acc1__header-title acc1__title">Наименование</div>
										<div class="acc1__header-sum acc1__sum">Сумма</div>
										<div class="acc1__header-bonus acc1__bonus">Оплата бонусами</div>
										<div class="acc1__header-qty acc1__qty">Кол-во</div>
										<div class="acc1__header-status acc1__status">Cтатус</div>
									</div>
				
									<div class="acc1-item">
										<div class="acc1-item__info-block acc1__title">
											<div class="acc1-item__date">09.11.2017</div>
											<div class="acc1-item__number">№102</div>
											<div  class="acc1-item__toggler">
												<div class="acc1-item__toggler-text">Комплект из 3 товаров</div>
												<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/angle-bottom-yellow.svg" alt="" class="acc1-item__toggler-icon">
											</div>
										</div>
										<div class="acc1-item__sum-block acc1__sum">
											<div class="acc1-item__price">180 000 ₽</div>
											<div class="acc1-item__price-bonus">+ 10000 бонусов</div>
										</div>
										<div class="acc1-item__bonus-block acc1__bonus">
											<div class="acc1-item__bonus">- 10000 бонусов</div>
										</div>
										<div class="acc1-item__qty-block acc1__qty">
											<div class="input-count input-count_disabled acc1-item__count-block ">
												<div class="input-group">
													<input type="text"
														   class="form-control input-count__input"
														   value="1" disabled>
												</div>
											</div>
										</div>
										<div class="acc1-item__status-block acc1__status">
											<div class="acc1-item__status">В обработке</div>
										</div>
				
				
										<div class="acc1__sub">
											<div class="acc1-item">
												<div class="acc1-item__info-block acc1__title">
													<a href="#" class="acc1-item__title">Электроцикл Denzel Rush I</a>
													<div class="acc1-item__attr">Артикул: <span>6500900</span></div>
													<div class="acc1-item__attr">Цвет: <span>Черный</span></div>
												</div>
												<div class="acc1-item__sum-block acc1__sum">
													<div class="acc1-item__price">135 999 ₽</div>
													<div class="acc1-item__price-bonus">+ 10000 бонусов</div>
												</div>
												<div class="acc1-item__bonus-block acc1__bonus">
													<div class="acc1-item__bonus">- 5000 бонусов</div>
												</div>
												<div class="acc1-item__qty-block acc1__qty">
													<div class="input-count input-count_disabled acc1-item__count-block ">
														<div class="input-group">
															<input type="text"
																   class="form-control input-count__input"
																   value="1" disabled>
														</div>
													</div>
												</div>
												<div class="acc1-item__status-block acc1__status"></div>
											</div>
											<div class="acc1-item">
												<div class="acc1-item__info-block acc1__title">
													<a href="#" class="acc1-item__title">Шлем кроссовый BELL SX-1</a>
													<div class="acc1-item__attr">Артикул: <span>6500900</span></div>
													<div class="acc1-item__attr">Цвет: <span>Черный</span></div>
												</div>
												<div class="acc1-item__sum-block acc1__sum">
													<div class="acc1-item__price">25 000 ₽</div>
													<div class="acc1-item__price-bonus">+ 10000 бонусов</div>
												</div>
												<div class="acc1-item__bonus-block acc1__bonus">
													<div class="acc1-item__bonus">- 4000 бонусов</div>
												</div>
												<div class="acc1-item__qty-block acc1__qty">
													<div class="input-count input-count_disabled acc1-item__count-block ">
														<div class="input-group">
															<input type="text"
																   class="form-control input-count__input"
																   value="1" disabled>
														</div>
													</div>
												</div>
												<div class="acc1-item__status-block acc1__status"></div>
											</div>
											<div class="acc1-item">
												<div class="acc1-item__info-block acc1__title">
													<a href="#" class="acc1-item__title">Насос для накачки шин ZF-0807</a>
													<div class="acc1-item__attr">Артикул: <span>6500900</span></div>
													<div class="acc1-item__attr">Цвет: <span>Черный</span></div>
												</div>
												<div class="acc1-item__sum-block acc1__sum">
													<div class="acc1-item__price">4 500 ₽</div>
													<div class="acc1-item__price-bonus">+ 500 бонусов</div>
												</div>
												<div class="acc1-item__bonus-block acc1__bonus">
													<div class="acc1-item__bonus">- 1000 бонусов</div>
												</div>
												<div class="acc1-item__qty-block acc1__qty">
													<div class="input-count input-count_disabled acc1-item__count-block ">
														<div class="input-group">
															<input type="text"
																   class="form-control input-count__input"
																   value="1" disabled>
														</div>
													</div>
												</div>
												<div class="acc1-item__status-block acc1__status"></div>
											</div>
										</div>
				
									</div>
				
				
									<div class="acc1-item">
										<div class="acc1-item__info-block acc1__title">
											<div class="acc1-item__date">09.11.2017</div>
											<div class="acc1-item__number">№103</div>
											<a href="#" class="acc1-item__title">Электроцикл Alta Redshift MX</a>
											<div class="acc1-item__attr">Артикул: <span>6500900</span></div>
											<div class="acc1-item__attr">Цвет: <span>Белый</span></div>
										</div>
										<div class="acc1-item__sum-block acc1__sum">
											<div class="acc1-item__price">135 999 ₽</div>
											<div class="acc1-item__price-bonus">+ 10000 бонусов</div>
										</div>
										<div class="acc1-item__bonus-block acc1__bonus">
											<div class="acc1-item__bonus">-</div>
										</div>
										<div class="acc1-item__qty-block acc1__qty">
											<div class="input-count input-count_disabled acc1-item__count-block ">
												<div class="input-group">
													<input type="text"
														   class="form-control input-count__input"
														   value="1" disabled>
												</div>
											</div>
										</div>
										<div class="acc1-item__status-block acc1__status">
											<div class="acc1-item__status acc1-item__status_green">Выполнен</div>
											<a href="#" class="acc1-item__add-review-link">Оставьте отзывы</a>
										</div>
									</div>
				
									<div class="acc1-item">
										<div class="acc1-item__info-block acc1__title">
											<div class="acc1-item__date">09.11.2017</div>
											<div class="acc1-item__number">№103</div>
											<a href="#" class="acc1-item__title">Электроцикл Alta Redshift MX</a>
											<div class="acc1-item__attr">Артикул: <span>6500900</span></div>
											<div class="acc1-item__attr">Цвет: <span>Белый</span></div>
										</div>
										<div class="acc1-item__sum-block acc1__sum">
											<div class="acc1-item__price">135 999 ₽</div>
											<div class="acc1-item__price-bonus">+ 10000 бонусов</div>
										</div>
										<div class="acc1-item__bonus-block acc1__bonus">
											<div class="acc1-item__bonus">-</div>
										</div>
										<div class="acc1-item__qty-block acc1__qty">
											<div class="input-count input-count_disabled acc1-item__count-block ">
												<div class="input-group">
													<input type="text"
														   class="form-control input-count__input"
														   value="1" disabled>
												</div>
											</div>
										</div>
										<div class="acc1-item__status-block acc1__status">
											<div class="acc1-item__status acc1-item__status_red">Отменен</div>
											<a href="#" class="acc1-item__add-review-link">Оставьте отзывы</a>
										</div>
									</div>
				
								</div>
				
								<div class="acc1__pagination">
									<div class="moto-pagination">
										<nav class="moto-pagination__nav">
											<a href="#" class="moto-pagination__item prev">1</a>
											<a href="#" class="moto-pagination__item prev">2</a>
											<div class="moto-pagination__item">3</div>
											<a href="#" class="moto-pagination__item">4</a>
											<a href="#" class="moto-pagination__item">5</a>
											<div class="moto-pagination__dots">...</div>
											<a href="#" class="moto-pagination__item">20</a>
										</nav>
									</div>                
								</div>
				
							</div><!--/.acc1 -->
*/?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>