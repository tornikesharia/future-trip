<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Редактирование профиля");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
?>


<?$APPLICATION->IncludeComponent(
    "dev:tariy.bitrix.profile",
	array(
		"CACHE_TYPE" => "N"
		),
	Array()
);?>


<?/*
                        <div class="acc-profile">
            
            
            
                                <div class="acc-profile__title">Редактирование профиля</div>
            
                                <div class="nav nav-pills acc-profile__tab-buttons" role="tablist">
                                    <a class="nav-link active acc-profile__tab-button"
                                       id="acc-profile__tab1-button"
                                       data-toggle="tab"
                                       href="#acc-profile__tab_1"
                                       role="tab"
                                       aria-selected="true">Физическое лицо</a>
                                    <a class="nav-link acc-profile__tab-button"
                                       id="acc-profile__tab2-button"
                                       data-toggle="tab"
                                       href="#acc-profile__tab_2"
                                       role="tab"
                                       aria-selected="false">Юридическое лицо</a>
                                </div>
            
                                <div class="tab-content">
                                    <div class="tab-pane fade show active acc-profile__tab acc-profile__tab_1" id="acc-profile__tab_1" role="tabpanel">
            
                                        <form class="acc-profile__form">
                                            <div class="form-group row acc-profile__form-group align-items-start">
                                                <div class="col-sm-4 col-md-3 col-lg-2 col-form-label">Фото</div>
                                                <div class="col-sm-8 col-md-9 col-lg-10 acc-profile__col-with-photo">
                                                    <div class="acc-profile__avatar">
                                                        <!-- вариант загруженной фотографии -->
                                                        <img src="assets/img/examples/account-photo.png" alt="" class="acc-profile__avatar-img">
                                                        <button type="button" class="acc-profile__delete-photo-button">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <!--вариант пустой фотографии, из за нестандартного инпута название файла не будет показываться после выбора,
                                                     можно сразу загрузить фотографию и показать по примеру как в добавлении нового отзыва -->
                                                    <!--<div class="acc-profile__empty-avatar">-->
                                                    <!--<img src="assets/img/icons/img-placeholder.svg" alt="" class="acc-profile__img-placeholder">-->
                                                    <!--<div class="custom-file">-->
                                                    <!--<input type="file" class="custom-file-input" id="acc-profile__avatar" accept="image/x-png,image/gif,image/jpeg">-->
                                                    <!--<label class="custom-file-label" for="acc-profile__avatar">Загрузить фото</label>-->
                                                    <!--</div>-->
                                                    <!--</div>-->
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_name" class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Имя</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           id="acc-profile__input_name"
                                                           name="acc-profile__input_name"
                                                           value="Михаил Иванович">
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_email" class="col-sm-4 col-md-3 col-lg-2 col-form-label required">E-mail</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10 acc-profile__col-with-hint">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           id="acc-profile__input_email"
                                                           name="acc-profile__input_email"
                                                           value="example@gmail.com">
                                                    <div class="acc-profile__input-hint">
                                                        <img src="assets/img/icons/ok.svg" alt="" class="acc-profile__input-hint-img">
                                                        <div class="acc-profile__input-hint-text">Подтвержден</div>
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_phone" class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Телефон</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10 acc-profile__col-with-hint">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           id="acc-profile__input_phone"
                                                           name="acc-profile__input_phone"
                                                           placeholder="+7____-___-__-__">
                                                    <div class="acc-profile__input-hint">
                                                        <img src="assets/img/icons/i.svg" alt="" class="acc-profile__input-hint-img">
                                                        <div class="acc-profile__input-hint-text">Для подтверждения нового номера вам будет выслан на него SMS-код</div>
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label  class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Дата рождения</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10 acc-profile__birthday-col">
                                                    <div class="acc-profile__birthday-wrap">
                                                        <!-- инициализируется плагином календаря -->
                                                        <input type="text"
                                                               class="moto-input acc-profile__input acc-profile__input_birth js__acc-profile-birth"
                                                               id=""
                                                               name="acc-profile__input_birth"
                                                               value=""
                                                               placeholder="Выберите дату">
                                                    </div>
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label class="col-sm-4 col-md-3 col-lg-2 col-form-label">Пол</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10 acc-profile__gender-col">
                                                    <div class="custom-control custom-radio custom-control-inline moto-radio acc-profile__radio">
                                                        <input type="radio"
                                                               id="acc-profile__gender_1"
                                                               name="acc-profile__gender"
                                                               class="custom-control-input"
                                                               checked>
                                                        <label class="custom-control-label" for="acc-profile__gender_1">Мужской</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline moto-radio acc-profile__radio">
                                                        <input type="radio"
                                                               id="acc-profile__gender_2"
                                                               name="acc-profile__gender"
                                                               class="custom-control-input">
                                                        <label class="custom-control-label" for="acc-profile__gender_2">Женский</label>
                                                    </div>
                                                </div>
                                            </div>
            
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_address" class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Адрес поставки</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           id="acc-profile__input_address"
                                                           name="acc-profile__input_address"
                                                           value=""
                                                           required>
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_index" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Индекс</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_sm"
                                                           id="acc-profile__input_index"
                                                           name="acc-profile__input_index"
                                                           value="">
                                                </div>
                                            </div>
            
                                            <div class="form-group row acc-profile__form-group acc-profile__form-group_with-textarea">
                                                <label for="acc-profile__input_comment" class="col-sm-4 col-md-3 col-lg-2 col-form-label">Комментарии</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                        <textarea class="moto-input acc-profile__input acc-profile__input_md"
                                                  id="acc-profile__input_comment"
                                                  name="acc-profile__input_comment"
                                                  rows="8"
                                                  placeholder="Укажите здесь комментраии к доставке, подъезд, этаж, наличие и код домофона, а также альтернативные способы связи."></textarea>
                                                </div>
                                            </div>
            
                                            <div class="acc-profile__help-text">
                                                <span class="acc-profile__help-text-notice">*</span>
                                                Обязательные поля для заполнения
                                            </div>
            
                                            <div class="acc-profile__title acc-profile__title_contacts">Контакты</div>
            
                                            <div class="acc-profile__contacts">
            
                                                <div class="acc-profile__contact">
                                                    <!--в дизайне этот блок с кнопками но мб можно это сделать каким нибудь стандартным компонентом, -->
                                                    <textarea class="moto-input acc-profile__input acc-profile__input_contact"
                                                              name="acc-profile__input_contact"
                                                              rows="8"
                                                              disabled
                                                    >Михаил Михайлович Иванов, michael@mail.ru, +7 925 100 00 05, 123 456, Москва, ул. Первая, д.55. Второй подъезд за мусоркой, затем налево, домофона нет. Просьба под окнами не кричать, соседи злые.</textarea>
                                                    <div class="acc-profile__contact-buttons">
                                                        <button class="acc-profile__contact-button acc-profile__contact-button_trash">
                                                            <svg class="acc-profile__contact-button-img" >
                                                                <use xlink:href="#icon-trash"></use>
                                                            </svg>
                                                        </button>
                                                        <button class="acc-profile__contact-button acc-profile__contact-button_edit">
                                                            <svg class="acc-profile__contact-button-img" >
                                                                <use xlink:href="#icon-edit"></use>
                                                            </svg>
                                                        </button>
                                                        <!--в дизайне есть зелёный стиль последней кнопки (или это не кнопка), для него добавить .valid к .acc-profile__contact-button_ok-->
                                                        <div class="acc-profile__contact-button acc-profile__contact-button_ok">
                                                            <svg class="acc-profile__contact-button-img" >
                                                                <use xlink:href="#icon-ok-empty"></use>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
            
                                                <!--для отключенного состояния добавить атрибут disabled, пример был в дизайне-->
                                                <button type="button" class="acc-profile__new-contact-button">
                                                    <span class="acc-profile__new-contact-plus">+</span>
                                                    <span class="acc-profile__new-contact-text">Добавить контакт</span>
                                                </button>
            
                                            </div><!-- /.acc-profile__contacts -->
            
                                            <div class="acc-profile__buttons">
                                                <!--для отключенного состояния добавить атрибут disabled-->
                                                <button type="submit" class="button_yellow acc-profile__save-button">СОХРАНИТЬ</button>
                                            </div>
                                        </form><!-- /.acc-profile__form -->
            
                                    </div><!--/. tab1 -->
                                    <div class="tab-pane fade acc-profile__tab acc-profile__tab_2" id="acc-profile__tab_2" role="tabpanel">
            
                                        <form class="acc-profile__form">
            
                                            <div class="form-group row acc-profile__form-group">
                                                <label class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Название организации</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           name="acc-profile__input_address"
                                                           value=""
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row acc-profile__form-group">
                                                <label for="acc-profile__input_inn" class="col-sm-4 col-md-3 col-lg-2 col-form-label required">ИНН</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_sm"
                                                           id="acc-profile__input_inn"
                                                           name="acc-profile__input_address"
                                                           value=""
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row acc-profile__form-group">
                                                <label class="col-sm-4 col-md-3 col-lg-2 col-form-label">КПП</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_sm"
                                                           name="acc-profile__input_address"
                                                           value=""
                                                           >
                                                </div>
                                            </div>
                                            <div class="form-group row acc-profile__form-group">
                                                <label  class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Физический адрес</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           name="acc-profile__input_address"
                                                           value=""
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row acc-profile__form-group">
                                                <label class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Юридический адрес</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           name="acc-profile__input_address"
                                                           value=""
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row acc-profile__form-group">
                                                <label class="col-sm-4 col-md-3 col-lg-2 col-form-label required">E-mail</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           name="acc-profile__input_address"
                                                           value=""
                                                           required>
                                                </div>
                                            </div>
                                            <div class="form-group row acc-profile__form-group">
                                                <label class="col-sm-4 col-md-3 col-lg-2 col-form-label required">Телефон</label>
                                                <div class="col-sm-8 col-md-9 col-lg-10">
                                                    <input type="text"
                                                           class="moto-input acc-profile__input acc-profile__input_md"
                                                           name="acc-profile__input_address"
                                                           value=""
                                                           required>
                                                </div>
                                            </div>
            
                                            <div class="acc-profile__buttons">
                                                <!--для отключенного состояния добавить атрибут disabled-->
                                                <button type="submit" class="button_yellow acc-profile__save-button">СОХРАНИТЬ</button>
                                            </div>
                                        </form><!-- /.acc-profile__form -->
            
                                    </div><!--/. tab2 -->
                                </div><!--/. tabs -->
                                
            
            
            
            
            
            
            
                        </div><!-- /.acc-profile -->
            


*/?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>