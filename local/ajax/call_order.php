<?
define("BS_FORM_TYPE", 'CALL_ORDER'); //Тип почтового события
define("BS_FORM_TEMPLATE", 84); // Шаблон почтового события
define("NO_AGENT_CHECK", true);//Отключаем выполнение агентов, при выполнении данного скрипта
define("NO_AGENT_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


global $APPLICATION, $USER;

$arResult = array(
    "RESULT" => "OK",
    "ERROR" => array()
);

$arDataNew = Array(
    "PHONE" => $_REQUEST["PHONE"],
);
foreach ($arDataNew as $key => $value) {
    $arData[$key] = addslashes(strip_tags($value));
    switch ($key) {

        case 'PHONE':
            if (empty($value)) {
                $arResult['ERROR'][] = array(
                    "REQUIRED_FIELDS",
                    "Не заполнено обязательное поле \"Ваш телефон\"!"
                );
            } else {
                $poneArray = Array('+', '(', ')', ' ', '-');
                foreach ($poneArray as $pArray) {
                    $value = str_replace($pArray, "", $value);
                }
                if (!is_numeric($value)) {
                    $arResult['ERROR'][] = array(
                        "REQUIRED_FIELDS",
                        'Ошибка, в поле "Ваш телефон" должны быть только цифры'
                    );
                } else if (iconv_strlen($value) < 6) {
                    $arResult['ERROR'][] = array(
                        "REQUIRED_FIELDS",
                        'Ошибка, в поле "Ваш телефон" не менее 6 символов'
                    );
                } else if (iconv_strlen($value) > 16) {
                    $arResult['ERROR'][] = array(
                        "REQUIRED_FIELDS",
                        'Ошибка, в поле "Ваш телефон" не более 16 символов'
                    );
                }
            }
            break;
    }
}

if (empty($arResult["ERROR"])) {

    $arFields = array(
        "EMAIL_FROM" => 'Futuretrip@' . Lib::GetDomainName(),
        "PHONE" => $arData["PHONE"],
        "DATE" => date("d/m/Y G:i:s", time() + CTimeZone::GetOffset()) // Текущее время с учетом часового пояса
    );

    //Отправляем почтовое событие
    if (CEvent::Send(BS_FORM_TYPE, SITE_ID, $arFields, true, BS_FORM_TEMPLATE)) {

        $arResult["RESULT_DETAIL"] = '<div style="padding:50px 0; font-size:25px;"><strong>Cпасибо за заявку, менеджер свяжется с вами в ближайшее время.</strong></div>';

        //Если нет ошибок, то возврещаем в json array["RESULT"] = "OK"
        $arResult["RESULT"] = "OK";
    } else {
        // Если почтовое событие не отправилось, то возвращаем ошибку
        $arResult["ERROR"][] = array("EVENT_FORM", "Произошла ошибка почтовой системы!");
    }

} else {
    //Если есть ошибки, то возврещаем в json array["RESULT"] = "ERROR"
    //В json array["RESULT_DETAIL"] записываем ошибки
    $arResult["RESULT"] = "ERROR";
    $arResult["RESULT_DETAIL"] = '';
    foreach ($arResult["ERROR"] as $error) {
        $arResult["RESULT_DETAIL"] .= '<span style="color:red">' . $error[1] . '</span><br>';
    }
}

/** @global CMain $APPLICATION */
if (strtolower(SITE_CHARSET) != 'utf-8')
    $arResult = $APPLICATION->ConvertCharsetArray($arResult, SITE_CHARSET, 'utf-8');

header('Content-Type: application/json');
echo json_encode($arResult);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
//php можно не закрывать, тогда не будет вероятности возвращения пробелов в ajax