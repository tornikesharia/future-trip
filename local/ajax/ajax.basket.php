<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader,
    \Bitrix\Main\Localization\Loc;

Loader::includeModule('catalog');
Loader::includeModule('sale');

if ($_REQUEST['KEY'] == md5("ADD_BASKET")) {
    $elementBasket = false;
	
	$IBLOCK_ID = 2;

    $arFilter = Array(
        "IBLOCK_ID" => IntVal($IBLOCK_ID),
        "ID" => $_REQUEST["ID"],
        "ACTIVE_DATE" => "Y",
        "ACTIVE" => "Y"
    );
	
	$arProps = array();
	if($_REQUEST["COLOR"]){
		$arProps[] = array(
			"NAME" => "Цвет",
			"CODE" => "COLOR",
			"VALUE" => $_REQUEST["COLOR"],
			"SORT" => "100"
		);
	}
	
	if($_REQUEST["DIMENSIONS"]){
		$arProps[] = array(
			"NAME" => "Размер",
			"CODE" => "DIMENSIONS",
			"VALUE" => $_REQUEST["DIMENSIONS"],
			"SORT" => "200"
		);		
	}		

    $res = CIBlockElement::GetList(
        Array(),
        Array(),
        false,
        false,
        Array("ID", "NAME", "DATE_ACTIVE_FROM")
    );
    if ($ob = $res->GetNextElement()) {
        if ($arFields = $ob->GetFields()) {
            $elementBasket = true;
        }
    }

    // Параметры товара
    $arResult["status"] = false;
    $arResult["status_button"] = false;

    if ($elementBasket) {
        $arResult["status_button"] = true;

        $productId = $_REQUEST["ID"];
        $quantity = ($_REQUEST["QUANTITY"] > 1 ? $_REQUEST["QUANTITY"] : 1);

        // Выборка данных товара
        $resElement = CIBlockElement::GetByID($productId);
        $arElement = $resElement->GetNext();
        // Цены товара
        $arPrice = CPrice::GetBasePrice($productId);
        // Тип цены
        $dbPriceType = CCatalogGroup::GetList(array("SORT" => "ASC"), array("NAME" => $arPrice["CATALOG_GROUP_NAME"]));
        $arPriceType = $dbPriceType->Fetch();
        // Скидка на товар
        $arDiscounts = false;
        $arDiscounts = CCatalogDiscount::GetDiscountByProduct($productId, $USER->GetUserGroupArray(), "N", 1, SITE_ID);
        if ($arDiscounts) {
            foreach ($arDiscounts as $iDiscount) {
                $arDiscounts = $iDiscount;
            }
        }

        $dbBasketItems = CSaleBasket::GetList(
            array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
            array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL"
            ),
            false,
            false,
            array("ID", "NAME", "PRODUCT_ID", "PRICE", "DELAY", "QUANTITY", "DISCOUNT_PRICE", "DISCOUNT_VALUE")
        );
        $resQuantity = 0;
        while ($bItem = $dbBasketItems->Fetch()) {
            $itemID[] = $bItem["ID"];
            if ($bItem["PRODUCT_ID"] == $productId) {
                $basketElement = $bItem;
            }
        }

        if ($basketElement) {
            $arFields = array(
                "QUANTITY" => ($basketElement["QUANTITY"] + $quantity),
				"PROPS" => $arProps
            );
            if (CSaleBasket::Update($basketElement["ID"], $arFields)) {
                $arResult["status"] = true;
            }
        } else {
            // $discount = false;
            if ($arPrice["PRICE"] > 0) {
                if ($arDiscounts) {
                    // Цена и скидки
                    $discount_value = $arDiscounts["VALUE"];
                    $price = array(
                        "PRICE" => $arPrice["PRICE"] - ($arPrice["PRICE"] * $discount_value / 100),
                        "DISCOUNT_PRICE" => ($arPrice["PRICE"] * $discount_value / 100),
                        "DISCOUNT_VALUE" => $discount_value,
                    );
                } else {
                    // Цена, скидок нет
                    $price = array(
                        "PRICE" => $arPrice["PRICE"],
                        "DISCOUNT_PRICE" => "",
                        "DISCOUNT_VALUE" => "",
                    );
                }
            } else {
                // Если цены нет, ставим цену 0, скики равны пустоте
                $price = array(
                    "PRICE" => 0,
                    "DISCOUNT_PRICE" => "",
                    "DISCOUNT_VALUE" => "",
                );
            }

		

            $arFields = Array(
                "PRODUCT_ID" => $productId,
                "QUANTITY" => $quantity,
                "NAME" => $arElement["NAME"],
                "LID" => \Bitrix\Main\Context::getCurrent()->getSite(),
                "CURRENCY" => $arPrice["CURRENCY"],
                "PRICE" => round(($item["PRICE"] > 0 ? $item["PRICE"] : $price["PRICE"])),
                "DISCOUNT_PRICE" => round($price["DISCOUNT_PRICE"]),
                "DISCOUNT_VALUE" => $price["DISCOUNT_VALUE"],
                "DETAIL_PAGE_URL" => $arElement["DETAIL_PAGE_URL"],
                "DELAY" => ($item ? $item["DELAY"] : 'N'),// флаг "товар отложен"
                "CAN_BUY" => "Y",// флаг "товар можно купить"
                "CUSTOM_PRICE" => "Y",// могут ли, например, скидки изменять стоимость позиции или нет.
                "IGNORE_CALLBACK_FUNC" => "Y",
                "NOTES" => $arPriceType["NAME_LANG"],
                "MODULE" => "catalog",
                "PROPS" => $arProps
            );
			
            $arResult["item"] = false;
            if ($basketItem = CSaleBasket::Add($arFields)) {
                $arResult["status"] = true;
                if (!in_array($basketItem, $itemID)) {
                    $arResult["item"] = $basketItem;
                }
            }
        }

        $dbBasketItems = CSaleBasket::GetList(
            array(
                "NAME" => "ASC",
                "ID" => "ASC"
            ),
            array(
                "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                "LID" => SITE_ID,
                "ORDER_ID" => "NULL",
                "DELAY" => "N"
            ),
            false,
            false,
            array("ID", "NAME", "PRODUCT_ID", "PRICE", "DELAY", "QUANTITY")
        );

        while ($bItem = $dbBasketItems->Fetch()) {
            $resQuantity = $resQuantity + 1;
            $arResult["MINI_BASKET"][] = $bItem;
            if ($productId == $bItem["PRODUCT_ID"]) {
                $item = $bItem;
            }
        }
        if (!$item) {
            $resQuantity = $resQuantity + 1;
        }
        $allprice = 0;
        foreach ($arResult["MINI_BASKET"] as $k => $item) {
            $allprice = $allprice + ($item["PRICE"] * $item["QUANTITY"]);

            $resElementRes = CIBlockElement::GetByID($item["PRODUCT_ID"]);
            $arElementRes = $resElementRes->GetNext();

            $arResult["MINI_BASKET"][$k]["PRICE"] = number_format(round($item["PRICE"] * $item["QUANTITY"]), 0, ',', ' ');

            $arResult["MINI_BASKET"][$k]["ELEMENT"] = $arElementRes;
			
			if($arElementRes["PREVIEW_PICTURE"]){
				$arResult["MINI_BASKET"][$k]["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
					CFile::GetFileArray($arElementRes["PREVIEW_PICTURE"]),
					array('width' => 70, 'height' => 70),
					BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
					true,
					Array()
				);
			} else {
				$arResult["MINI_BASKET"][$k]["ELEMENT"]["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
					CFile::GetFileArray($arElementRes["DETAIL_PICTURE"]),
					array('width' => 70, 'height' => 70),
					BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
					true,
					Array()
				);
			}

            $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($item["PRODUCT_ID"]);
            foreach ($arMeasure as $Measure) {
                $arResult["MINI_BASKET"][$k]["MEASURE"] = $Measure["MEASURE"];
            }
        }
		if($arResult["status"]){
			$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_ARTNUMBER", "PROPERTY_COLOR");
			$arFilter = Array("IBLOCK_ID"=>IntVal($IBLOCK_ID), "ID" => $_REQUEST["ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			$ob = $res->GetNextElement();
			$arThisElement = $ob->GetFields();
			
			$arThisElement['NAME'] = html_entity_decode($arThisElement['NAME']);

 			if($arThisElement["PREVIEW_PICTURE"]){
				$arThisElement["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
					CFile::GetFileArray($arThisElement["PREVIEW_PICTURE"]),
					array('width' => 162, 'height' => 114),
					BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
					true,
					Array()
				);
			} else {
				$arThisElement["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
					CFile::GetFileArray($arThisElement["DETAIL_PICTURE"]),
					array('width' => 162, 'height' => 114),
					BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
					true,
					Array()
				);
			}	 
		}		
		
		$arResult["ITEM"] = $arThisElement;
        $arResult["quantity"] = $resQuantity;
        $arResult["price"] = CurrencyFormat($allprice, "RUB");
		$arResult["REQUEST"] = $_REQUEST;
    }

    echo json_encode($arResult);
} else if ($_REQUEST['KEY'] == md5("CHECKOUT_BASKET")) {
    // Оформить заказ
    $priceNo = 0;
    $arResult["status"] = true;

    //
    $dbBasketItems = CSaleBasket::GetList(
        array(
            "NAME" => "ASC",
            "ID" => "ASC"
        ),
        array(
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ),
        false,
        false,
        array("ID", "PRODUCT_ID", "PRICE", "DELAY")
    );

    while ($bItem = $dbBasketItems->Fetch()) {
        if ($bItem["PRICE"] == 0 and $bItem["DELAY"] == "N") {
            $priceNo = $priceNo + 1;
        } else if ($bItem["PRICE"] > 0 and $bItem["DELAY"] == "N") {
            $arResult["ITEMS_BASKET"][] = $bItem;
        }
    }
    if ($priceNo > 0) {
        $arResult["status"] = false;
    }

    echo json_encode($arResult);
} else if ($_REQUEST['KEY'] == md5("SPECIFY_PRICES_BASKET")) {
    // Уточнить цены
    $dbBasketItems = CSaleBasket::GetList(
        array(
            "NAME" => "ASC",
            "ID" => "ASC"
        ),
        array(
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ),
        false,
        false,
        array("ID", "NAME", "PRODUCT_ID", "PRICE", "DELAY", "QUANTITY")
    );

    while ($bItem = $dbBasketItems->Fetch()) {
        if ($bItem["PRICE"] == 0 and $bItem["DELAY"] == "N") {
            $arResult["ITEM"][] = $bItem;

            $arFields = array(
                "DELAY" => "Y"
            );
            if (CSaleBasket::Update($bItem["ID"], $arFields)):
                $arResult["status"] = true;
            else:
                $arResult["status"] = false;
            endif;
        }
    }

    //
    $dbBasketItems = CSaleBasket::GetList(
        array(
            "NAME" => "ASC",
            "ID" => "ASC"
        ),
        array(
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL",
            "DELAY" => "Y"
        ),
        false,
        false,
        array("ID", "NAME", "PRODUCT_ID", "PRICE", "DELAY", "QUANTITY")
    );
    while ($bItem = $dbBasketItems->Fetch()) {
        $arResult["ITEMS"][] = $bItem;
        $itemID[] = $bItem["PRODUCT_ID"];
        $manager_items_array[] = $bItem["ID"] . "-" . $bItem["QUANTITY"];
    }
    $arSelect = Array("ID", "DETAIL_PAGE_URL", 'PREVIEW_PICTURE');
    $arFilter = Array("IBLOCK_ID" => IntVal(2), "ID" => $itemID, "ACTIVE" => "Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();

        $itemEl[$arFields["ID"]] = $arFields;
        $itemEl[$arFields["ID"]]['PREVIEW_PICTURE'] = CFile::ResizeImageGet(
            CFile::GetFileArray($arFields["PREVIEW_PICTURE"]),
            array('width' => 70, 'height' => 70),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
            true,
            Array()
        );
    }
    foreach ($arResult["ITEMS"] as $k => $item) {
        $arResult["ITEMS"][$k]["ELEMENT"] = $itemEl[$item["PRODUCT_ID"]];
        /*$arResult["ITEMS"][$k]["ELEMENT"]['PREVIEW_PICTURE'] = CFile::ResizeImageGet(
            CFile::GetFileArray($item["ELEMENT"]["PREVIEW_PICTURE"]),
            array('width' => 70, 'height' => 70),
            BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
            true,
            Array()
        );*/
    }

    if ($arResult["status"]) {
        $manager_items = '';
        foreach ($manager_items_array as $k => $miArray) {
            $manager_items .= $miArray . (count($manager_items_array) - 1 == $k ? '' : ':');
        }
        //$manager_linck = $_SERVER['SERVER_NAME'] . "/bitrix/admin/tariy.meblers_manager_pricing.php" . "?FUSER_ID=" . CSaleBasket::GetBasketUserID() . "&SITE_ID=" . SITE_ID;
        $manager_linck = $_SERVER['SERVER_NAME'] . "/bitrix/admin/tariy.meblers_manager_pricing.php" . "?ID=" . $USER->GetID() . "&SITE_ID=" . SITE_ID;

        $arResult["mail"] = $manager_linck;

        $arData["EMAIL"] = COption::GetOptionString('main', 'email_from');
        $arData["TEXT"] = 'Прошу уточнить цены на товары';
        $arData["TEXT"] .= '<br />';
        $arData["TEXT"] .= '<br />';
        $arData["TEXT"] .= '<table style="width: 100%;">';
        $arData["TEXT"] .= '<tr>';
        $arData["TEXT"] .= '<td><b>Картинка</b></td>';
        $arData["TEXT"] .= '<td><b>Название</b></td>';
        $arData["TEXT"] .= '<td><b>Кол-во</b></td>';
        $arData["TEXT"] .= '<td><b>ID</b></td>';
        $arData["TEXT"] .= '</tr>';
        foreach ($arResult["ITEMS"] as $item) {
            $arData["TEXT"] .= '<tr>';
            $arData["TEXT"] .= '<td><img src="' . $_SERVER['SERVER_NAME'] . $item["ELEMENT"]["PREVIEW_PICTURE"]["src"] . '" /></td>';
            $arData["TEXT"] .= '<td><a href="' . $_SERVER['SERVER_NAME'] . $item["ELEMENT"]["DETAIL_PAGE_URL"] . '">' . $item["NAME"] . '</a></td>';
            $arData["TEXT"] .= '<td>' . $item["QUANTITY"] . '</td>';
            $arData["TEXT"] .= '<td>' . $item["ID"] . '</td>';
            $arData["TEXT"] .= '</tr>';
        }
        $arData["TEXT"] .= '</table>';
        $arData["TEXT"] .= '<br />';
        $arData["TEXT"] .= '<br />';
        $arData["TEXT"] .= '<a href="' . $manager_linck . '">перейти в панель</a>';

        $SID = CEvent::Send('MEBLERS_MANAGER', SITE_ID, $arData, true);

    }

    echo json_encode($arResult);
} else if ($_REQUEST['KEY'] == md5("ASSEMBLY")) {
    $dbBasketItems = CSaleBasket::GetList(
        array(
            "NAME" => "ASC",
            "ID" => "ASC"
        ),
        array(
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ),
        false,
        false,
        array("ID", "NAME", "PRODUCT_ID", "PRICE", "DELAY", "QUANTITY")
    );

    while ($bItem = $dbBasketItems->Fetch()) {
        if ($bItem["ID"] == $_REQUEST["ID"]) {
            $arItem["ITEM"] = $bItem;
            $db_res = CSaleBasket::GetPropsList(
                array(
                    "SORT" => "ASC",
                    "NAME" => "ASC"
                ),
                array("BASKET_ID" => $bItem["ID"])
            );
            while ($ar_res = $db_res->Fetch()) {
                $arItem["ITEM"]["PROP"] = $ar_res;
            }

            if ($_REQUEST["ASSEMBLY"] == "Y" and empty($arItem["ITEM"]["PROP"]["VALUE"])) {
                $arFields["PRICE"] = $bItem["PRICE"] + $_REQUEST["PRICE_ASSEMBLY"];
            } else if ($_REQUEST["ASSEMBLY"] == "N" and !empty($arItem["ITEM"]["PROP"]["VALUE"])) {
                $arFields["PRICE"] = $bItem["PRICE"] - $_REQUEST["PRICE_ASSEMBLY"];
            } else {
                $arFields["PRICE"] = $bItem["PRICE"];
            }
            $arFields["PROPS"] = array(
                array(
                    "NAME" => "Сборка",
                    "CODE" => "UF_ASSEMBLY_BASKET",
                    "VALUE" => ($_REQUEST["ASSEMBLY"] == "Y" ? $_REQUEST["PRICE_ASSEMBLY"] : '')
                )
            );
            if (CSaleBasket::Update($bItem["ID"], $arFields)):
                $arResult["PRICE_ITEMS"][] = $bItem;
            endif;
        }
    }

    $arResult["ASSEMBLY"] = $_REQUEST["ASSEMBLY"];
    $arResult["arFields"] = $arFields;

    echo json_encode($arResult);
}