/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
;
(function($) {
    function Form(options) {
        this.options = $.extend({
            required: '.required',
            errclass: 'error',
            succclass: 'success',
            demo: false,
            successAnimate: null,
            attachEvents: null,
            inputmaskEnable: false,
            errorType: 'default',
            onShowError: null,
            onSend: null
        }, options);
        this.init();
    }
    Form.prototype = {
        init: function() {
            this.findElements();
            this.attachEvents();
            if (this.options.inputmaskEnable) {
                this.inputmask();
            }
        },
        findElements: function() {
            this.holder = $(this.options.holder);
            this.form = this.holder.find('form');
            this.success = this.holder.find('.' + this.options.succclass);
            this.error = this.holder.find('.' + this.options.errclass);
        },
        attachEvents: function() {
            var self = this;
            self.form.submit(function(e) {
                if ($.isFunction(self.options.onSend)) {
                    self.options.onSend(self, e);
                }
                if (self.options.demo || self.validate()) {
                    self.send();
                }
                return false;
            });
            if ($.isFunction(this.options.attachEvents)) {
                this.options.attachEvents(this);
            }
        },
        inputmask: function() {
            //TODO Check plugin enable
            var self = this;
            self.form.find('input.phone').inputmask("+7(999) 999-99-99");
            self.form.find('input.time_hour').inputmask('h');
            self.form.find('input.time_minute').inputmask('s');
        },
        successAnimate: function() {
            if ($.isFunction(this.options.successAnimate)) {
                this.options.successAnimate(this);
            } else {
                var self = this;
                self.form.parent().hide();
                self.form.parent().next().fadeIn(200);

                setTimeout(function() {
                    self.holder.animate({
                        left: "-280px"
                    }, 600, function() {
                        self.holder.animate({
                            left: "-270px"
                        }, 400);
                        self.holder.removeClass("msgOpen");
                    })
                }, 2000);
            }
        },
        validate: function() {
            return true;
        },
        send: function() {
            var self = this,
                href = self.form.attr('action'),
                inputArray = self._getDataArray();
            inputArray.append('ajax', true);
            if (!self.options.demo) {
                $.ajax({
                    url: href,
                    data: inputArray,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(jsonData) {
                        if (jsonData.status) {
                            self.successAnimate();
                        } else {
                            var error = jsonData.error,
                                controls = self.form.find('input, textarea, select');
                            controls.removeClass('error');
                            if (self.options.errorType == 'default') {
                                for (var i in error) {
                                    switch (error[i].type) {
                                        case 'DATA':
                                            var control = self.form.find('[name*="DATA[' + error[i].name + ']"]');
                                            self.appendTooltip(control, error[i].text);
                                            control.addClass('error');
                                            break;
                                        case 'submit':
                                            var control = self.form.find('[type="submit"');
                                            self.appendTooltip(control, error[i].text);
                                            control.addClass('error');
                                            break;
                                    }
                                }
                            } else if (self.options.errorType == 'custom') {
                                if ($.isFunction(self.options.onShowError)) {
                                    self.options.onShowError(self, jsonData);
                                }
                            }
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log('ajax error', jqXHR);
                        alert(textStatus + ' : ' + errorThrown);
                    }
                });
            } else {
                self.successAnimate();
            }
        },
        _getDataArray: function() {
            var self = this,
                data = $(self.form).serializeArray(),
                newData = new FormData(),
                inputFile = self.form.find('input[type=file]');
            $.each(inputFile, function(j, obFileInput) {
                if (obFileInput.files.length > 0) {
                    $.each(obFileInput.files, function(i, file) {
                        newData.append(obFileInput.name, file);
                    });
                }
            });
            for (i in data) {
                if (data[i].constructor == Object) {
                    newData.append(data[i].name, data[i].value);
                }
            }
            return newData;
        },
        appendTooltip: function(elem, text) {
            var tooltip = elem.data('Tooltip');
            if (!tooltip) {
                elem.data('Tooltip', new Tooltip({elem: elem, text: text}));
            }
        }
    }
    $.fn.bsForm = function(options) {
        return this.each(function() {
            $(this).data('Form', new Form($.extend(options, {holder: this})));
        })
    }
    function Tooltip(options) {
        this.options = $.extend({
        }, options);
        this.init();
    }
    Tooltip.prototype = {
        init: function() {
            this.findElements();
            this.attachEvents();
        },
        findElements: function() {
            this.elem = $(this.options.elem);
            var day = new Date();
            this.id = 'tooltip' + day.getTime();
            this.create();
        },
        attachEvents: function() {
            var self = this;
            self.elem.mouseover(function() {
                if (self.elem.hasClass('error')) {
                    self.tooltip.css({opacity: 0.8, display: "none"}).fadeIn(400);
                }
            }).mousemove(function(kmouse) {
                self.tooltip.css({left: kmouse.pageX + 15, top: kmouse.pageY + 15});
            }).mouseout(function() {
                self.tooltip.fadeOut(400);
            });
        },
        create: function() {
            var self = this;
            this.tooltipText = $("<p>" + self.options.text + "</p>");
            this.tooltip = $("<div class='bsTooltip' id='" + self.id + "'></div>");
            this.tooltip.append(this.tooltipText);
            $('body').append(this.tooltip);

        }
    }
    $.fn.bsTooltip = function(options) {
        return this.each(function() {
            $(this).data('Tooltip', new Tooltip($.extend(options, {holder: this})));
        })
    }
}(jQuery));
