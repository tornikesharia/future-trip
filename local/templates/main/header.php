<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?


	if($_GET['ip']) {
		$_SESSION['temp_ip'] = $_GET['ip'];
		$ip = $_SESSION['temp_ip'];
	} else {
		$ip = $_SESSION['temp_ip'];
	}
	
	if(!$USER->IsAuthorized() && $_SERVER['REMOTE_ADDR'] !== $ip){
		include_once($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/s1/site_closed.php');
		exit;
	}
/* 	if(Lib::UrlContains('/personal/') && !$USER->IsAuthorized()){
		LocalRedirect('');
	} */
	

?><!doctype html>
<html lang="<?=LANGUAGE_ID;?>">
  <head>
	<?$APPLICATION->ShowHead()?>
	<?
    use Bitrix\Main\Page\Asset;
	//файлы плагинов отдельно, чтобы не было конфликтов потом (кроме slick и bootstrap)
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/vendor/plyr/plyr.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/vendor/rangeslider/ion.rangeSlider.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/vendor/rangeslider/ion.rangeSlider.skinNice.css");
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/vendor/rateyo/jquery.rateyo.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/vendor/scrollbar/jquery.mCustomScrollbar.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/vendor/select2/select2.min.css");
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/vendor/flatpickr/flatpickr.css");
	// стили проекта + bootstrap 4 + slick
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/assets/css/app.css");
	Asset::getInstance()->addString('<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700&amp;subset=cyrillic,latin-ext" rel="stylesheet">');
    ?>

    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title><?$APPLICATION->ShowTitle()?></title>

		<link rel="apple-touch-icon" sizes="57x57" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

  </head>
  <body>

	<?$APPLICATION->ShowPanel();?>
    <div class="wrap">
		<header class="header">

			<div class="header-top">
				<div class="container">
					<div class="header-top__holder">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
								"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
								"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
								"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
								"DELAY" => "N",	// Откладывать выполнение шаблона меню
								"MAX_LEVEL" => "1",	// Уровень вложенности меню
								"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
									0 => "",
								),
								"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
								"MENU_CACHE_TYPE" => "A",	// Тип кеширования
								"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
								"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
								"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							),
							false
						);?>
						<?
                        global $USER;
                        if ($USER->IsAuthorized()):
						?>
								<?$APPLICATION->IncludeComponent("bitrix:main.user.link", "main", Array(
									"CACHE_TYPE" => "A",	// Тип кеширования
										"CACHE_TIME" => "7200",	// Время кеширования (сек.)
										"ID" => $USER->GetID(),	// Идентификатор пользователя
										"NAME_TEMPLATE" => "#NOBR##LAST_NAME# #NAME##/NOBR#",	// Отображение имени
										"SHOW_LOGIN" => "Y",	// Показывать логин, если не задано имя
										"THUMBNAIL_LIST_SIZE" => "30",	// Максимальный размер в пикселях фото в списке
										"THUMBNAIL_DETAIL_SIZE" => "100",	// Максимальный размер в пикселях фото детально
										"USE_THUMBNAIL_LIST" => "Y",	// Отображать личное фото в списке
										"SHOW_FIELDS" => array(	// Показывать поля пользователя
											0 => "PERSONAL_PHOTO"
										),
										"USER_PROPERTY" => array(	// Показывать доп. свойства
											//0 => "UF_USER_CAR_DEMO",
										),
										"PATH_TO_SONET_USER_PROFILE" => "",	// Шаблон пути к странице пользователя
										"PROFILE_URL" => "",	// Путь к странице пользователя
										"DATE_TIME_FORMAT" => "d.m.Y H:i:s",	// Формат показа даты и времени
										"SHOW_YEAR" => "Y",	// Показывать год рождения
									),
									false
								);?>
						<?
						else:
                        ?>
						<div class="header-top__acc-block">
							<a href="#" class="header-top__acc-enter-link" data-toggle="modal" data-target="#login-modal">Войти</a>&nbsp;|&nbsp;<a href="#" class="header-top__acc-enter-link" data-toggle="modal" data-target="#reg-modal">Регистрация</a>
						</div>
						<?endif;?>
					</div>
				</div>
			</div><!--/.header-top -->

			<div class="header-middle">
				<div class="container">
					<div class="header-middle__holder">

						<div class="header-middle__left">
							<a href="/" class="header-middle__logo-block">
							  <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/logo-black.svg" alt="" class="header-middle__logo-img" />
							</a>
							<?$APPLICATION->IncludeComponent("bitrix:search.title", "header_visual", Array(
								"NUM_CATEGORIES" => "1",	// Количество категорий поиска
									"TOP_COUNT" => "6",	// Количество результатов в каждой категории
									"CHECK_DATES" => "N",	// Искать только в активных по дате документах
									"SHOW_OTHERS" => "N",	// Показывать категорию "прочее"
									"PAGE" => SITE_DIR."catalog/",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
									"CATEGORY_0_TITLE" => "Товары",	// Название категории
									"CATEGORY_0" => array(	// Ограничение области поиска
										0 => "iblock_catalog",
									),
									"CATEGORY_0_iblock_catalog" => array(	// Искать в информационных блоках типа "iblock_catalog"
										0 => "all",
									),
									"CATEGORY_OTHERS_TITLE" => "Прочее",
									"SHOW_INPUT" => "Y",	// Показывать форму ввода поискового запроса
									"INPUT_ID" => "title-search-input",	// ID строки ввода поискового запроса
									"CONTAINER_ID" => "search",	// ID контейнера, по ширине которого будут выводиться результаты
									"PRICE_CODE" => array(	// Тип цены
										0 => "BASE",
									),
									"SHOW_PREVIEW" => "Y",	// Показать картинку
									"PREVIEW_WIDTH" => "75",	// Ширина картинки
									"PREVIEW_HEIGHT" => "75",	// Высота картинки
									"CONVERT_CURRENCY" => "Y",	// Показывать цены в одной валюте
								),
								false
							);?>
						</div>

						<div class="header-middle__phone-block">
							<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/phone-ring.svg" alt="" class="header-middle__phone-icon">
							<a href="tel:" class="header-middle__phone-link">8 800 700 55 65</a>
							<a href="javascript:void(0);" class="header-middle__phone-callback" data-toggle="modal" data-target="#phone-modal">Заказать звонок</a>
						</div>

						<div class="header-middle__right">
							<a href="/personal/favorites/" class="header-middle__fav-block">
								<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/star.svg" alt="" class="header-middle__fav-icon">
								<?$APPLICATION->IncludeComponent(
									"tariy:tariy.favorites.list", 
									"count",
									array(
										"CACHE_TIME" => 0,
										"CACHE_TYPE" => "N",
										),
									Array()
								);?>
								
							</a>
			

							<!-- cart -->
							<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "main", Array(
								"PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
									"PATH_TO_ORDER" => SITE_DIR."personal/cart/",	// Страница оформления заказа
									"PATH_TO_PERSONAL" => SITE_DIR."personal/",	// Страница персонального раздела
									"SHOW_PERSONAL_LINK" => "N",	// Отображать персональный раздел
									"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
									"SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
									"SHOW_PRODUCTS" => "Y",	// Показывать список товаров
									"SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
									"SHOW_IMAGE" => "Y",	// Выводить картинку товара
									"POSITION_FIXED" => "N",	// Отображать корзину поверх шаблона
									"SHOW_AUTHOR" => "N",	// Добавить возможность авторизации
									"PATH_TO_REGISTER" => SITE_DIR."login/",	// Страница регистрации
									"PATH_TO_PROFILE" => SITE_DIR."personal/",	// Страница профиля
								),
								false
							);?>
							<!-- /cart -->
						</div>

					</div><!--/.header-middle__holder -->
				</div><!--/.container -->
			</div><!--/.header-middle -->
			<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "header_menu", Array(
				"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
					"CACHE_GROUPS" => "Y",	// Учитывать права доступа
					"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
					"CACHE_TYPE" => "Y",	// Тип кеширования
					"COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
					"IBLOCK_ID" => "2",	// Инфоблок
					"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
					"SECTION_CODE" => "",	// Код раздела
					"SECTION_FIELDS" => array(	// Поля разделов
						0 => "ID",
						1 => "CODE",
						2 => "NAME",
						3 => "PICTURE",
						4 => "",
					),
					"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
					"SECTION_URL" => "/catalog/#CODE#/",	// URL, ведущий на страницу с содержимым раздела
					"SECTION_USER_FIELDS" => array(	// Свойства разделов
						0 => "UF_BACKGROUND_IMAGE",
						1 => "UF_ICON",
						2 => "UF_TYPE",
					),
					"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
					"TOP_DEPTH" => "3",	// Максимальная отображаемая глубина разделов
					"VIEW_MODE" => "LINE",	// Вид списка подразделов
					"COMPONENT_TEMPLATE" => "header_menu"
				),
				false
			);?>

		</header>

		<?
		$GLOBALS['CurPage'] = $APPLICATION->GetCurPage(false);
		switch($CurPage){
			case "/": $CurPageClass = " page_home"; break;
			case "/events/": $CurPageClass = " page_events"; break;

			default: $CurPageClass = "";
		}
		?>

		<main class="page">

			<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "main", Array(
				"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
					"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
					"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
					"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
					"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
				),
				false
			);?>

			<?if($CurPage !== '/'):?>
				<div class="page-content">

				<?if($USER->IsAuthorized() && Lib::UrlContains('/personal/') && $CurPage !== '/personal/cart/' && $CurPage !== '/personal/cart2/' && $CurPage !== '/personal/order/make/'):?>
					<?$APPLICATION->IncludeComponent("bitrix:main.user.link", "personal", Array(
						"CACHE_TYPE" => "A",	// Тип кеширования
							"CACHE_TIME" => "7200",	// Время кеширования (сек.)
							"ID" => $USER->GetID(),	// Идентификатор пользователя
							"NAME_TEMPLATE" => "#NOBR##LAST_NAME# #NAME##/NOBR#",	// Отображение имени
							"SHOW_LOGIN" => "Y",	// Показывать логин, если не задано имя
							"THUMBNAIL_LIST_SIZE" => "65",	// Максимальный размер в пикселях фото в списке
							"THUMBNAIL_DETAIL_SIZE" => "100",	// Максимальный размер в пикселях фото детально
							"USE_THUMBNAIL_LIST" => "Y",	// Отображать личное фото в списке
							"SHOW_FIELDS" => array(	// Показывать поля пользователя
								0 => "PERSONAL_PHOTO"
							),
							"USER_PROPERTY" => array(	// Показывать доп. свойства
								//0 => "UF_USER_CAR_DEMO",
							),
							"PATH_TO_SONET_USER_PROFILE" => "",	// Шаблон пути к странице пользователя
							"PROFILE_URL" => "",	// Путь к странице пользователя
							"DATE_TIME_FORMAT" => "d.m.Y H:i:s",	// Формат показа даты и времени
							"SHOW_YEAR" => "Y",	// Показывать год рождения
						),
						false
					);?>				
                    <div class="acc-menu">
                        <div class="container">
                            <nav class="acc-menu__menu">
								<?$APPLICATION->IncludeComponent("bitrix:menu", "profile", Array(
									"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
										"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
										"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
										"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
										"DELAY" => "N",	// Откладывать выполнение шаблона меню
										"MAX_LEVEL" => "1",	// Уровень вложенности меню
										"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
											0 => "",
										),
										"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
										"MENU_CACHE_TYPE" => "A",	// Тип кеширования
										"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
										"ROOT_MENU_TYPE" => "personal",	// Тип меню для первого уровня
										"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
									),
									false
								);?>							
                            </nav>
                        </div>
                    </div>
				<?endif;?>

				<div class="container">
			<?endif;?>
