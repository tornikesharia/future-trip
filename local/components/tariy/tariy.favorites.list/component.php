<?php session_start();
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

$arResult["MODULES_NAME"] = "tariy.favorites";

if( $arResult["MODULES_NAME"] ){
        global $TPrintFavorites;
        $cookie = $_COOKIE["T_FAVORITES_ID_NEW"];
        //$cookie = $APPLICATION->get_cookie("T_FAVORITES_ID");
        $idFavorites = false;
        if( !empty($TPrintFavorites["FAVORITES_ID"]) ) {
            $idFavorites = explode(":",$TPrintFavorites["FAVORITES_ID"]);
        }else if( !empty($cookie) ) {
            $idFavorites = explode(":",$cookie);
        }
      
        
        //
        if( !empty($arParams["SORT_BY1"]) || !empty($arParams["SORT_ORDER1"]) ){
            $sort[$arParams["SORT_BY1"]]  = $arParams["SORT_ORDER1"];
        }else{
            $sort["ACTIVE_FROM"] = "DESC";
        }
        if( !empty($arParams["SORT_BY2"]) || !empty($arParams["SORT_ORDER2"]) ){
            $sort[$arParams["SORT_BY2"]]  = $arParams["SORT_ORDER2"];
        }else{
            $sort["SORT"] = "ASC";
        }
        
        //
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
        $res = CIBlockElement::GetList(
                $sort,
                Array("ID"=>$idFavorites, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y"),
                false,
                Array("nPageSize"=>100),
                Array()
                );
        while($ob = $res->GetNextElement()){
            $arFields[$ob->GetFields()["ID"]] = $ob->GetFields();
            $arFields[$ob->GetFields()["ID"]]["PROPERTY"] = $ob->GetProperties();
        }

        foreach($arFields as $keyItem => $arItem){
            $arResult["ITEMS"][] = $arItem;
        }

        if ( $this->StartResultCache(1) ):
            $this->IncludeComponentTemplate();
        endif;        
}