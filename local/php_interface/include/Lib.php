<?php

class Lib {
    /*
     * Функция распечатки массива
     * $var = array(); массив
     * dump($var); массив распечатывается только админу
     * 
     * $shodow - если true то печатаем всем но в <!-- --> коде (на страницу не выводим)
     * dump($var,true);
     * 
     * $root печатаем для всех
     * dump($var,false,true);
     */
    public function Dump($var, $shodow=false, $root=false) {
        global $USER;

        if($shodow != true){
            if($root != true){
                if($USER->isAdmin()) {
                    print "<pre style='font-size:11px; margin-left: 20px;'>";
                    print var_dump($var);
                    print "</pre>";
                }
            } else {
                print "<pre style='font-size:11px; margin-left: 20px;'>";
                print var_dump($var);
                print "</pre>";
            }
        } else {
            print "<!--<pre style='font-size:11px; margin-left: 20px;'>";
            print var_dump($var);
            print "</pre>-->";
        }
    }

    public function Debug($array, $shodow=false, $root=false) {
        global $USER;

        if($shodow != true){
            if($root != true){
                if($USER->isAdmin()) {
                    echo "<pre style='font-size:11px; margin-left: 20px;'>";
                    print_r($array);
                    echo "</pre>";
                }
            } else {
                echo "<pre style='font-size:11px; margin-left: 20px;'>";
                print_r($array);
                echo "</pre>";
            }
        } else {
            echo "<!--<pre style='font-size:11px; margin-left: 20px;'>";
            print_r($array);
            echo "</pre>-->";
        }
    }      
    
    /*
     * Массив для операторов if
     * Возвращяет true или false
     * нужен чтобы ограничить видемость, только для Админа
     * 
     * if(admin()){
     *  // Видно только админу
     * }
     * если админ, вернет true
     */
    public function Admin(){
        global $USER;
        if( $USER->isAdmin() ) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Массив для операторов if
     * Возвращяет true или false
     * нужен чтобы ограничить видемость, только для Авторизованных пользователей
     * 
     * if(user()){
     *  // Видно только авторизованные пользователи
     * }
     * если авторизованный пользователь, вернет true
     */
    public function User() {
        global $USER;
        if($USER->IsAuthorized()) {
            return true;
        } else {
            return false;
        }
    }
    
    /*
     * Проверяем, есть ли папка в урле
     * Возвращяет true или false
     * 
     * urlPach("искомая директория в урле")
     * если она еть, вернет true
     */
    public function UrlContains($pachURL){	
            $url = $_SERVER['REQUEST_URI'];
            $arr = parse_url($url);
            $path = $arr["path"];

            if( strpos($path, $pachURL )!==false ){
                    return true;
            } else {
                    return false;
            }
    }
    
    /*
     * Функция проверяет искомую страницу и ту на который мы находимся по урлу
     * Возвращяет true или false
     * 
     * urlPach("искомый урл")
     * если совпадает, вернет true
     */
    public function UrlLinck($pachURL){
        $url = $_SERVER['REQUEST_URI'];
        $arr = parse_url($url);
        $path = $arr["path"];

        if( $path == $pachURL ){
            return true;
        } else {
            return false;
        }
    }
	
    public function GetDomainName(){
            if(isset($_SERVER['SERVER_NAME'])) 
                $DOMAIN_NAME = $_SERVER['SERVER_NAME'];
            else if(!isset($_SERVER['SERVER_NAME']) && isset($_SERVER['HTTP_HOST']))
                $DOMAIN_NAME = $_SERVER['HTTP_HOST'];
            else 
                $DOMAIN_NAME = SITE_SERVER_NAME;

            return $DOMAIN_NAME;
    }  
	
	public function GetFullUrl(){
		$uri = $_SERVER['REQUEST_URI'];
		 
		$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		 
		$url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	
		return  $url;
	
	}	
	
    public function LimitLetters($string, $length)
    {
      $string = strip_tags($string);
      if(strlen($string)<=$length)
      {
        echo $string;
      }
      else
      {
        $y=substr($string, 0, $length) . '...';
        echo $y;
      }

    }
	
	public function GetDirFromUrl($level){
		$arDirsClear = explode("?", $_SERVER["REQUEST_URI"]);
		$arDirs = explode("/", $arDirsClear[0]);
		if($arDirs[$level] && is_string($arDirs[$level])) $dirLevel = $arDirs[$level]; else $dirLevel = "";
		return $dirLevel;
	}

    // Check whether string contains numbers
    public function ContainsNumbers($String){
        return preg_match('/\\d/', $String) > 0;
    } 

	// allows only limited IP addresses or redirects to given url
    public function Block_IP($arr, $URL){
        $ip = $_SERVER['REMOTE_ADDR'];
        if(!in_array($ip, $arr)){
            LocalRedirect($URL);
        }
    }

    /*
     * Функция проверки пароля
     * $userId - id авторизованного пользователя
     * $password - проверяемый пароль
     */
    public function IsUserPassword($userId, $password){
        $userData = CUser::GetByID($userId)->Fetch();

        $salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));

        $realPassword = substr($userData['PASSWORD'], -32);
        $password = md5($salt.$password);

        return ($password == $realPassword);
    }
	
	public function randomString(){
		$date = date("Y d m h:i:sa");
		return md5($date);
	}

	public function formatBytesExact($bytes, $precision = 2) { 
		$units = array('B', 'KB', 'MB', 'GB', 'TB'); 

		$bytes = max($bytes, 0); 
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
		$pow = min($pow, count($units) - 1); 

		// Uncomment one of the following alternatives
		// $bytes /= pow(1024, $pow);
		// $bytes /= (1 << (10 * $pow)); 

		return round($bytes, $precision) . ' ' . $units[$pow]; 
	} 
	
	public function formatBytesRound($size, $precision = 2)
	{
		$base = log($size, 1024);
		$suffixes = array('', 'КБ', 'МБ', 'ГБ', 'ТБ');   

		return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
	}

	// Creates directory
	public function make_dir($path){
		if(!file_exists($path)){
			@mkdir($path);	
			@chmod($path, 0755);
		}
	}
	// Deletes directory
	public static function delDirs($dir) { 
	   $files = array_diff(scandir($dir), array('.','..')); 
		foreach ($files as $file) { 
		  (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
		} 
		return rmdir($dir); 
	} 	
	// gets file extension from string
	public function ext($text){
		$a = preg_split("/\./",$text);
		return $a[1];
	}
	public function without_ext($text){
		$a = preg_split("/\./",$text);
		return $a[0];
	}
	
	public function CreateRusDate($date){
		if($date){
			$time = strtotime($date);
			$hour_minute = date('H:i', $time);
			$day = date('d', $time);
			$month = date('m', $time);
			$year = date('Y', $time);

			switch ($month) {
				case '01': $month = 'января'; break;
				case '02': $month = 'февраля'; break;
				case '03': $month = 'марта'; break;
				case '04': $month = 'апреля'; break;
				case '05': $month = 'мая'; break;
				case '06': $month = 'июня'; break;
				case '07': $month = 'июля'; break;
				case '08': $month = 'августя'; break;
				case '09': $month = 'сентября'; break;
				case '10': $month = 'октября'; break;
				case '11': $month = 'ноября'; break;
				case '12': $month = 'декабря'; break;
			}
			
			$arFormatedDate = array(
				"HOUR_MINUTE" => $hour_minute,
				"DAY" => $day,
				"MONTH" => $month,
				"YEAR" => $year
			);
			
			return $arFormatedDate;
		}
	}
	
/**
	 * Счетчик обратного отсчета
	 *
	 * @param mixed $date
	 * @return
	 */
	public function downcounter($date){
	    $check_time = strtotime($date) - time();
	    if($check_time <= 0){
	        return false;
	    }

	    $days = floor($check_time/86400);
	    $hours = floor(($check_time%86400)/3600);
	    $minutes = floor(($check_time%3600)/60);
	    $seconds = $check_time%60; 

	    $str = '';
	    if($days > 0) $str .= self::declension($days,array('день','дня','дней')).' ';
	    if($hours > 0) $str .= self::declension($hours,array('час','часа','часов')).' ';
	    if($minutes > 0) $str .= self::declension($minutes,array('минута','минуты','минут')).' ';
	    //if($seconds > 0) $str .= self::declension($seconds,array('секунда','секунды','секунд'));

	    return $str;
	}


	/**
	 * Функция склонения слов
	 *
	 * @param mixed $digit
	 * @param mixed $expr
	 * @param bool $onlyword
	 * @return
	 */
	public function declension($digit,$expr,$onlyword=false){
	    if(!is_array($expr)) $expr = array_filter(explode(' ', $expr));
	    if(empty($expr[2])) $expr[2]=$expr[1];
	    $i=preg_replace('/[^0-9]+/s','',$digit)%100;
	    if($onlyword) $digit='';
	    if($i>=5 && $i<=20) $res=$digit.' '.$expr[2];
	    else
	    {
	        $i%=10;
	        if($i==1) $res=$digit.' '.$expr[0];
	        elseif($i>=2 && $i<=4) $res=$digit.' '.$expr[1];
	        else $res=$digit.' '.$expr[2];
	    }
	    return trim($res);
	}	
	
    /*
     *
     * *************************************************************************
     * Функция проверки пароля
     * $userId - id авторизуемого пользователя
     * $password - проверяемый пароль
     * *************************************************************************
     *
     */
    public function UserPassword($userId, $password)
    {
        $userData = CUser::GetByID($userId)->Fetch();
        $salt = substr($userData['PASSWORD'], 0, (strlen($userData['PASSWORD']) - 32));
        $realPassword = substr($userData['PASSWORD'], -32);
        $password = md5($salt . $password);

        return ($password == $realPassword);
    }	

	public function AddGetParamToUrl(&$url, $varName, $value)
	{
		// is there already an ?
		if (strpos($url, "?"))
		{
			$url .= "&" . $varName . "=" . $value; 
		}
		else
		{
			$url .= "?" . $varName . "=" . $value;
		}
		
		return $url;		
	}

	public function YoutubeCode($url){
		$url = trim($url);
		if($url){
			$parts = parse_url($url);
			parse_str($parts['query'], $query);
			$video_id = $query['v'];
			return $video_id;
		} else {
			return false;
		}			
		
	}
	
	
}

$Lib = new Lib();