<?
define("HIDE_SIDEBAR", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?><?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "bootstrap_v4_main", Array(
	"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"COLUMNS_LIST" => array(
			0 => "NAME",
			1 => "DISCOUNT",
			2 => "QUANTITY",
			3 => "SUM",
			4 => "PROPS",
			5 => "DELETE",
			6 => "DELAY"
		),
		"SHOW_FILTER" => "N",	// Отображать фильтр товаров
		"TOTAL_BLOCK_DISPLAY" => array(	// Отображение блока с общей информацией по корзине
			0 => "bottom",
		),
		"PRICE_DISPLAY_MODE" => "N",	// Отображать цену в отдельной колонке
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"PATH_TO_ORDER" => "/personal/order/make/",	// Страница оформления заказа
		"HIDE_COUPON" => "N",	// Спрятать поле ввода купона
		"QUANTITY_FLOAT" => "N",	// Использовать дробное значение количества
		"PRICE_VAT_SHOW_VALUE" => "Y",	// Отображать значение НДС
		"TEMPLATE_THEME" => "site",	// Цветовая тема
		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы
		"AJAX_OPTION_ADDITIONAL" => "",
		"OFFERS_PROPS" => array(	// Свойства, влияющие на пересчет корзины
			//0 => "COLOR_REF",
		),
		"COMPONENT_TEMPLATE" => "bootstrap_v4_main",
		"DEFERRED_REFRESH" => "N",	// Использовать механизм отложенной актуализации данных товаров с провайдером
		"USE_DYNAMIC_SCROLL" => "Y",	// Использовать динамическую подгрузку товаров
		"SHOW_RESTORE" => "N",	// Разрешить восстановление удалённых товаров
		"COLUMNS_LIST_EXT" => array(	// Выводимые колонки
			0 => "PREVIEW_PICTURE",
			1 => "DISCOUNT",
			2 => "DELETE",
			3 => "DELAY",
			4 => "PROPS",
			5 => "SUM"
		),
		"COLUMNS_LIST_MOBILE" => array(	// Колонки, отображаемые на мобильных устройствах
			0 => "PREVIEW_PICTURE",
			1 => "DISCOUNT",
			2 => "DELETE",
			3 => "DELAY",
			4 => "PROPS",
			5 => "SUM",
		),
		"DISPLAY_MODE" => "extended",	// Режим отображения корзины
		"SHOW_DISCOUNT_PERCENT" => "Y",	// Показывать процент скидки рядом с изображением
		"DISCOUNT_PERCENT_POSITION" => "bottom-right",	// Расположение процента скидки
		"PRODUCT_BLOCKS_ORDER" => "props,sku,columns",	// Порядок отображения блоков товара
		"USE_PRICE_ANIMATION" => "Y",	// Использовать анимацию цен
		"LABEL_PROP" => "",	// Свойства меток товара
		"USE_PREPAYMENT" => "N",	// Использовать предавторизацию для оформления заказа (PayPal Express Checkout)
		"CORRECT_RATIO" => "Y",	// Автоматически рассчитывать количество товара кратное коэффициенту
		"AUTO_CALCULATION" => "Y",	// Автопересчет корзины
		"ACTION_VARIABLE" => "basketAction",	// Название переменной действия
		"COMPATIBLE_MODE" => "Y",	// Включить режим совместимости
		"ADDITIONAL_PICT_PROP_2" => "-",	// Дополнительная картинка [Товары]
		"ADDITIONAL_PICT_PROP_3" => "-",	// Дополнительная картинка [Товары (предложения)]
		"BASKET_IMAGES_SCALING" => "adaptive",	// Режим отображения изображений товаров
		"USE_GIFTS" => "Y",	// Показывать блок "Подарки"
		"GIFTS_PLACE" => "BOTTOM",	// Вывод блока "Подарки"
		"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",	// Текст заголовка "Подарки"
		"GIFTS_HIDE_BLOCK_TITLE" => "N",	// Скрыть заголовок "Подарки"
		"GIFTS_TEXT_LABEL_GIFT" => "Подарок",	// Текст метки "Подарка"
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара
		"GIFTS_SHOW_OLD_PRICE" => "Y",	// Показывать старую цену
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",	// Показывать процент скидки
		"GIFTS_MESS_BTN_BUY" => "Выбрать",	// Текст кнопки "Выбрать"
		"GIFTS_MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",	// Количество элементов в строке
		"GIFTS_CONVERT_CURRENCY" => "Y",	// Показывать цены в одной валюте
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",	// Не отображать товары, которых нет на складах
		"USE_ENHANCED_ECOMMERCE" => "N",	// Отправлять данные электронной торговли в Google и Яндекс
	),
	false
);?>
<?/*
                        <div class="cart-upsells">
                            <div class="upsells__title-block">
                                <div class="upsells__title">Рекомендуемые тоже</div>
                                <a href="#" class="moto-more-link upsells__more-link">
                                    <span class="moto-more-link__text">Увидеть все модели</span>
                                    <span class="moto-right-arrow">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/angle-right.svg" alt="" class="moto-right-arrow__icon">
                                    </span>
                                </a>
                            </div>
            
                            <div class="upsells__list">
                                <div class="row no-gutters">
            
                                    <div class="col-sm-6 col-xl-3 mb-3">
                                        <div class="vcard">
                                            <div class="vcard__top">
                                                <div class="vcard__img-block">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/examples/product1.png" alt="" class="vcard__img">
                                                </div>
                                                <a href="#" class="vcard__title">Горный велосипед  CRS 120</a>
                                                <div class="vcard__buttons">
                                                    <div class="button_tr vcard__price">535 999 ₽</div>
                                                    <a href="#" class="button_yellow vcard__button">Купить</a>
                                                </div>
                                            </div>
                                            <div class="vcard__bottom">
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/star-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <a href="#" class="vcard__attr-link">Добавить в избранное</a>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/speedometer-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Макс.скорость - 130 км/ч</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/car-battery-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">250 Вт, Li-ion, 48В, 10Ач</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/cog-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Запас хода 50 км</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bicycle-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Рама из карбона</div>
                                                </div>
                                            </div>
                                        </div>                        
									</div>
                                    <div class="col-sm-6 col-xl-3 mb-3">
                                        <div class="vcard">
                                            <div class="vcard__top">
                                                <div class="vcard__img-block">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/examples/product1.png" alt="" class="vcard__img">
                                                </div>
                                                <a href="#" class="vcard__title">Горный велосипед  CRS 120</a>
                                                <div class="vcard__buttons">
                                                    <div class="button_tr vcard__price">535 999 ₽</div>
                                                    <a href="#" class="button_yellow vcard__button">Купить</a>
                                                </div>
                                            </div>
                                            <div class="vcard__bottom">
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/star-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <a href="#" class="vcard__attr-link">Добавить в избранное</a>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/speedometer-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Макс.скорость - 130 км/ч</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/car-battery-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">250 Вт, Li-ion, 48В, 10Ач</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/cog-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Запас хода 50 км</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bicycle-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Рама из карбона</div>
                                                </div>
                                            </div>
                                        </div>                        
									</div>
                                    <div class="col-sm-6 col-xl-3 mb-3">
                                        <div class="vcard">
                                            <div class="vcard__top">
                                                <div class="vcard__img-block">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/examples/product1.png" alt="" class="vcard__img">
                                                </div>
                                                <a href="#" class="vcard__title">Горный велосипед  CRS 120</a>
                                                <div class="vcard__buttons">
                                                    <div class="button_tr vcard__price">535 999 ₽</div>
                                                    <a href="#" class="button_yellow vcard__button">Купить</a>
                                                </div>
                                            </div>
                                            <div class="vcard__bottom">
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/star-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <a href="#" class="vcard__attr-link">Добавить в избранное</a>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/speedometer-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Макс.скорость - 130 км/ч</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/car-battery-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">250 Вт, Li-ion, 48В, 10Ач</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/cog-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Запас хода 50 км</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bicycle-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Рама из карбона</div>
                                                </div>
                                            </div>
                                        </div>                        
									</div>
                                    <div class="col-sm-6 col-xl-3 mb-3">
                                        <div class="vcard">
                                            <div class="vcard__top">
                                                <div class="vcard__img-block">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/examples/product1.png" alt="" class="vcard__img">
                                                </div>
                                                <a href="#" class="vcard__title">Горный велосипед  CRS 120</a>
                                                <div class="vcard__buttons">
                                                    <div class="button_tr vcard__price">535 999 ₽</div>
                                                    <a href="#" class="button_yellow vcard__button">Купить</a>
                                                </div>
                                            </div>
                                            <div class="vcard__bottom">
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/star-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <a href="#" class="vcard__attr-link">Добавить в избранное</a>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/speedometer-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Макс.скорость - 130 км/ч</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/car-battery-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">250 Вт, Li-ion, 48В, 10Ач</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/cog-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Запас хода 50 км</div>
                                                </div>
                                                <div class="vcard__attr">
                                                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bicycle-yellow.svg" alt="" class="vcard__attr-icon">
                                                    <div class="vcard__attr-text">Рама из карбона</div>
                                                </div>
                                            </div>
                                        </div>                        
									</div>
            
                                </div>
                            </div>
            
                        </div><!--/.cart-upsells -->
*/?>
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.viewed.product",
	"",
	Array(
		"VIEWED_COUNT" => "4",
		"VIEWED_NAME" => "Y",
		"VIEWED_IMAGE" => "Y",
		"VIEWED_PRICE" => "Y",
		"VIEWED_CURRENCY" => "default",
		"VIEWED_CANBUY" => "Y",
		"VIEWED_CANBASKET" => "Y",
		"VIEWED_IMG_HEIGHT" => "308",
		"VIEWED_IMG_WIDTH" => "291",
		"BASKET_URL" => "/personal/cart/",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"SET_TITLE" => "N"
	)
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>