<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule('iblock');

foreach ($arResult['ORDERS'] as $k => $arOrder) {
    foreach ($arOrder["BASKET_ITEMS"] as $kItems => $arItems) {
        $elID[] = $arItems["PRODUCT_ID"];
    }
}
$elIDNew = array_unique($elID);

$res = CIBlockElement::GetList(
    Array(
        "SORT"=>"ASC"
    ),
    Array("IBLOCK_ID"=>IntVal(2), "ID"=>$elIDNew),
    false,
    false,
    Array("ID", "NAME", "DETAIL_PICTURE")
);
while($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $elIDNewItem[$arFields["ID"]] = Array(
        "PREVIEW_PICTURE" => CFile::ResizeImageGet(
            CFile::GetFileArray($arFields["DETAIL_PICTURE"]),
            array('width' => 90, 'height' => 90),
            BX_RESIZE_IMAGE_EXACT,
            true,
            Array()
        ),
        "DETAIL_PICTURE" => CFile::GetFileArray($arFields["DETAIL_PICTURE"])
    );
}
foreach ($arResult['ORDERS'] as $k => $arOrder) {
    foreach ($arOrder["BASKET_ITEMS"] as $kItems => $arItems) {
        $arResult['ORDERS'][$k]["BASKET_ITEMS"][$kItems]["PREVIEW_PICTURE"]["SRC"] = $elIDNewItem[$arItems["PRODUCT_ID"]]["PREVIEW_PICTURE"]["src"];
        $arResult['ORDERS'][$k]["BASKET_ITEMS"][$kItems]["DETAIL_PICTURE"] = $elIDNewItem[$arItems["PRODUCT_ID"]]["DETAIL_PICTURE"];
    }
}