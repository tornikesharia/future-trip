<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

		  <?if($CurPage !== '/'):?> 
              </div><!--/.container -->	
          </div><!--/.page-content -->			  
		  <?endif;?>
		  <?if($CurPage == '/delivery-and-payment/'):?>
                <div class="feedback">
                    <div class="feedback__inner">
                        <div class="container">
                            <form action="/local/ajax/qusetions.php" class="feedback__form">
								<input type="hidden" name="URL" value="<?=Lib::GetFullUrl();?>">
                                <div class="feedback__title">Есть вопросы?</div>
                                <div class="feedback__subtitle">Наши специалисты помогут Вам!</div>
                
								<div class="feedback__form-msg mb-3"></div>
                                <div class="form-group">
                                    <input type="text" class="moto-input feedback__input pl-3" name="NAME" placeholder="Как вас зовут?" required />
                                </div>
                                <div class="form-group">
                                    <input type="text" class="moto-input feedback__input pl-3" name="PHONE" placeholder="Укажите Ваш телефон" required />
                                </div>
                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="feedback__policy-checkbox" checked="checked" required />
                                        <label class="custom-control-label" for="feedback__policy-checkbox">Вы согласны с Политикой
                                            конфиденциальности и Политикой обработки данных</label>
                                    </div>
                                </div>
                
                                <div class="feedback__button-block">
                                    <button class="button_tr feedback__button">получить консультацию</button>
                                </div>
                                
                                
                            </form>
                        </div>
                    </div>
                </div>		  
			<?endif;?>

      </main><!--/.page -->
      <footer class="footer">
          <div class="container">
              <div class="row">
                  <div class="col-sm-6 col-lg-3 footer__col1">
                      <div class="footer1">
                          <a href="/" class="footer1__logo-block">
                              <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/logo-white.svg" alt="" class="footer1__logo-img">
                          </a>
                          <div class="footer1__title"><?$APPLICATION->IncludeFile(__include__ . "footer_copyright.php", Array(), Array("MODE" => "text"));?></div>
                          <a href="#" class="footer1__policy-link link-ext">Политика конфиденциальности</a>
                          <button type="button" class="footer1__to-top js__to-top-button">
                              <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/to-top.svg" alt="" class="footer1__to-top-icon">
                              <span class="footer1__to-top-text">В начало страницы</span>
                          </button>
                      </div>
                  </div>
                  <div class="col-sm-6 col-lg-3 footer__col2">
                      <div class="footer2">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_1", Array(
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
								"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
								"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
								"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
								"DELAY" => "N",	// Откладывать выполнение шаблона меню
								"MAX_LEVEL" => "1",	// Уровень вложенности меню
								"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
									0 => "",
								),
								"MENU_CACHE_TIME" => "3600000000000",	// Время кеширования (сек.)
								"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
								"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
								"ROOT_MENU_TYPE" => "bottom_1",	// Тип меню для первого уровня
								"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							),
							false
						);?>
                      </div>						
                  </div>
                  <div class="col-sm-6 col-lg-3 footer__col3">
                      <div class="footer3">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_2", Array(
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
								"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
								"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
								"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
								"DELAY" => "N",	// Откладывать выполнение шаблона меню
								"MAX_LEVEL" => "1",	// Уровень вложенности меню
								"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
									0 => "",
								),
								"MENU_CACHE_TIME" => "3600000000000",	// Время кеширования (сек.)
								"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
								"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
								"ROOT_MENU_TYPE" => "bottom_2",	// Тип меню для первого уровня
								"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							),
							false
						);?>					  
                      </div>
                  </div>
					<div class="col-sm-6 col-lg-3 footer__col4">
						<div class="footer4">
							<div class="footer4__address"><?$APPLICATION->IncludeFile(__include__ . "footer_address.php", Array(), Array("MODE" => "html"));?></div>
							<?$APPLICATION->IncludeFile(__include__ . "footer_phone.php", Array(), Array("MODE" => "html"));?>
							<?$APPLICATION->IncludeFile(__include__ . "footer_email.php", Array(), Array("MODE" => "html"));?>
							<?$APPLICATION->IncludeComponent("bitrix:menu", "social", Array(
								"ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
								"CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
								"DELAY" => "N",    // Откладывать выполнение шаблона меню
								"MAX_LEVEL" => "1",    // Уровень вложенности меню
								"MENU_CACHE_GET_VARS" => array(    // Значимые переменные запроса
									0 => "",
								),
								"MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
								"MENU_CACHE_TYPE" => "N",    // Тип кеширования
								"MENU_CACHE_USE_GROUPS" => "Y",    // Учитывать права доступа
								"ROOT_MENU_TYPE" => "social",    // Тип меню для первого уровня
								"USE_EXT" => "N",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
								),
								false
							);?>						  
							<div class="footer4__copyright">
								Разработка сайта-
								<a href="http://pureidea.ru" target="_blank">PureIdea</a>
							</div>
						</div>
					</div>
              </div>
          </div>
      </footer>    
	</div>

     <!-- Окно добавленного в корзину товара -->
     <div class="modal fade simple-modal cart-modal" id="cart-modal" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-body">
                     <button type="button" class="close simple-modal__close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                     <div class="simple-modal__title">Товар добавлен в корзину! </div>
     
                     <div class="cart-modal__content">
                         <div class="cart-modal__img-block">
                             <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/examples/product3.png" alt="" class="cart-modal__img">
                         </div>
                         <div class="cart-modal__text-block">
                             <a href="#" class="cart-modal__item-title">Электроцикл Denzel Rush I</a>
							 <div class="cart-modal__item-props">							 
							 <?/*
                             <div class="cart-modal__item-attr">Артикул: <span>6500900</span></div>
                             <div class="cart-modal__item-attr">Цвет: <span>Черный</span></div>
							 */?>
							 </div>
                         </div>
                     </div>
                     <div class="cart-modal__sum">Всего в корзине 15 товаров на сумму 100 500 руб.</div>
                     <div class="cart-modal__buttons">
                         <a href="/personal/cart/" class="button_yellow cart-modal__cart-button">Перейти в корзину</a>
                         <a href="#" class="button_yellow cart-modal__continue-button" data-dismiss="modal">продолжить покупки</a>
                     </div>
     
                 </div><!-- /.modal-body -->
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->

     <!-- Окно с информацией (состоит из стандатных элементов) -->
     <div class="modal fade simple-modal info-modal" id="info-modal" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-body">
                     <button type="button" class="close simple-modal__close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                     <div class="simple-modal__title"></div>
					 <div class="simple-modal__body"></div>
                 </div><!-- /.modal-body -->
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
     
     
     <!-- Окно ввода телефона -->
     <div class="modal fade white-modal phone-modal" id="phone-modal" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
             <div class="modal-content">
                 <div class="modal-body">
                     <button type="button" class="close white-modal__close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
     
                     <div class="phone-modal__title">Введите свой номер телефона, чтобы мы могли связаться с вами </div>
     
                     <form action="/local/ajax/call_order.php" class="phone-modal__form call-me-form">
						<div class="call-me-form__msg mb-2"></div>
                        <div class="phone-modal__input-block">
                            <input type="text"
                                    class="moto-input phone-modal__input"
									name="PHONE"
                                    placeholder="+ 7 ___-___-__-__"
                                    required>
                        </div>
                        <div class="phone-modal__button-block">
                            <button type="submit" class="button_yellow phone-modal__button">ОТПРАВИТЬ</button>
                        </div>
                     </form>
     
     
                 </div><!-- /.modal-body -->
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
     
     
     <!-- Окно ввода подтверждения номера телефона -->
     <div class="modal fade white-modal confirm-modal" id="confirm-modal" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
             <div class="modal-content">
                 <div class="modal-body">
                     <button type="button" class="close white-modal__close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
     
                     <div class="confirm-modal__title">Подтвердите свой номер телефона.
                         Вам необходимо ввести четырехзначный код из SMS</div>
     
                     <form action="" class="confirm-modal__form">
                         <div class="confirm-modal__input-block">
                             <input type="text"
                                    class="moto-input confirm-modal__input"
                                    placeholder="&#11044;  &#11044;  &#11044;  &#11044;"
                                    required>
                         </div>
                         <div class="confirm-modal__button-block">
                             <button type="submit" class="button_yellow confirm-modal__button">ОТПРАВИТЬ</button>
                         </div>
                     </form>
     
     
                 </div><!-- /.modal-body -->
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
     
     
     <!-- Окно успешной регистрации -->
     <div class="modal fade success-reg-modal" id="success-reg-modal" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-body">
                     <button type="button" class="close success-reg-modal__close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
     
                     <div class="success-reg-modal__img">
                         <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/checkmark.svg" alt="" class="success-reg-modal__icon">
                     </div>
     
                     <div class="success-reg-modal__title">Вы успешно зарегистированы!</div>
                     <div class="success-reg-modal__text">Мы вышлем Вам ссылку подтверждения на указанный Вами адрес электронной почты</div>
     
                     <button class="button_yellow success-reg-modal__button">ПОДВЕРДИТЬ РЕГИСТРАЦИЮ</button>
     
     
                     <a href="#" class="success-reg-modal__link" data-dismiss="modal">
                         <svg class="success-reg-modal__link-icon"><use xlink:href="#arrow-right"></use></svg>
                         <span class="success-reg-modal__link-text">Обратно в Motoryzed Systems</span>
                     </a>
                     
     
                 </div><!-- /.modal-body -->
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
     
     
     <!-- Окно входа -->
     <div class="modal fade yellow-modal login-modal" id="login-modal" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-body">
     
                     <div class="yellow-modal__header">
                         <div class="yellow-modal__title">Укажите Ваши данные</div>
                         <button type="button" class="close yellow-modal__close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                         </button>
                     </div>
     
                     <!--в дизайне были стили валидного и невалидного ввода, для валидного добавить .login-modal__input-block_ok к -->
                     <!--.login-modal__input-block и .login-modal__input_ok к .login-modal__input, -->
                     <!--а для невалидного добавить .login-modal__input-block_error к-->
                     <!--.login-modal__input-block и .login-modal__input_error к .login-modal__input,
                     может быть оно и не нужно-->
                     <div class="yellow-modal__content">
                         <form action="/local/ajax/login.php" class="login-modal__form">
							<input type="hidden" name="CUR_URL" value="<?=Lib::GetFullUrl();?>" /> 
							<div class="login-modal__input-block">
							 <input type="text"
									class="moto-input login-modal__input"
									name="EMAIL"
									placeholder="Укажите  электронную почту"
									required>
							 <svg class="login-modal__input-icon"><use xlink:href="#letter"></use></svg>
							</div>
							<div class="login-modal__input-block">
								<input type="password"
									class="moto-input login-modal__input"
									name="PASSWORD"
									placeholder="Укажите пароль"
									required>
								<svg class="login-modal__input-icon"><use xlink:href="#key"></use></svg>
							</div>
							<div class="modal__error-text"></div>
							<a href="/auth/?forgot_password=yes" class="login-modal__reg-link login-modal__forgot-password">Забыли свой пароль?</a><br />
                            <a href="javascript:void(0)" class="login-modal__reg-link login-modal__not-registered">Не зарегестрированы?</a>
                            <button type="submit" class="button_yellow login-modal__submit">Войти</button>
                         </form>
                     </div>
     
                 </div><!-- /.modal-body -->
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
     
     
     
     <!-- Окно регистрации -->
     <div class="modal fade yellow-modal reg-modal" id="reg-modal" tabindex="-1" role="dialog" aria-hidden="true">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <div class="modal-body">
     
                     <div class="yellow-modal__header">
                         <div class="yellow-modal__title">Укажите Ваши данные</div>
                         <button type="button" class="close yellow-modal__close" data-dismiss="modal" aria-label="Close">
                             <span aria-hidden="true">&times;</span>
                         </button>
                     </div>
     
                     <div class="yellow-modal__content">
     
                         <div class="nav nav-pills reg-modal__tab-buttons" role="tablist">
                             <a class="nav-link active reg-modal__tab-button"
                                id="reg-modal__tab1-button"
                                data-toggle="tab"
                                href="#reg-modal__tab_1"
                                role="tab"
                                aria-selected="true">Физическое лицо</a>
                             <a class="nav-link reg-modal__tab-button"
                                id="reg-modal__tab2-button"
                                data-toggle="tab"
                                href="#reg-modal__tab_2"
                                role="tab"
                                aria-selected="false">Юридическое лицо</a>
                         </div>
     
                         <div class="tab-content">
                             <div class="tab-pane fade show active reg-modal__tab reg-modal__tab_1" id="reg-modal__tab_1" role="tabpanel">
                                 <form class="reg-modal__form reg-modal__form_1" id="reg-modal__form_1" action="/local/ajax/register.php">
									<input type="hidden" name="CUR_URI" value="<?=Lib::GetFullUrl();?>" />
									<input type="hidden" name="USER_TYPE" value="individual" />
									<div class="popup-reg__error" style="display:none; color:red;"></div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="text"
                                                id="reg-modal__input_name"
                                                class="moto-input reg-modal__input reg-modal__input_name"
                                                name="NAME"
                                                required>
                                         <!-- если убрать required из поля то разноцветный плейсхолдер работать не будет -->
                                         <label for="reg-modal__input_name"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">Ваше имя</label>
                                     </div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="text"
                                                id="reg-modal__input_email"
                                                class="moto-input reg-modal__input reg-modal__input_email"
                                                name="EMAIL"
                                                required>
                                         <label for="reg-modal__input_email"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">E-mail</label>
                                     </div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="text"
                                                id="reg-modal__input_phone"
                                                class="moto-input reg-modal__input reg-modal__input_phone"
                                                name="PERSONAL_PHONE"
                                                required>
                                         <label for="reg-modal__input_phone"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">Телефон</label>
                                     </div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="password"
                                                id="reg-modal__input_password1"
                                                class="moto-input reg-modal__input reg-modal__input_password1"
                                                name="PASSWORD"
                                                required>
                                         <label for="reg-modal__input_password1"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">Пароль</label>
                                     </div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="password"
                                                id="reg-modal__input_password2"
                                                class="moto-input reg-modal__input reg-modal__input_password2"
                                                name="CONFIRM_PASSWORD"
                                                required>
                                         <label for="reg-modal__input_password2"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">Подтверждение пароля</label>
                                     </div>
     
                                     <div class="modal__error-text"></div>
     
                                     <div class="reg-modal__required-explanation"><span>*</span> обязательные поля</div>
                                     <div class="reg-modal__text">
                                         Нажимая на кнопку “зарегистрироваться”,  Вы соглашаетесь на обработку персональных данных
                                     </div>
                                     <button type="submit" class="button_yellow reg-modal__submit-button">ЗАРЕГИСТРИРОВАТЬСЯ</button>
     
                                 </form>
                             </div><!--/.reg-modal__tab_2 -->
                             <div class="tab-pane fade reg-modal__tab reg-modal__tab_2" id="reg-modal__tab_2" role="tabpanel">
                                 <form action="/local/ajax/register.php" class="reg-modal__form reg-modal__form_2" id="reg-modal__form_2">
									<input type="hidden" name="CUR_URL" value="<?=Lib::GetFullUrl();?>" />
									<input type="hidden" name="USER_TYPE" value="legal" />
									<div class="popup-reg__error" style="display:none; color:red;"></div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="text"
                                                id="reg-modal__input_org-name"
                                                class="moto-input reg-modal__input reg-modal__input_org-name"
                                                name="WORK_COMPANY"
                                                required>
                                         <label for="reg-modal__input_org-name"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">Название организации</label>
                                     </div>
                                     <div class="row">
                                         <div class="col-12 col-sm">
                                             <div class="form-group moto__form-group_with-fake-placeholder">
                                                 <input type="text"
                                                        id="reg-modal__input_inn"
                                                        class="moto-input reg-modal__input reg-modal__input_inn"
                                                        name="UF_INN"
                                                        required>
                                                 <label for="reg-modal__input_inn"
                                                        class="moto__fake-placeholder reg-modal__fake-placeholder">ИНН</label>
                                             </div>
                                         </div>
                                         <div class="col-12 col-sm">
                                             <div class="form-group">
                                                 <input type="text"
                                                        id="reg-modal__input_kpp"
                                                        class="moto-input reg-modal__input reg-modal__input_kpp"
                                                        name="UF_KPP"
                                                        placeholder="КПП">
                                             </div>
                                         </div>
                                     </div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="text"
                                                id="reg-modal__input_legal-address"
                                                class="moto-input reg-modal__input reg-modal__input_legal-address"
                                                name="UF_LEGAL_ADDRESS"
                                                required>
                                         <label for="reg-modal__input_legal-address"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">Юридический адрес</label>
                                     </div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="text"
                                                id="reg-modal__input_physical-address"
                                                class="moto-input reg-modal__input reg-modal__input_physical-address"
                                                name="UF_PHYSICAL_ADDRESS"
                                                required>
                                         <label for="reg-modal__input_physical-address"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">Физический адрес</label>
                                     </div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="text"
                                                id="reg-modal__input_email-address"
                                                class="moto-input reg-modal__input reg-modal__input_email-address"
                                                name="EMAIL"
                                                required>
                                         <label for="reg-modal__input_email-address"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">E-mail адрес</label>
                                     </div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="text"
                                                id="reg-modal__input_legal-phone"
                                                class="moto-input reg-modal__input reg-modal__input_legal-phone"
                                                name="PERSONAL_PHONE"
                                                required>
                                         <label for="reg-modal__input_legal-phone"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">Телефон</label>
                                     </div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="password"
                                                id="reg-modal__input_ur-password1"
                                                class="moto-input reg-modal__input reg-modal__input_ur-password1"
                                                name="PASSWORD"
                                                required>
                                         <label for="reg-modal__input_phone"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">Пароль</label>
                                     </div>
                                     <div class="form-group moto__form-group_with-fake-placeholder">
                                         <input type="password"
                                                id="reg-modal__input_ur-password2"
                                                class="moto-input reg-modal__input reg-modal__input_ur-password2"
                                                name="CONFIRM_PASSWORD"
                                                required>
                                         <label for="reg-modal__input_phone"
                                                class="moto__fake-placeholder reg-modal__fake-placeholder">Подтверждение пароля</label>
                                     </div>
     
                                     <div class="modal__error-text"></div>
     
                                     <div class="reg-modal__required-explanation"><span>*</span> обязательные поля</div>
                                     <div class="reg-modal__text">
                                         Нажимая на кнопку “зарегистрироваться”,  Вы соглашаетесь на обработку персональных данных
                                     </div>
                                     <button type="submit" class="button_yellow reg-modal__submit-button">ЗАРЕГИСТРИРОВАТЬСЯ</button>
                                 </form>
                             </div><!--/.reg-modal__tab_2 -->
                         </div><!--/.tab-content -->
                         
                     </div>
     
                 </div><!-- /.modal-body -->
             </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->     <!--можно вынести в отдельный файл, но не будет работать в <ie11  -->
     <svg  style="display: none;" width="0" height="0" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" >
     	<symbol id="icon-arrow-right" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" width="512px" height="512px" viewBox="0 0 284.936 284.936" xml:space="preserve">
     		<g>
     			<path d="M277.515,135.9L144.464,2.857C142.565,0.955,140.375,0,137.9,0c-2.472,0-4.659,0.955-6.562,2.857l-14.277,14.275    c-1.903,1.903-2.853,4.089-2.853,6.567c0,2.478,0.95,4.664,2.853,6.567l112.207,112.204L117.062,254.677    c-1.903,1.903-2.853,4.093-2.853,6.564c0,2.477,0.95,4.667,2.853,6.57l14.277,14.271c1.902,1.905,4.089,2.854,6.562,2.854    c2.478,0,4.665-0.951,6.563-2.854l133.051-133.044c1.902-1.902,2.851-4.093,2.851-6.567S279.417,137.807,277.515,135.9z"/>
     			<path d="M170.732,142.471c0-2.474-0.947-4.665-2.857-6.571L34.833,2.857C32.931,0.955,30.741,0,28.267,0s-4.665,0.955-6.567,2.857    L7.426,17.133C5.52,19.036,4.57,21.222,4.57,23.7c0,2.478,0.95,4.664,2.856,6.567L119.63,142.471L7.426,254.677    c-1.906,1.903-2.856,4.093-2.856,6.564c0,2.477,0.95,4.667,2.856,6.57l14.273,14.271c1.903,1.905,4.093,2.854,6.567,2.854    s4.664-0.951,6.567-2.854l133.042-133.044C169.785,147.136,170.732,144.945,170.732,142.471z"/>
     		</g>
     	</symbol>
     	<symbol id="arrow-right" viewBox="0 0 268.832 268.832">
     		<path d="M265.171,125.577l-80-80c-4.881-4.881-12.797-4.881-17.678,0c-4.882,4.882-4.882,12.796,0,17.678l58.661,58.661H12.5 c-6.903,0-12.5,5.597-12.5,12.5c0,6.902,5.597,12.5,12.5,12.5h213.654l-58.659,58.661c-4.882,4.882-4.882,12.796,0,17.678 c2.44,2.439,5.64,3.661,8.839,3.661s6.398-1.222,8.839-3.661l79.998-80C270.053,138.373,270.053,130.459,265.171,125.577z"/>
     	</symbol>
     	<symbol  id="view-rows" viewBox="0 0 25 25">
     		<path stroke-width="0.25" d="M6.2,0.9H2.5c-0.9,0-1.7,0.7-1.7,1.7v3.7c0,0.9,0.7,1.7,1.7,1.7h3.7
     			c0.9,0,1.7-0.7,1.7-1.7V2.5C7.9,1.6,7.1,0.9,6.2,0.9z M6.5,6.2c0,0.1-0.1,0.2-0.2,0.2H2.5c-0.1,0-0.2-0.1-0.2-0.2V2.5
     			c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V6.2z"/>
     		<path stroke-width="0.25" d="M6.2,9.2H2.5c-0.9,0-1.7,0.7-1.7,1.7v3.6h0v0.1
     			c0,0.9,0.7,1.7,1.7,1.7h3.7c0.9,0,1.7-0.7,1.7-1.7v-3.7C7.9,10,7.1,9.2,6.2,9.2z M6.5,14.6c0,0.1-0.1,0.2-0.2,0.2H2.5
     			c-0.1,0-0.2-0.1-0.2-0.2v-3.7v0c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V14.6z"/>
     		<path stroke-width="0.25" d="M6.2,17.6H2.5c-0.9,0-1.7,0.7-1.7,1.7v3.6h0V23
     			c0,0.9,0.7,1.7,1.7,1.7h3.7c0.9,0,1.7-0.7,1.7-1.7v-3.7C7.9,18.4,7.1,17.6,6.2,17.6z M6.5,23c0,0.1-0.1,0.2-0.2,0.2H2.5
     			c-0.1,0-0.2-0.1-0.2-0.2v-3.7v0c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V23z"/>
     		<path  stroke-width="0.25" d="M10.7,5.1h13.2c0.4,0,0.7-0.3,0.7-0.7c0-0.4-0.3-0.7-0.7-0.7H10.7
     			C10.3,3.7,10,4,10,4.4C10,4.8,10.3,5.1,10.7,5.1z"/>
     		<path  stroke-width="0.25" d="M23.9,12H10.7c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h13.2
     			c0.4,0,0.7-0.3,0.7-0.7C24.7,12.4,24.3,12,23.9,12z"/>
     		<path  stroke-width="0.25" d="M23.9,20.4H10.7c-0.4,0-0.7,0.3-0.7,0.7c0,0.4,0.3,0.7,0.7,0.7h13.2
     			c0.4,0,0.7-0.3,0.7-0.7C24.7,20.7,24.3,20.4,23.9,20.4z"/>
     	</symbol>
     	<symbol id="view-cells" viewBox="0 0 27 25">
     		<path stroke-width="0.25" d="M6.2,0.9H2.5c-0.9,0-1.7,0.7-1.7,1.7v3.7c0,0.9,0.7,1.7,1.7,1.7h3.7
     			c0.9,0,1.7-0.7,1.7-1.7V2.5C7.9,1.6,7.1,0.9,6.2,0.9z M6.5,6.2c0,0.1-0.1,0.2-0.2,0.2H2.5c-0.1,0-0.2-0.1-0.2-0.2V2.5
     			c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V6.2z"/>
     		<path stroke-width="0.25" d="M6.2,9.2H2.5c-0.9,0-1.7,0.7-1.7,1.7v3.6h0v0.1c0,0.9,0.7,1.7,1.7,1.7h3.7
     			c0.9,0,1.7-0.7,1.7-1.7v-3.7C7.9,10,7.1,9.2,6.2,9.2z M6.5,14.6c0,0.1-0.1,0.2-0.2,0.2H2.5c-0.1,0-0.2-0.1-0.2-0.2v-3.7v0
     			c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V14.6z"/>
     		<path stroke-width="0.25" d="M6.2,17.6H2.5c-0.9,0-1.7,0.7-1.7,1.7v3.6h0V23c0,0.9,0.7,1.7,1.7,1.7h3.7
     			c0.9,0,1.7-0.7,1.7-1.7v-3.7C7.9,18.4,7.1,17.6,6.2,17.6z M6.5,23c0,0.1-0.1,0.2-0.2,0.2H2.5c-0.1,0-0.2-0.1-0.2-0.2v-3.7v0
     			c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V23z"/>
     		<path stroke-width="0.25" d="M15.3,0.9h-3.7c-0.9,0-1.7,0.7-1.7,1.7v3.7c0,0.9,0.7,1.7,1.7,1.7h3.7
     			c0.9,0,1.7-0.7,1.7-1.7V2.5C17,1.6,16.3,0.9,15.3,0.9z M15.6,6.2c0,0.1-0.1,0.2-0.2,0.2h-3.7c-0.1,0-0.2-0.1-0.2-0.2V2.5
     			c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V6.2z"/>
     		<path stroke-width="0.25" d="M15.3,9.2h-3.7c-0.9,0-1.7,0.7-1.7,1.7v3.6h0v0.1c0,0.9,0.7,1.7,1.7,1.7h3.7
     			c0.9,0,1.7-0.7,1.7-1.7v-3.7C17,10,16.3,9.2,15.3,9.2z M15.6,14.6c0,0.1-0.1,0.2-0.2,0.2h-3.7c-0.1,0-0.2-0.1-0.2-0.2v-3.7v0
     			c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V14.6z"/>
     		<path stroke-width="0.25" d="M15.3,17.6h-3.7c-0.9,0-1.7,0.7-1.7,1.7v3.6h0V23c0,0.9,0.7,1.7,1.7,1.7h3.7
     			c0.9,0,1.7-0.7,1.7-1.7v-3.7C17,18.4,16.3,17.6,15.3,17.6z M15.6,23c0,0.1-0.1,0.2-0.2,0.2h-3.7c-0.1,0-0.2-0.1-0.2-0.2v-3.7v0
     			c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V23z"/>
     		<path stroke-width="0.25" d="M24.5,0.9h-3.7c-0.9,0-1.7,0.7-1.7,1.7v3.7c0,0.9,0.7,1.7,1.7,1.7h3.7
     			c0.9,0,1.7-0.7,1.7-1.7V2.5C26.1,1.6,25.4,0.9,24.5,0.9z M24.7,6.2c0,0.1-0.1,0.2-0.2,0.2h-3.7c-0.1,0-0.2-0.1-0.2-0.2V2.5
     			c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V6.2z"/>
     		<path stroke-width="0.25" d="M24.5,9.2h-3.7c-0.9,0-1.7,0.7-1.7,1.7v3.6h0v0.1c0,0.9,0.7,1.7,1.7,1.7h3.7
     			c0.9,0,1.7-0.7,1.7-1.7v-3.7C26.1,10,25.4,9.2,24.5,9.2z M24.7,14.6c0,0.1-0.1,0.2-0.2,0.2h-3.7c-0.1,0-0.2-0.1-0.2-0.2v-3.7v0
     			c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V14.6z"/>
     		<path stroke-width="0.25" d="M24.5,17.6h-3.7c-0.9,0-1.7,0.7-1.7,1.7v3.6h0V23c0,0.9,0.7,1.7,1.7,1.7h3.7
     			c0.9,0,1.7-0.7,1.7-1.7v-3.7C26.1,18.4,25.4,17.6,24.5,17.6z M24.7,23c0,0.1-0.1,0.2-0.2,0.2h-3.7c-0.1,0-0.2-0.1-0.2-0.2v-3.7v0
     			c0-0.1,0.1-0.2,0.2-0.2h3.7c0.1,0,0.2,0.1,0.2,0.2V23z"/>
     	</symbol>
     	<symbol  id="icon-calendar" viewBox="0 0 21 23">
     		<path d="M20.7,4.8l-0.2,0l0,0L20.7,4.8L20.7,4.8c-0.2-0.7-0.5-1.4-1-1.9c-0.1-0.1-0.3-0.3-0.5-0.4l0,0c0,0-0.1,0-0.1-0.1l0,0l0,0
     			c-0.1,0-0.2-0.1-0.3-0.1l0,0h0C18.2,2,17.7,1.8,17,1.8h-1.2V1.1c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v0.7H6.6V1.1
     			c0-0.4-0.3-0.7-0.7-0.7c-0.4,0-0.7,0.3-0.7,0.7v0.7H4c-0.5,0-1,0.1-1.4,0.3v0l0,0l0.1,0.2l0,0L2.6,2.1C2.1,2.3,1.7,2.6,1.4,2.9
     			C1.2,3.1,1,3.3,0.8,3.5l0,0l0,0C0.7,3.8,0.6,4.1,0.5,4.3h0l0,0l0.2,0.1l0,0L0.4,4.3c0,0.1-0.1,0.3-0.1,0.5l0,0c0,0,0,0,0,0s0,0,0,0
     			l0,0c0,0.2-0.1,0.5-0.1,0.7v2.3v10.8c0,1,0.4,2,1.1,2.6c0.7,0.7,1.6,1.1,2.6,1.1h13.1c1,0,2-0.4,2.6-1.1c0.7-0.7,1.1-1.6,1.1-2.6
     			V7.9V5.5C20.7,5.3,20.7,5,20.7,4.8z M0.6,4.8L0.6,4.8L0.6,4.8L0.6,4.8z M19,2.7L19,2.7L19,2.7L19,2.7z M18.7,2.4L18.7,2.4l0,0.1h0
     			L18.7,2.4z M2.6,3.3L2.6,3.3L2.6,3.3L2.6,3.3z M1.7,5.5c0-0.2,0-0.3,0-0.5c0-0.1,0.1-0.3,0.1-0.4C1.9,4.4,2,4.2,2.1,4.1
     			C2.2,4,2.2,3.9,2.3,3.9c0.1-0.1,0.3-0.2,0.4-0.3c0.4-0.2,0.7-0.3,1.2-0.3h1.2v0.7c0,0.4,0.3,0.7,0.7,0.7c0.4,0,0.7-0.3,0.7-0.7V3.2
     			h8v0.7c0,0.4,0.3,0.7,0.7,0.7c0.4,0,0.7-0.3,0.7-0.7V3.2h1.2c0.5,0,0.9,0.1,1.2,0.3c0.2,0.1,0.3,0.2,0.4,0.3
     			c0.3,0.3,0.5,0.7,0.6,1.2c0,0.2,0,0.3,0,0.5v1.7H1.7V5.5z M19.4,18.6c0,0.7-0.3,1.2-0.7,1.7c-0.4,0.4-1,0.7-1.6,0.7H4
     			c-0.6,0-1.2-0.3-1.6-0.7s-0.7-1-0.7-1.6V8.5h17.8V18.6z"/>
     	</symbol>
     	<symbol id="icon-edit" viewBox="0 0 21 24">
     		<g>
     			<path d="M19.1,4.1L19.1,4.1l-2.7-1.9v0c0,0,0,0,0,0l0,0v0c-0.1-0.1-0.3-0.1-0.4-0.1l0,0c0,0,0,0,0,0c0,0,0,0,0,0v0
     				c-0.2,0-0.3,0.1-0.4,0.2c0,0,0,0,0,0h0L7.7,13.7c0,0,0,0,0,0l0,0l0,0c0,0.1-0.1,0.2-0.1,0.3l-0.5,3.6v0v0c0,0.2,0.1,0.4,0.2,0.6
     				l0,0c0,0,0,0,0,0s0,0,0,0l0,0c0.1,0.1,0.2,0.1,0.3,0.1c0.1,0,0.2,0,0.3-0.1l0,0l3.2-1.7v0c0,0,0,0,0,0l0,0l0,0
     				c0.1,0,0.2-0.1,0.2-0.2l0,0l7.9-11.4l0,0C19.4,4.6,19.3,4.2,19.1,4.1z M7.3,17.6L7.3,17.6L7.3,17.6L7.3,17.6z M10.4,15.5l-2,1.1
     				l0.3-2.3l7.5-10.8l1.7,1.2L10.4,15.5z"/>
     			<path d="M18.5,9c-0.3,0-0.6,0.3-0.6,0.6v11.5c0,0.6-0.5,1.1-1.1,1.1H2.3c-0.6,0-1.1-0.5-1.1-1.1V6.6c0-0.6,0.5-1.1,1.1-1.1h8.5
     				c0.3,0,0.6-0.3,0.6-0.6c0-0.3-0.3-0.6-0.6-0.6H2.3c-1.3,0-2.3,1-2.3,2.3v14.5c0,1.3,1,2.3,2.3,2.3h14.5c1.3,0,2.3-1,2.3-2.3V9.6
     				C19.1,9.3,18.8,9,18.5,9z"/>
     			<path d="M20,2.8L17.3,1l0,0c-0.3-0.2-0.6-0.1-0.8,0.2l0,0c0,0,0,0,0,0c0,0,0,0,0,0l0,0c-0.2,0.3-0.1,0.6,0.2,0.8l2.8,1.8l0,0
     				l0.1-0.2l0,0l-0.1,0.2c0.1,0.1,0.2,0.1,0.3,0.1c0.2,0,0.4-0.1,0.5-0.3h0c0,0,0,0,0,0C20.4,3.4,20.3,3,20,2.8z"/>
     		</g>
     	</symbol>
     	<symbol id="icon-ok-empty" viewBox="0 0 75 75">
     		<g>
     			<path d="M49.5,25.8L28.6,45.3l-6-6c-0.9-0.9-2.3-0.9-3.2,0s-0.9,2.3,0,3.2l7.5,7.5c0.9,0.9,2.2,0.9,3.1,0.1l22.5-21
     				c0.9-0.8,1-2.3,0.1-3.2S50.4,25,49.5,25.8z"/>
     			<path d="M37.5,2c-19.9,0-36,16.1-36,36c0,19.9,16.1,36,36,36c19.9,0,36-16.1,36-36S57.4,2,37.5,2z M37.5,71c-18.2,0-33-14.8-33-33
     				c0-18.2,14.8-33,33-33c18.2,0,33,14.8,33,33S55.7,71,37.5,71z"/>
     		</g>
     	</symbol>
     	<symbol id="icon-trash" viewBox="0 0 24 25">
     		<g>
     			<path d="M7.8,7.4c-0.3,0-0.5,0.2-0.5,0.5v12c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5v-12C8.3,7.7,8.1,7.4,7.8,7.4z"/>
     			<path d="M11.8,7.4c-0.3,0-0.5,0.2-0.5,0.5v12c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5v-12C12.3,7.7,12.1,7.4,11.8,7.4z"/>
     			<path d="M15.3,7.9v12c0,0.3,0.2,0.5,0.5,0.5s0.5-0.2,0.5-0.5v-12c0-0.3-0.2-0.5-0.5-0.5S15.3,7.7,15.3,7.9z"/>
     			<path d="M23.3,3.9h-3.5h-3.5V1.4c0-0.3-0.2-0.5-0.5-0.5h-8c-0.3,0-0.5,0.2-0.5,0.5v2.5H3.8H1.3C1,3.9,0.8,4.2,0.8,4.4
     				c0,0.3,0.2,0.5,0.5,0.5h2v19.5c0,0.3,0.2,0.5,0.5,0.5h16c0.3,0,0.5-0.2,0.5-0.5V4.9h3c0.3,0,0.5-0.2,0.5-0.5
     				C23.8,4.2,23.6,3.9,23.3,3.9z M19.3,23.9h-15v-19h3.5h8h3.5V23.9z M8.3,1.9h7v2h-7V1.9z"/>
     		</g>
     	</symbol>
     	<symbol id="letter" viewBox="0 0 22 15">
     		<path fill-rule="evenodd" clip-rule="evenodd" d="M20 11.9048C20 12.6933 19.36 13.3333 18.5714 13.3333H1.42857C0.64 13.3333 0 12.6933 0 11.9048V1.42857C0 0.639048 0.64 0 1.42857 0H18.5714C19.36 0 20 0.639048 20 1.42857V11.9048Z" transform="translate(1 1)"  stroke-linecap="round" stroke-linejoin="round"/>
     		<path d="M19.0476 0L9.52381 7.61905L0 0" transform="translate(1.47656 1.4762)" stroke-linecap="round" stroke-linejoin="round"/>
     	</symbol>
     	<svg id="key" viewBox="0 0 24 24" fill="none" >
     		<path fill-rule="evenodd" clip-rule="evenodd" d="M17.3913 0L8.88174 8.51043C8.04435 8.07565 7.09478 7.82609 6.08696 7.82609C2.72522 7.82609 0 10.5513 0 13.913C0 17.2757 2.72522 20 6.08696 20C9.44783 20 12.1739 17.2757 12.1739 13.913C12.1739 12.9043 11.9243 11.9557 11.4904 11.1191L14.3478 8.26087H15.6522V6.95652L16.087 6.52174H17.3913V5.21739L17.8261 4.78261H19.1304V3.47826L20 2.6087V0H17.3913V0Z" transform="translate(4.19922 0.390625) scale(-1 1) rotate(80.8009)" stroke-linecap="round" stroke-linejoin="round"/>
     		<path fill-rule="evenodd" clip-rule="evenodd" d="M2.6087 1.30435C2.6087 2.02609 2.02435 2.6087 1.30435 2.6087C0.584348 2.6087 0 2.02609 0 1.30435C0 0.584348 0.584348 0 1.30435 0C2.02435 0 2.6087 0.584348 2.6087 1.30435V1.30435Z" transform="translate(17.875 5.68866) scale(-1 1) rotate(80.8009)" stroke-linecap="round" stroke-linejoin="round"/>
     	</svg>
     </svg>

	<?
	use Bitrix\Main\Page\Asset;
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/jquery/jquery-3.3.1.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/bootstrap4/bootstrap.bundle.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/slick/slick.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/scrollbar/jquery.mCustomScrollbar.concat.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/ion-rangeSlider/ion.rangeSlider.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/rateyo/jquery.rateyo.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/select2/select2.full.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/flatpickr/flatpickr.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/flatpickr/ru.js");
	Asset::getInstance()->addString('<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=es6,Array.prototype.includes,CustomEvent,Object.entries,Object.values,URL" crossorigin="anonymous"></script>'); //<!--грузит полифилы для plyr потому что там es6, если не нужен ie11 и старые браузеры то можно убрать -->
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/plyr/plyr.min.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/vendor/file-input/bootstrap.file-input.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/app.js");
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH."/assets/js/additional.js");	
	?>
	
  </body>
</html>