$(function(){
    form_personal();
	form_legal();
});

/*
 * *****************************************************************************
 * Формы
 * *****************************************************************************
 */
var scrol_to = $('.acc-profile__tab-buttons'); 

function form_personal() {
    var sendRequest = $('#acc-profile__tab_1');
	var sendRequest_form = sendRequest.find("form");
    sendRequest.find("form").submit(function () {
		$('html, body').animate({
			scrollTop: scrol_to.offset().top
		}, 500);		
        sendRequest.find('.result').html('');
        sendRequest.find('.result').removeClass('red');
        sendRequest.find('.result').removeClass('green');
		sendRequest.find('.result').hide().fadeIn(300).html('<div style="color:#ffe23f;">Загрузка...</div>');
		sendRequest_form.css({"opacity" : "0.5"});

        return false;
    });
    if (sendRequest.length) {
        sendRequest.bsForm({
            successAnimate: function (self) {
                sendRequest.find('.result').addClass('green');
                sendRequest.find('.result').html('<p>Данные изменены!</p>');
                sendRequest.find('input[name="DATA[OLD_PASS]"]').val('');
                sendRequest.find('input[name="DATA[PASS]"]').val('');
				sendRequest.find('.result').hide().fadeIn(100).delay(2000).fadeOut(300);
            },
			onShowOk: function (self, jsonData) {
 				var uploaded_image = '<div class="acc-profile__avatar">' +
										'<img src="' + jsonData.data.PERSONAL_PHOTO + '" alt="" class="acc-profile__avatar-img">' +
                                                        '<button type="button" class="acc-profile__delete-photo-button">' +
                                                            '<span aria-hidden="true">&times;</span>' +
                                                        '</button>' +
                                      '</div>'; 
													//console.log(jsonData);					
				
				if(jsonData.data.PERSONAL_PHOTO) sendRequest.find('.acc-profile__col-with-photo').html( uploaded_image );
				sendRequest_form.css({"opacity" : "1"});
				
			},			
            errorType: 'custom',
            onShowError: function (self, jsonData) {
				console.log(jsonData);
                var error = jsonData.error;
                self.form.find('.error').removeClass('error');
                sendRequest.find('.result').addClass('red');
				sendRequest_form.css({"opacity" : "1"});
									
				sendRequest.find('.result').html('');
                for (var i in error) {
                    switch (error[i].type) {
                        case 'DATA':
                            var control = self.form.find('[name*="DATA[' + error[i].name + ']"]');
                            control.closest('div').addClass('error');
                            control.data('tmpval', control.val());
                            sendRequest.find('.result').append('<p>' + error[i].text + '</p>');
                            break;
                    }
                    control.unbind('focus');
                    control.bind("focus", function () {
                        var control = $(this);
                        control.parents('div').removeClass('error');
                    });

                }
            },
            onSend: function (self, e) {
                var controls = self.form.find('.error [name*="DATA"]');
                controls.each(function (index) {
                    var control = $(this);
                    control.parents('div').removeClass('error');
                });
            }
        });
    }
}

function form_legal() {
    var sendRequest = $('#acc-profile__tab_2');
	var sendRequest_form = sendRequest.find("form");
    sendRequest.find("form").submit(function () {
		$('html, body').animate({
			scrollTop: scrol_to.offset().top
		}, 500);		
        sendRequest.find('.result').html('');
        sendRequest.find('.result').removeClass('red');
        sendRequest.find('.result').removeClass('green');
		sendRequest.find('.result').hide().fadeIn(300).html('<div style="color:#ffe23f;">Загрузка...</div>');
		sendRequest_form.css({"opacity" : "0.5"});		

        return false;
    });
    if (sendRequest.length) {
        sendRequest.bsForm({
            successAnimate: function (self) {
                sendRequest.find('.result').addClass('green');
                sendRequest.find('.result').html('<p>Данные изменены!</p>');
                sendRequest.find('input[name="DATA[OLD_PASS]"]').val('');
                sendRequest.find('input[name="DATA[PASS]"]').val('');
				sendRequest.find('.result').hide().fadeIn(100).delay(2000).fadeOut(300);
            },
			onShowOk: function (self, jsonData) {sendRequest_form.css({"opacity" : "1"});},
            errorType: 'custom',
            onShowError: function (self, jsonData) {
				console.log(jsonData);
                var error = jsonData.error;
                self.form.find('.error').removeClass('error');
                sendRequest.find('.result').addClass('red');
				sendRequest_form.css({"opacity" : "1"});

				sendRequest.find('.result').html('');
                for (var i in error) {
                    switch (error[i].type) {
                        case 'DATA':
                            var control = self.form.find('[name*="DATA[' + error[i].name + ']"]');
                            control.closest('div').addClass('error');
                            control.data('tmpval', control.val());
                            sendRequest.find('.result').append('<p>' + error[i].text + '</p>');
                            break;
                    }
                    control.unbind('focus');
                    control.bind("focus", function () {
                        var control = $(this);
                        control.parents('div').removeClass('error');
                    });

                }
            },
            onSend: function (self, e) {
                var controls = self.form.find('.error [name*="DATA"]');
                controls.each(function (index) {
                    var control = $(this);
                    control.parents('div').removeClass('error');
                });
            }
        });
    }
}

$(function() {
	$('body').on('click','.acc-profile__delete-photo-button', function(e){
			e.preventDefault();
			
			var form = $(this).closest('form');
			var formAction = form.attr("action");
			
			$.ajax({
				type: "POST",
				url: formAction,
				data: { del: 'Y' },
				success: function (arResult) {
					console.log(arResult);
					if(arResult.status == true){
						$('.acc-profile__col-with-photo').html(arResult.data.empty_photo_html);
					} else {
						alert('Произашла ошибка, фотография не удалена');
					}
				},
				dataType: 'json'
			}); 
	});


	$("body").on("change", ".acc-profile__empty-avatar input:file", function (){
		var fileName = $(this).val().split('\\').pop();
		if(fileName.length > 0){
			$(".acc-profile__col-with-photo .custom-file-label").html(fileName);
		} else {
			$(".acc-profile__col-with-photo .custom-file-label").html('Загрузить фото');
			$(this).val('');
		}
	});
});