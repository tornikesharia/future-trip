<? if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();

if(!CModule::IncludeModule("iblock"))
	return;
        
$arComponentParameters = array(
        "GROUPS" => array(
	),
        'PARAMETERS' => array(
                'ELEMENT_ID' => array(
                    "NAME" => GetMessage("ELEMENT_ID"),
                    "TYPE" => "STRING",
                    "DEFAULT" => "",            
                    "PARENT" => "BASE"
                    ),

		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
        ),
);
?>