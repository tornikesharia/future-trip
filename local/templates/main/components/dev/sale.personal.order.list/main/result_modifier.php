<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


foreach($arResult['ORDERS'] as $key => $arOrder){
	foreach($arOrder['BASKET_ITEMS'] as $bKey => $arBasketItem){
		$VALUES = array();
		$res = CIBlockElement::GetProperty(2, $arBasketItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "ARTNUMBER", "CODE" => "COLOR"));
		while ($ob = $res->GetNext())
		{
			$VALUES[$ob['CODE']] = $ob;
		}
		
		$arResult['ORDERS'][$key]['BASKET_ITEMS'][$bKey]['PROPERTIES'] = $VALUES;
	}
}