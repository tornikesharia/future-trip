<?

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

//Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/bootstrap_v4/script.js");
//Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/bootstrap_v4/style.css");
//CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

//Lib::Debug($arResult);

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach($arResult['ERRORS']['FATAL'] as $code => $error)
	{
		if ($code !== $component::E_NOT_AUTHORIZED)
			ShowError($error);
	}
	$component = $this->__component;
	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		?>
		<div class="row">
			<div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3">
				<div class="alert alert-danger"><?=$arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]?></div>
			</div>
			<? $authListGetParams = array(); ?>
			<div class="col-md-8 offset-md-2 col-lg-6 offset-lg-3" id="catalog-subscriber-auth-form" style="<?=$authStyle?>">
				<?$APPLICATION->AuthForm('', false, false, 'N', false);?>
			</div>
		</div>
		<?
	}

}
else
{
	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	if (!count($arResult['ORDERS']))
	{

			?>
			<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST')?></h3>
			<?

	}
	?>
	<?if(count($arResult['ORDERS']) > 0):?>
							<div class="acc1">
								<div class="acc1__orders">
									<div class="acc1__orders-header">
										<div class="acc1__header-title acc1__title">Наименование</div>
										<div class="acc1__header-sum acc1__sum">Сумма</div>
										<div class="acc1__header-bonus acc1__bonus">Оплата бонусами</div>
										<div class="acc1__header-qty acc1__qty">Кол-во</div>
										<div class="acc1__header-status acc1__status">Cтатус</div>
									</div>

		<?foreach($arResult['ORDERS'] as $key => $arOrder):?>
									<div class="acc1-item">
										<div class="acc1-item__info-block acc1__title">
											<div class="acc1-item__date">
												<?=$arOrder['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT'])?>
											</div>
											<div class="acc1-item__number"><?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$arOrder['ORDER']['ACCOUNT_NUMBER']?></div>
											<?if(count($arOrder['BASKET_ITEMS']) > 1):?>
											<div  class="acc1-item__toggler">
												<div class="acc1-item__toggler-text">Комплект из <?=count($arOrder['BASKET_ITEMS']);?> товаров</div>
												<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/angle-bottom-yellow.svg" alt="" class="acc1-item__toggler-icon">
											</div>
											<?else:?>
												
												<?foreach($arOrder['BASKET_ITEMS'] as $basketItem):?>	
													<a href="#" class="acc1-item__title"><?=$basketItem['NAME']?></a>
													<?if($basketItem['PROPERTIES']['ARTNUMBER']['VALUE']):?>
													<div class="acc1-item__attr"><?=$basketItem['PROPERTIES']['ARTNUMBER']['NAME']?>: <span><?=$basketItem['PROPERTIES']['ARTNUMBER']['VALUE']?></span></div>
													<?endif;?>
													<?if($basketItem['PROPERTIES']['COLOR']['VALUE']):?>
													<div class="acc1-item__attr"><?=$basketItem['PROPERTIES']['COLOR']['NAME']?>: <span><?=$basketItem['PROPERTIES']['COLOR']['VALUE']?></span></div>
													<?endif;?>
												<?endforeach;?>	
											<?endif;?>
										</div>
										<div class="acc1-item__sum-block acc1__sum">
											<div class="acc1-item__price"><?=$arOrder['ORDER']['FORMATED_PRICE'];?></div>
											<?/*<div class="acc1-item__price-bonus">+ 10000 бонусов</div>*/?>
										</div>
										<div class="acc1-item__bonus-block acc1__bonus">
											<?/*<div class="acc1-item__bonus">- 10000 бонусов</div>*/?>
										</div>
										<div class="acc1-item__qty-block acc1__qty">
											<div class="input-count input-count_disabled acc1-item__count-block ">
												<div class="input-group">
													<input type="text"
														   class="form-control input-count__input"
														   value="<?=count($arOrder['BASKET_ITEMS']);?>" disabled>
												</div>
											</div>
										</div>
										<div class="acc1-item__status-block acc1__status">
											<div class="acc1-item__status">
												<?if ($arOrder['ORDER']['CANCELED'] == "Y"):?>
													<div class="acc1-item__status_red">Отменен</div>
												<?else:?>
													<?if($arOrder['ORDER']['STATUS_ID'] == 'F'):?>
														<div class="acc1-item__status_green"><?=htmlspecialcharsbx($arResult['INFO']['STATUS'][$arOrder['ORDER']['STATUS_ID']]['NAME'])?></div>
													<?else:?>
														<?=htmlspecialcharsbx($arResult['INFO']['STATUS'][$arOrder['ORDER']['STATUS_ID']]['NAME'])?>
													<?endif;?>
												<?endif;?>
											</div>
										</div>
				
				<?if(count($arOrder['BASKET_ITEMS']) > 0):?>				
										<div class="acc1__sub">
					<?foreach($arOrder['BASKET_ITEMS'] as $bKey => $arBasketItem):?>
											<div class="acc1-item">
												<div class="acc1-item__info-block acc1__title">
													<a href="#" class="acc1-item__title"><?=$arBasketItem['NAME']?></a>
													<?if($arBasketItem['PROPERTIES']['ARTNUMBER']['VALUE']):?>
													<div class="acc1-item__attr"><?=$arBasketItem['PROPERTIES']['ARTNUMBER']['NAME']?>: <span><?=$arBasketItem['PROPERTIES']['ARTNUMBER']['VALUE']?></span></div>
													<?endif;?>
													<?if($arBasketItem['PROPERTIES']['COLOR']['VALUE']):?>
													<div class="acc1-item__attr"><?=$arBasketItem['PROPERTIES']['COLOR']['NAME']?>: <span><?=$arBasketItem['PROPERTIES']['COLOR']['VALUE']?></span></div>
													<?endif;?>
												</div>
												<div class="acc1-item__sum-block acc1__sum">
													<div class="acc1-item__price"><?=CurrencyFormat($arBasketItem['PRICE'], $arBasketItem['CURRENCY']);?></div>
													<?/*<div class="acc1-item__price-bonus">+ 10000 бонусов</div>*/?>
												</div>
												<div class="acc1-item__bonus-block acc1__bonus">
													<?/*<div class="acc1-item__bonus">- 5000 бонусов</div>*/?>
												</div>
												<div class="acc1-item__qty-block acc1__qty">
													<div class="input-count input-count_disabled acc1-item__count-block ">
														<div class="input-group">
															<input type="text"
																   class="form-control input-count__input"
																   value="<?=$arBasketItem['QUANTITY']?>" disabled>
														</div>
													</div>
												</div>
												<div class="acc1-item__status-block acc1__status"></div>
											</div>
					<?endforeach;?>											

										</div>
				<?endif;?>
									</div>
		<?endforeach;?>
			
				
								</div>
				
								<?=$arResult["NAV_STRING"];?>
							</div><!--/.acc1 -->		
			
	<?endif;?>
	<?
}
?>
