<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION, $USER;

$arResult = array(
	"CUR_URI" => "/",
	"LOGOUT" => ""
);

if($_POST['param']=="/auth/?logout=yes"){
	$USER->Logout();
	$arResult["LOGOUT"]="yes";
	echo json_encode($arResult);	
}



require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>