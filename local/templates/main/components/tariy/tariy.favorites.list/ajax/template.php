<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["ITEMS"]) > 0):?>
            <section class="favorites">

                    <div class="home-products__list">
                        <div class="row no-gutters">
<?foreach( $arResult["ITEMS"] as $key => $arItem ):?>
                            <div class="favorites-item col-sm-6 col-xl-3 mb-3">
                                <div class="vcard">
                                    <div class="vcard__top">
                                        <div class="vcard__img-block">
											<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="vcard__title">
												<img src="<?=$arItem['PREVIEW_PICTURE']['src']?>" alt="<?=$arItem['PREVIEW_PICTURE']['alt']?>" class="vcard__img" />
											</a>	
                                        </div>
                                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="vcard__title"><?=$arItem['NAME']?></a>
                                        <div class="vcard__buttons">
                                            <div class="button_tr vcard__price">
												<?if($arItem['PRICE']['PRICE'] > 0):?>
													<?=$arItem['PRICE']['DISCOUNT_PRICE_FORMATED']?>
												<?else:?>
													по запросу
												<?endif;?>
												<?if($arItem['PRICE']['PRICE'] > $arItem['PRICE']['DISCOUNT_PRICE']):?>
													<div class="vcard__old-price"><?=$arItem['PRICE']['PRICE_FORMATED']?></div>
												<?endif;?>										
											</div>
											<a href="javascript:void(0);" class="button_yellow vcard__button Add2BasketTriger" data-key="<?=md5("ADD_BASKET")?>" data-id="<?=$arItem['ID'];?>" data-quantity="1">В корзину</a>
											
                                        </div>
                                    </div>
                                    <div class="vcard__bottom">
                                        <div class="vcard__attr">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/star-yellow.svg" alt="" class="vcard__attr-icon">
											<?$APPLICATION->IncludeComponent(
													"tariy:tariy.favorites.button",
													"",
													Array(
															"CACHE_TIME" => "1",
															"CACHE_TYPE" => "A",
															"ELEMENT_ID" => "{$arItem["ID"]}"
													)
											);?>											
                                        </div>
										<?if($arItem['PROPERTY']['MAXIMUM_SPEED']['VALUE']):?>
                                        <div class="vcard__attr">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/speedometer-yellow.svg" alt="" class="vcard__attr-icon">
                                            <div class="vcard__attr-text">Макс.скорость - <?=$arItem['PROPERTY']['MAXIMUM_SPEED']['VALUE']?></div>
                                        </div>
										<?endif;?>
										<?if($arItem['PROPERTY']['BATTERY_CAPACITY']['VALUE']):?>
                                        <div class="vcard__attr">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/car-battery-yellow.svg" alt="" class="vcard__attr-icon">
                                            <div class="vcard__attr-text"><?=$arItem['PROPERTY']['BATTERY_CAPACITY']['VALUE']?></div>
                                        </div>
										<?endif;?>
										<?if($arItem['PROPERTY']['POWER_RESERVE']['VALUE']):?>
                                        <div class="vcard__attr">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/cog-yellow.svg" alt="" class="vcard__attr-icon">
                                            <div class="vcard__attr-text"><?=$arItem['PROPERTY']['POWER_RESERVE']['VALUE']?></div>
                                        </div>
										<?endif;?>
										<?if($arItem['PROPERTY']['FRAME_MATERIAL']['VALUE']):?>
                                        <div class="vcard__attr">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bicycle-yellow.svg" alt="" class="vcard__attr-icon">
                                            <div class="vcard__attr-text"><?=$arItem['PROPERTY']['FRAME_MATERIAL']['VALUE']?></div>
                                        </div>
										<?endif;?>
                                    </div>
                                </div>
                            </div>
<?endforeach;?>

                        </div><!--/.row -->
                    </div><!--/.home-products__list -->


            </section><!--/.home-products -->	
<?else:?>
	<p>У вас пока еще нет избранных товаров</p>
<?endif;?>
