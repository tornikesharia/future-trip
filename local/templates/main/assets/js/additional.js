$(function(){
	
	MyAdd2Basket();
	
	// Выбирает цвет в списке и карточке товара и передает кнопке для добавления в карзину
	$('body').on('change', 'input[name="COLOR"]', function(){
		var chosen_color = $(this).val();
		$(this).closest("[data-product-info]").find('.Add2BasketTriger').attr('data-color', chosen_color);
	});
	// Выбирает размер в списке и карточке товара и передает кнопке для добавления в карзину
	$('body').on('change', 'input[name="DIMENSIONS"]', function(){
		var chosen_dimensions = $(this).val();
		$(this).closest("[data-product-info]").find('.Add2BasketTriger').attr('data-demensions', chosen_dimensions);
	});		
	
	/* add to basket */
	function MyAdd2Basket() {
		$('body').on('click','.Add2BasketTriger', function () { 
			var $this = $(this),
				$info_modal = $('#info-modal'),
				$href = "/local/ajax/ajax.basket.php",
				$key = $(this).attr("data-key"),
				$id = $(this).attr("data-id"),
				$color = $(this).attr("data-color"),
				$demensions = $(this).attr("data-demensions"),
				$quantity = $(this).attr("data-quantity"),
				$color_input = $this.closest("[data-product-info]").find('input[name="COLOR"]'),
				$dimensions_input = $this.closest("[data-product-info]").find('input[name="DIMENSIONS"]'),
				$addToBasketStatus = true;	

				if($color_input.is(':checked')){
					if($dimensions_input.length){
						if($dimensions_input.is(':checked')){
							
						} else {
							$info_modal.find('.simple-modal__title').html('Внимание');
							$info_modal.find('.simple-modal__body').html('Вы не выбрали размер');
							$info_modal.modal('show');
							
							$addToBasketStatus = false;
						}
					}
				} else {
						$info_modal.find('.simple-modal__title').html('Внимание');
						$info_modal.find('.simple-modal__body').html('Вы не выбрали цвет');
						$info_modal.modal('show');
						
						$addToBasketStatus = false;
				}
				


			if($addToBasketStatus){

				BX.ajax({
					url: $href,
					data: {
						'KEY': $key,
						'ID': $id,
						'QUANTITY': $quantity,
						'COLOR': $color,
						'DIMENSIONS': $demensions
					},
					method: 'POST',
					dataType: 'json',
					cache: false,
					onsuccess: function (data) {
						//console.log(data);
						
							if (data.status) {
								
								//$this.html("Добавлено");
								$('#cart-modal').find('.cart-modal__item-title').text(data.ITEM.NAME);
								$('#cart-modal').find('.cart-modal__item-title').attr("href", data.ITEM.DETAIL_PAGE_URL);
								$('#cart-modal').find('.cart-modal__img').attr('src', data.ITEM.PREVIEW_PICTURE.src);
								if(data.quantity > 1){
									$('#cart-modal').find('.cart-modal__sum').html('Всего в корзине '+ data.quantity +' товаров на сумму '+ data.price);
								} else {
									$('#cart-modal').find('.cart-modal__sum').html('Всего в корзине '+ data.quantity +' товар на сумму '+ data.price);
								}
								if(data.ITEM.PROPERTY_ARTNUMBER_VALUE){
									$('#cart-modal').find('.cart-modal__item-props').append('<div class="cart-modal__item-attr">Артикул: <span>'+data.ITEM.PROPERTY_ARTNUMBER_VALUE+'</span></div>');
								}
								if(data.REQUEST.COLOR && data.REQUEST.COLOR != "undefined"){
									$('#cart-modal').find('.cart-modal__item-props').append('<div class="cart-modal__item-attr">Цвет: <span>'+data.REQUEST.COLOR+'</span></div>');
								}
								if(data.REQUEST.DIMENSIONS && data.REQUEST.DIMENSIONS != "undefined"){
									$('#cart-modal').find('.cart-modal__item-props').append('<div class="cart-modal__item-attr">Размеры: <span>'+data.REQUEST.DIMENSIONS+'</span></div>');
								}							
	/* 							if(data.ITEM.PROPERTY_COLOR_VALUE){
									$('#cart-modal').find('.cart-modal__item-props').append('<div class="cart-modal__item-attr">Цвет: <span>'+data.ITEM.PROPERTY_COLOR_VALUE+'</span></div>');
								}	 */						
								
								$('#cart-modal').modal('show');
								
								var $idBasket = $('.bx-basket.bx-opener').attr("id");
								
								if (data.MINI_BASKET) {
									var $basket = data.MINI_BASKET;
									$('#' + $idBasket).find('.cart-dropdown__items .mCSB_container').html("");
									if ($basket.length > 0) {
										if (!$('#' + $idBasket).find('#' + $idBasket + 'products').hasClass("open")) {
											$('#' + $idBasket).find('#' + $idBasket + 'products').addClass("open");
										}
									} else {
										$('#' + $idBasket).find('#' + $idBasket + 'products').removeClass("open");
									}
									for ($i in $basket) {
										$('#' + $idBasket).find('.cart-dropdown__items .mCSB_container').append('' +
											'<div class="cart-dropdown__item">' +
											'<div class="cart-dropdown__item-img-block"><a href="' + $basket[$i].ELEMENT.DETAIL_PAGE_URL + '"><img src="' + $basket[$i].ELEMENT.PREVIEW_PICTURE.src + '" alt="' + $basket[$i].NAME + '" class="cart-dropdown__item-img" /></a></div>' +
											'<div class="cart-dropdown__item-desc-block">' +
											'<a href="'+ $basket[$i].ELEMENT.DETAIL_PAGE_URL +'" class="cart-dropdown__item-title">' + $basket[$i].NAME + '</a>' +
											'</div><!--cart-dropdown__item-desc-block-->' +
											'</div>' +
											'');
									}
									$('#' + $idBasket).find('.cart-dropdown__total-price').text(data.price);
									$('#' + $idBasket).find('.header-middle__cart-count, .cart-dropdown__total-number').text(data.quantity);
									if(data.quantity) {
										$('#' + $idBasket).find('.button_yellow.cart-dropdown__button').show();
									}
								}
								

							
							}
					}
				});
			}
			return false;
		});
	}	
	
	$('#cart-modal').on('hidden.bs.modal', function () {
	  $('#cart-modal').find('.cart-modal__item-props').html('');
	});
	
	$('.reviews-note-box.reviews-note-note').fadeIn('fast').delay(5000).fadeOut('slow');
	
	// changes login form to register form
	$('.login-modal__not-registered').click(function(e){
		e.preventDefault();
		
		$('#login-modal').modal('hide');
		setTimeout(function () {
			$('#reg-modal').modal('show');
		}, 500); 				
	});
	
	// login form submit
	$('.login-modal__form').on('submit', function (e) {
		e.preventDefault();

		var formData = $(this).serialize();
		var formAction = $(this).attr("action");
		
		$.ajax({
			type: "POST",
			url: formAction,
			data: formData,
			success: function (arResult) {
				//console.log(arResult);
				if (arResult.TYPE == "ERROR") { 
					$(".modal__error-text").fadeIn(300).delay(2000).fadeOut(300).html(arResult.MESSAGE);
				} else if (arResult.TYPE == "OK") {
					$('#login-modal').modal('hide');
					$("#info-modal .simple-modal__title").fadeIn(300).html(arResult.MESSAGE);
					$("#info-modal .simple-modal__body").html('');
					setTimeout(function () {
						$('#info-modal').modal('show');
					}, 500); 					
					setTimeout(function () {
						window.location.reload(true);
						document.location.href = arResult.CUR_URI;
					}, 1000); 
				} else {
					$('#login-modal').modal('hide');
					$("#info-modal .simple-modal__title").fadeIn(300).html("Произошла ошибка. Попробуйте еще раз.");
					setTimeout(function () {
						$('#info-modal').modal('show');
					}, 500); 					
				}
			},
			dataType: 'json'
		});
	});
	
	// register form submit
	$('#reg-modal__form_1, #reg-modal__form_2').on('submit', function (e) {
		e.preventDefault();

		var formData = $(this).serialize();
		var formAction = $(this).attr("action");
		var textErrorBlock = $(this).find('.modal__error-text');

		$.ajax({
			type: "POST",
			url: formAction,
			data: formData,
			cache: false,
			success: function (arResult) { 
				//console.log(arResult); 
				if (arResult.TYPE == "ERROR") {								
					textErrorBlock.hide().fadeIn(300).delay(2000).fadeOut(300).html(arResult.MESSAGE);
				} else {
					$('#reg-modal').modal('hide');
					$("#info-modal .simple-modal__title").fadeIn(300).html("Вы успешно зарегистрировались и авторизовались!");
					$("#info-modal .simple-modal__body").html('');
					setTimeout(function () {
						$('#info-modal').modal('show');
					}, 500); 							
												
					setTimeout(function () {
						window.location.reload(true);
						document.location.href = arResult.CUR_URI;
					}, 1000); 
				}
			},
			dataType: 'json'
		});
	});		

	// logout triger	
	$('.logout').click(function (e) {
		e.preventDefault();
		
		var href_str = $(this).attr('href'); 
		$.ajax({
			type: "POST",
			url: "/local/ajax/logout.php",
			data: {param: href_str},
			success: function (arResult) {
				if (arResult.LOGOUT == "yes") {
					$("#info-modal .simple-modal__title").fadeIn(300).html("Вы успешно вышли из системы!");
					$("#info-modal .simple-modal__body").html('');
					$('#info-modal').modal('show');						
					setTimeout(function () {
						document.location.href = arResult.CUR_URI;
					}, 1000);
				}
			},
			dataType: 'json'
		});
	});
	
    //переключение значения инпута в компоненте с кнопками + и -
    $('.input-count__button').click(function(e){
        e.preventDefault();

        var inputCountBlock = $(this).closest('.input-count');
        var input = inputCountBlock.find('.input-count__input');
        var currentVal = parseInt(input.val());
		$('.product-info__add-to-cart').find('.Add2BasketTriger').attr('data-quantity', currentVal);

    });
	
	$('.call-me-form').submit(function(e){
		e.preventDefault();

		var datastring = $(this).serialize(),
			action = $(this).attr('action'),
			info_modal = $('#info-modal'),
			phone_modal = $("#phone-modal");

		$.ajax({
			url: action, 
			method: "POST",
			data: datastring,
			success: function(result){
				if(result.RESULT=="OK"){
					phone_modal.modal('hide');
					$('.call-me-form').find('input[type="text"], textarea').val('');
					info_modal.find(".simple-modal__title").html('');
					info_modal.find(".simple-modal__body").fadeIn(300).html(result.RESULT_DETAIL);
					setTimeout(function () {
						info_modal.modal('show');
					}, 500);
					setTimeout(function() {
						info_modal.modal('hide');
					}, 4000);
				} else {
					$(".call-me-form__msg").promise().done(function(){
						$(this).hide().fadeIn(300).html(result.RESULT_DETAIL).delay(3000).fadeOut(300);
					});
				}
			}
		});
	});
	
	$('.feedback__form').submit(function(e){
		e.preventDefault(); console.log("form");

		var this_form = $(this);
			datastring = $(this).serialize(),
			action = $(this).attr('action'),
			info_modal = $('#info-modal');

		$.ajax({
			url: action, 
			method: "POST",
			data: datastring,
			success: function(result){
				if(result.RESULT=="OK"){
					this_form.find('input[type="text"], textarea').val('');
					info_modal.find(".simple-modal__title").html('');
					info_modal.find(".simple-modal__body").fadeIn(300).html(result.RESULT_DETAIL);
					setTimeout(function () {
						info_modal.modal('show');
					}, 500);
					setTimeout(function() {
						info_modal.modal('hide');
					}, 4000);
				} else {
					$(".feedback__form-msg").promise().done(function(){
						$(this).hide().fadeIn(300).html(result.RESULT_DETAIL).delay(3000).fadeOut(300);
					});
				}
			}
		});
	});	
	
	$('.catalog__order-change-form select.catalog__select').change(function(){
		$(this).closest('form').submit();
	});
	
});