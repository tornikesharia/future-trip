<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?php foreach( $arResult["ITEMS"] as $key => $arItem ): ?>
<?
if($arItem['PROPERTY']['VIDEO_URL']['EMBED_ID']){
	$typeClass = 'event-card__video';
	$videoAttr = ' data-type="youtube" data-video-id="'.$arItem['PROPERTY']['VIDEO_URL']['EMBED_ID'].'"';
} else {
	$typeClass = 'event-card__date';
}
?>
	<div class="col-lg-6 event-card__outer">
		<div class="event-card<?=($arItem['PROPERTY']['VIDEO_URL']['EMBED_ID']?' event-card_video':'');?>">
			<div class="event-card__top">
				<div class="<?=$typeClass?>"<?=$videoAttr?>>
					<div class="event-card__date-day"><?=$arItem['DATE_CUSTOM']['DAY']?></div>
					<div class="event-card__date-month"><?=$arItem['DATE_CUSTOM']['MONTH']?></div>
				</div>
			</div>
			<div class="event-card__content">
				<div class="event-card__content-main">
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="event-card__title"><?=$arItem["NAME"]?></a>
					<div class="event-card__text">
					  <?=TruncateText(strip_tags(trim(str_replace("&nbsp;", "", $arItem['PREVIEW_TEXT']))), 200)?>
					</div>
					<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="eg-arrow-link event-card__more-link">
						<div class="eg-arrow-link__text">Подробнее</div>
						<svg class="eg-arrow-link__icon"><use xlink:href="#arrow-right"></use></svg>
					</a>
				</div>
				<div class="event-card__content-additional">
					<?$APPLICATION->IncludeComponent(
							"tariy:tariy.favorites.button",
							"",
							Array(
									"CACHE_TIME" => "3600",
									"CACHE_TYPE" => "A",
									"ELEMENT_ID" => "{$arItem["ID"]}"
							)
					);?>
					<?if (new DateTime() < new DateTime($arItem['PROPERTY']['DATE_TIME_START']['VALUE'])):?>
						<a href="javascript:void(0);" class="eg-icon-button event-card__buy-button Add2BasketTriger" data-key="<?=md5("ADD_BASKET")?>" data-id="<?=$item['ID'];?>" data-quantity="1">
							<svg class="eg-icon-button__icon event-card__buy-button-icon"><use xlink:href="#ticket"></use></svg>
							<span class="eg-icon-button__text event-card__buy-button-text">Купить билет</span>
						</a>
					<?endif;?>					
				</div>
			</div>
		</div>                            
	</div>
        <?php endforeach; ?>
