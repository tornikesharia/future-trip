<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $APPLICATION, $USER;

$arResult = array(
	"CUR_URI" => "",
	"MESSAGE" => "",
	"TYPE" => "ERROR"
);

$arResult["CUR_URI"] = $_POST['CUR_URL'];

$userLoginResult = $USER->Login($_POST['EMAIL'], $_POST['PASSWORD'], "Y");

if ($userLoginResult['TYPE'] == "ERROR"){
	
	$arResult = $userLoginResult;

} else if ($userLoginResult == true && $userLoginResult['TYPE'] !== "ERROR") {

	$arResult['MESSAGE'] = "Вы успешно авторизовались!";
	$arResult['TYPE'] = "OK";
}

echo json_encode($arResult);

//Lib::Debug($arResult,false,true);


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>