<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

?>


<?php
//TMyClass::Dump($arResult)
//TMyClass::Dump( $arResult['ORDERS'] );
?>

<? foreach ($arResult['ORDERS'] as $k => $arOrder) { ?>
    <?
    //TMyClass::Dump($arOrder["BASKET_ITEMS"]);
    //TMyClass::Dump( TMyClass::Date($arOrder["ORDER"]["DATE_INSERT_FORMATED"]) );
    ?>

    <div class="account-order">
        <div class="account-order__left">
            <? /*<div class="account-order__media">
                <img src="http://goncharova.org/temp/assets/img/account/order-img1.png" alt=""
                     class="account-order__img">
            </div>*/ ?>
            <? //$date = TMyClass::Date($arOrder["ORDER"]["DATE_INSERT_FORMATED"]); ?>
            <div class="account-order__media">
                <div class="account-order__date">
                    <div class="account-order__date-day"><?= date('d', strtotime($arOrder["ORDER"]["DATE_INSERT_FORMATED"])) ?></div>
                    <div class="account-order__date-month"><?= FormatDate('F', strtotime($arOrder["ORDER"]["DATE_INSERT_FORMATED"])) ?></div>
                </div>
            </div>
            <div class="account-order__text">
                <? /*<div class="account-order__category">Запись семинара</div>
                <a href="#" class="account-order__title">Финансовый воркшоп</a>*/ ?>
                <a href="#" class="account-order__title">Заказ №<?= $arOrder["ORDER"]["ID"] ?></a>
            </div>
        </div>
        <div class="account-order__center">
            <div class="account-order__count"><?= count($arOrder["BASKET_ITEMS"]) ?></div>
            <div class="account-order__price"><?= CurrencyFormat($arOrder["ORDER"]["PRICE"], $arOrder["ORDER"]["CURRENCY"]); ?><? /*<i class="ruble"></i>*/ ?></div>
            <?
            $color_status = Array(
                "F" => "_green",
                "N" => "_brown",
                "P" => "_red",
                "Z" => ""
            );
            foreach (array("F" => "Выполены", "N" => "Ожидают оплаты", "P" => "Отменён", "Z" => "В обработке") as $k => $name) {
                if ($k == $arOrder["ORDER"]["STATUS_ID"]) { ?>
                    <div class="account-order__status account-order__status<?= $color_status[$k] ?>"><?= $name ?></div>
                <? }
            } ?>
        </div>
        <? if (!in_array($arOrder["ORDER"]["STATUS_ID"], ["F", "P", "Z"])) { ?>
            <a href="<?= $templateFolder ?>/ajax.php" data-id="<?= $arOrder["ORDER"]["ID"] ?>"
               class="account-order__delete-link close">
                <svg class="account-order__delete-icon">
                    <use xlink:href="#close"></use>
                </svg>
            </a>
        <? } else { ?>
            <span class="account-order__delete-link" style="width: 20px;"></span>
        <? } ?>
    </div>
    <div class="account-order-items" style="display: none;">
        <? foreach ($arOrder["BASKET_ITEMS"] as $kItems => $arItems) { ?>
            <div class="account-order">
                <span class="account-order__delete-link" style="width: 20px;"></span>
                <div class="account-order__left">
                    <div class="account-order__media">
                        <? if ($arItems["PREVIEW_PICTURE"]["SRC"]) { ?>
                            <img src="<?= $arItems["PREVIEW_PICTURE"]["SRC"] ?>" alt=""
                                 class="account-order__img">
                        <? } else { ?>
                            <div class="account-order__date">
                                <div class="account-order__date-day"><?= date('d', strtotime($arOrder["ORDER"]["DATE_INSERT_FORMATED"])) ?></div>
                                <div class="account-order__date-month"><?= FormatDate('F', strtotime($arOrder["ORDER"]["DATE_INSERT_FORMATED"])) ?></div>
                            </div>
                        <? } ?>
                    </div>
                    <div class="account-order__text">
                        <? /*<div class="account-order__category">Запись семинара</div>
                <a href="#" class="account-order__title">Финансовый воркшоп</a>*/ ?>
                        <div class="account-order__title"><?= $arItems["NAME"] ?></div>
                    </div>
                </div>
                <div class="account-order__center">
                    <div class="account-order__count"><?= $arItems["QUANTITY"] ?></div>
                    <div class="account-order__price"><?= CurrencyFormat($arItems["PRICE"], $arItems["CURRENCY"]); ?></div>
                    <div class="account-order__status"></div>
                </div>
                <span class="account-order__delete-link" style="width: 20px;"></span>
            </div>
        <? } ?>
    </div>
<? } ?>

<? echo $arResult["NAV_STRING"]; ?>