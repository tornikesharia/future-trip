<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php    
CModule::IncludeModule($arResult["MODULES_NAME"]);

foreach( $arResult["ITEMS"] as $key => $arItem ):    
        if( !empty($arItem["PREVIEW_PICTURE"]) ):
            
                $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                        $arItem["PREVIEW_PICTURE"],
                        array('width' => 300,'height' => 200),
                        BX_RESIZE_IMAGE_EXACT,
                        true,
                        Array()
                );
            
        elseif( !empty($arItem["DETAIL_PICTURE"]) ):
            
                $arResult["ITEMS"][$key]["PREVIEW_PICTURE"] = CFile::ResizeImageGet(
                        $arItem["DETAIL_PICTURE"],
                        array('width' => 300,'height' => 200),
                        BX_RESIZE_IMAGE_EXACT,
                        true,
                        Array()
                );
        else:
            
            $arResult["ITEMS"][$key]["PREVIEW_PICTURE"]["src"] = TMyClass::FromRootToFile(__DIR__)."/img/img.jpg";
            
        endif;
		
		
    //get price
    $dbPrice = CPrice::GetList(
        array("QUANTITY_FROM" => "ASC", "QUANTITY_TO" => "ASC", "SORT" => "ASC"),
        array("PRODUCT_ID" => $arItem['ID']),
        false,
        false,
        array("ID", "CATALOG_GROUP_ID", "PRICE", "CURRENCY", "QUANTITY_FROM", "QUANTITY_TO")
    );
    while ($arPrice = $dbPrice->Fetch()) {
        $arDiscounts = CCatalogDiscount::GetDiscountByPrice(
            $arPrice["ID"],
            $USER->GetUserGroupArray(),
            "N",
            SITE_ID
        );
        $discountPrice = CCatalogProduct::CountPriceWithDiscount(
            $arPrice["PRICE"],
            $arPrice["CURRENCY"],
            $arDiscounts
        );
        $arPrice["DISCOUNT_PRICE"] = $discountPrice;
		$arPrice["DISCOUNT_PRICE_FORMATED"] = CurrencyFormat($discountPrice, $arPrice['CURRENCY']);
		$arPrice['PRICE_FORMATED'] = CurrencyFormat($arPrice['PRICE'], $arPrice['CURRENCY']);
        
        $arResult["ITEMS"][$key]["PRICE"] = $arPrice;
    }
endforeach;