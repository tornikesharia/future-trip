<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("TARIY_COMPOSITE_NAME"),
	"DESCRIPTION" => GetMessage("TARIY_COMPOSITE_DESCRIPTION"),
	//"ICON" => "/images/news_all.gif",
	"COMPLEX" => "Y",
	"PATH" => array(
		"ID" => "tariy_components",
		"SORT" => 2000,
		"NAME" => GetMessage("TARIY_COMPONENTS"),
		"CHILD" => array(
			"ID" => "tariy_favorites",
			"NAME" => GetMessage("TARIY_NEWS"),
			"SORT" => 10,
		),
	),
);

?>