<?php


define(
        __main__,
        "/local/templates/main/"
        );
define(
        __include__,
        __main__."include/"
        );
define(
        __ajax__,
        __main__."ajax/"
        );
define(
        __email__,
        COption::GetOptionString('main','email_from')
        );