<?php require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();

$arParams['PERSONAL_PHOTO_WIDTH'] = 200;
$arParams['PERSONAL_PHOTO_HEIGHT'] = 200;

//file delete
if($_POST['del'] == 'Y'){
	
    $arResult = array(
        'error' => array(),
        'status' => false,
		'data' => array()
    );	
	
	$rsUser =  $USER->GetByID ($USER->GetID()); 
	$arUser = $rsUser->Fetch();

	CFile::Delete((int) $arUser['PERSONAL_PHOTO']);
    $fields['PERSONAL_PHOTO'] = Array('del' => 'Y', 'old_file' => (int) $arUser['PERSONAL_PHOTO'], "MODULE_ID" => "main");
	$USER->Update ($USER->GetID(), $fields);
	
	$rsUser =  $USER->GetByID ($USER->GetID()); 
	$arUser = $rsUser->Fetch();	
	
	if(intval($arUser['PERSONAL_PHOTO']) > 0){
		$arResult['status'] = false;
	} else {
		$html = '<div class="acc-profile__empty-avatar">
					<img src="'.SITE_TEMPLATE_PATH.'/assets/img/icons/img-placeholder.svg" alt="" class="acc-profile__img-placeholder">
					<div class="custom-file">
						<input type="file" name="PERSONAL_PHOTO" class="custom-file-input" id="acc-profile__avatar" accept="image/x-png,image/gif,image/jpeg">
						<label class="custom-file-label" for="acc-profile__avatar">Загрузить фото</label>
					</div>
				</div>';		
		$arResult['data']['empty_photo_html'] = $html;
		$arResult['status'] = true;
	}

	echo json_encode($arResult);	
	exit();
} 

if (check_bitrix_sessid("profile_personal") || check_bitrix_sessid("profile_legal")) {

    define("STOP_STATISTICS", true);
    define("PUBLIC_AJAX_MODE", true);

    $arResult = array(
        'error' => array(),
        'status' => false,
		'data' => array()
    );
	
	//Lib::Debug($_REQUEST['DATA']);
	
    if (!empty($_REQUEST['DATA'])) {
		

		$arData = $_REQUEST['DATA'];
	
		if($arData['PROFILE_TYPE'] == 'PERSONAL'){
	
			if (!isset($arData['FZ152']) || empty($arData['FZ152'])) {
				$arResult['error'][] = array(
					'type' => 'DATA',
					'name' => 'FZ152',
					'text' => 'Вы не согласны с "Политикой конфиденциальности"!'
				);
			} 		
		
			if($arData["PERSONAL_BIRTHDAY"]){
				$date = date_create($arData["PERSONAL_BIRTHDAY"]);
				$arData["PERSONAL_BIRTHDAY"] = date_format($date,"d.m.Y");			
			}		

			foreach ($arData as $key => $value) {
				if($key != "OLD_PASS") $arData[$key] = addslashes(strip_tags($value));
				$fileError = true;
				
				switch ($key) {
					case 'PASS':
						$arData["NEW_PASS"] = false; 
						
						if (!empty($value) || !empty($arData["OLD_PASS"])) {
							if (!empty($value) and empty($arData["OLD_PASS"])) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => "OLD_PASS",
									'text' => 'Поле "Текущий пароль" не заполнено!'
								);
							} else if (empty($value) and !empty($arData["OLD_PASS"])) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => $key,
									'text' => 'Поле "Новый пароль" не заполнено!'
								);
							} else if (!Lib::UserPassword($USER->GetID(), $arData["OLD_PASS"])) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => "OLD_PASS",
									'text' => '"Текущий пароль" не верный!'
								);
							} else {
								$arData["NEW_PASS"] = $value;
							}
						}
						break;
					case 'EMAIL':
						if (empty($value)) {
							$arResult['error'][] = array(
								'type' => 'DATA',
								'name' => $key,
								'text' => 'Поле "Email" не заполнено!'
							);
						} else {
							if (!preg_match("/^[-0-9a-z_\.]+@[-0-9a-z^\.]+\.[a-z]{2,4}$/i", $value)) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => $key,
									'text' => 'Поле "E-mail" неправильно заполнено!'
								);
							}
						}
						break;
					case 'NAME':
						if (empty($value)) {
							$arResult['error'][] = array(
								'type' => 'DATA',
								'name' => $key,
								'text' => 'Поле "Имя" не заполнено!'
							);
						} else {
							$arData["NAME"] = $value;
							/* 							
							$full_name = explode(" ", $value);

							$arData["LAST_NAME"] = '';
							$arData["NAME"] = '';
							$arData["SECOND_NAME"] = '';

							if ($full_name[0]) {
								$arData["LAST_NAME"] = $full_name[0];
							}
							if ($full_name[0]) {
								$arData["NAME"] = $full_name[1];
							}
							if ($full_name[0]) {
								$arData["SECOND_NAME"] = $full_name[2];
							} 
							*/
						}
						break;
					case 'PERSONAL_PHONE':
						if (empty($value)) {
							$arResult['error'][] = array(
								'type' => 'DATA',
								'name' => $key,
								'text' => 'Поле "Телефон" не заполнено!'
							);
						} else {
							$poneArray = Array('+', '(', ')', ' ', '-');
							foreach ($poneArray as $pArray) {
								$value = str_replace($pArray, "", $value);
							}

							if (!is_numeric($value)) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => $key,
									'text' => 'Ошибка, в поле "Телефон"'
								);
							} else if (iconv_strlen($value) < 6) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => $key,
									'text' => '"Телефон" не может быть меньше 6 цифр'
								);
							}
						}
						break;
					case 'PERSONAL_GENDER':
						if (empty($value)) {
							$arResult['error'][] = array(
								'type' => 'DATA',
								'name' => $key,
								'text' => 'Поле "Пол" не Заполнено!'
							);
						}
						break;
				}
			}

			$arFields = [];

			foreach($arData as $dKey => $value){
				$arFields[$dKey] = ($arData[$dKey] ? $arData[$dKey] : '');
			}	
			unset($arFields['PROFILE_TYPE']);
			
			//Lib::Debug($arFields);
			
			if (empty($arResult['error']) and $arData["NEW_PASS"]) {
				$arFields["PASSWORD"] = $arData["NEW_PASS"];
				$arFields["CONFIRM_PASSWORD"] = $arData["NEW_PASS"];
			}
			
			
			if($_FILES["PERSONAL_PHOTO"]["size"] > 1024*5*1024)
			{
				$arResult['error'][] = array(
					'type' => 'DATA',
					'name' => 'PERSONAL_PHOTO',
					'text' => 'Размер фото превышает 3 мегабайта'
				);				
			} else {
				if(is_uploaded_file($_FILES["PERSONAL_PHOTO"]["tmp_name"]))
				{
					move_uploaded_file($_FILES["PERSONAL_PHOTO"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$_FILES["PERSONAL_PHOTO"]["name"]);
					$arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$_FILES["PERSONAL_PHOTO"]["name"]);
					$arFile["MODULE_ID"] = "main";
					$fid = CFile::SaveFile($arFile, "main");
				 
					if (intval($fid)>0)
					{
						$arPhoto = CFile::MakeFileArray($fid);

						$fields = Array(
							"PERSONAL_PHOTO" => $arPhoto,
						);
						
						$USER->Update($USER->GetID(), $fields);
						CFile::Delete($fid);
						unlink($_SERVER["DOCUMENT_ROOT"]."/upload/tmp/".$_FILES["PERSONAL_PHOTO"]["name"]);
						
						$rsUser = $USER->GetByID($USER->GetID());
						$arUser = $rsUser->Fetch();
						if($arUser['PERSONAL_PHOTO']):
							$renderImage = CFile::ResizeImageGet($arUser['PERSONAL_PHOTO'], Array("width" => $arParams['PERSONAL_PHOTO_WIDTH'], "height" => $arParams['PERSONAL_PHOTO_HEIGHT']), BX_RESIZE_IMAGE_EXACT, true);
							$arUser['PERSONAL_PHOTO'] = [];
							$arUser['PERSONAL_PHOTO']['SRC'] = $renderImage['src'];
							$arUser['PERSONAL_PHOTO']['WIDTH'] = $renderImage['width'];
							$arUser['PERSONAL_PHOTO']['HEIGHT'] = $renderImage['height'];
							$arUser['PERSONAL_PHOTO']['FILE_SIZE'] = $renderImage['size'];
							
							$arResult['data']['PERSONAL_PHOTO'] = $arUser['PERSONAL_PHOTO']['SRC'];
						endif;						
						//Lib::Debug($arResult); 
						
						
					}
				}
			}			
			
			
		
			if (empty($arResult['error'])) {
				if ($USER->Update($arUser["ID"], $arFields)) {
				} else if ($USER->LAST_ERROR){
					$arResult['error'][] = array(
						'type' => 'DATA',
						'name' => 'LAST_ERROR',
						'text' => $USER->LAST_ERROR
					);
				} else {
					$arResult['error'][] = array(
						'type' => 'DATA',
						'name' => 'KEY',
						'text' => 'Ошибка, изменения данных!'
						//'text' => $USER->LAST_ERROR
					);
				}
			}

			if (empty($arResult['error'])) {
				$SID = true;
			}
			
			if (!$SID) {
				$arResult['error'][] = array(
					'type' => 'submit',
					'text' => 'Сообщение небыло отправлено.'
				);
			} else {
				$arResult['status'] = true;
			}
			
		}  // PERSONAL
		
		if($arData['PROFILE_TYPE'] == 'LEGAL'){
	
			if (!isset($arData['FZ152']) || empty($arData['FZ152'])) {
				$arResult['error'][] = array(
					'type' => 'DATA',
					'name' => 'FZ152',
					'text' => 'Вы не согласны с "Политикой конфиденциальности"!'
				);
			} 		

			foreach ($arData as $key => $value) {
				$arData[$key] = addslashes(strip_tags($value));
				$fileError = true;
				
				switch ($key) {


					case 'UF_INN':
						if(!empty($value)){
							if (!is_numeric($value)) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => $key,
									'text' => 'Ошибка, в поле "Инн" должны быть только цифры'
								);
							} else if (iconv_strlen($value) < 10) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => $key,
									'text' => 'Ошибка, в поле "Инн" не менее 10 символов'
								);
							} else if (iconv_strlen($value) > 12) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => $key,
									'text' => 'Ошибка, в поле "Инн" не более 12 символов'
								);
							}
						}
						break;	
				
					case 'WORK_PHONE':
						if (!empty($value)) {
							$poneArray = Array('+', '(', ')', ' ', '-');
							foreach ($poneArray as $pArray) {
								$value = str_replace($pArray, "", $value);
							}

							if (!is_numeric($value)) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => $key,
									'text' => 'Ошибка, в поле "Телефон"'
								);
							} else if (iconv_strlen($value) < 6) {
								$arResult['error'][] = array(
									'type' => 'DATA',
									'name' => $key,
									'text' => '"Телефон" не может быть меньше 6 цифр'
								);
							}
						}
						break;
				}
			}
			
			$arFields = [];

			foreach($arData as $dKey => $value){
				$arFields[$dKey] = ($arData[$dKey] ? $arData[$dKey] : '');
			}	
			unset($arFields['PROFILE_TYPE'], $arFields['FZ152']);
			
			//Lib::Debug($arResult);
		
			if (empty($arResult['error'])) {
				if ($USER->Update($arUser["ID"], $arFields)) {
				} else if ($USER->LAST_ERROR){
					$arResult['error'][] = array(
						'type' => 'DATA',
						'name' => 'LAST_ERROR',
						'text' => $USER->LAST_ERROR
					);
				} else {
					$arResult['error'][] = array(
						'type' => 'DATA',
						'name' => 'KEY',
						'text' => 'Ошибка, изменения данных!'
						//'text' => $USER->LAST_ERROR
					);
				}
			}

			if (empty($arResult['error'])) {
				$SID = true;
			}
			
			if (!$SID) {
				$arResult['error'][] = array(
					'type' => 'submit',
					'text' => 'Сообщение небыло отправлено.'
				);
			} else {
				$arResult['status'] = true;
			}
		}  // LEGAL		
					
	} else {
		$arResult['error'][] = array(
			'type' => 'submit',
			'text' => 'Нет входных данных.'
		);
	}
	
	
    echo json_encode($arResult);
    die();

} else {
    if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

    $arResult["USER"] = $arUser;

    $db_vars = CSaleLocation::GetList(
        array(
            "CITY_NAME" => "ASC",
        ),
        array(
            "LID" => LANGUAGE_ID,
            "!CITY_NAME" => false
        ),
        false,
        false,
        array()
    );
    while ($vars = $db_vars->Fetch()) {
        $arResult["CITY"][] = $vars;
    }
	
    //if ($this->StartResultCache($arParams['CACHE_TIME'])):
        $this->IncludeComponentTemplate();
    //endif;

} 



